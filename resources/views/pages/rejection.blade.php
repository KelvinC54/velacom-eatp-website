@extends('layouts.main')

@section('title','Rejection')

@section('main-content')
<div class="section-body">
	@if(Session::get('message'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"></button>
			<p> {{ Session::get('message') }} </p>
		</div>
	@endif
	<div class="container-fluid mt-2">
		<div class="d-flex justify-content-between align-items-center ">
			<div class="header-action">
				<h1 class="page-title">Documents Rejection Records</h1>
			</div>
		</div>
	</div>
</div>
<div class="section-body mt-2">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="table-responsive">
						@if (
							(Auth::user()->jobdesk == "vlcadm" || Auth::user()->jobdesk == "vlcpm" || Auth::user()->jobdesk === "tkminframanager")
						)
						<table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="reject_table" style="width: 100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Site ID</th>
									<th>Site Name</th>
									<th>Area</th>
									<th>Location</th>
									<th>Reject Date</th>
                                    <th>Rejected by</th>
                                    <th>Alasan Reject</th>
									<th style="display: none;">Alasan Reject</th>
								</tr>
							</thead>
						</table>
						<div id="remarks_modal" class="modal fade">
							<div class="modal-dialog modal-dialog-scrollable">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Alasan Reject :</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p id="rm1"></p>
									</div>
									<div class="modal-footer justify-content-center">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
									</div>
								</div>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function showData(id) {
        $.ajax({
            type: "GET",
            url: "{{URL('/rejection/viewremarks')}}/"+id,
            dataType: "JSON",
            success: function ( data ) {
                if ( data.success ) {
                    $('#rm1').html(data.reject.doc_remarks);
                    $('#remarks_modal').modal('show');
                } else {
                    $('#rm1').html('<i class="fa fa-danger"></i>&nbsp; Terjadi kesalahan saat memuat data, silahkan muat ulang halaman')
                }
            }
        });
    }
	$(document).ready( function () {
		btnView = function(id){
            return '<div class="btn-group btn-group-sm"><a class="btn btn-warning" data-id="'+id+'" onclick="showData('+id+')">'
                    +'<i class="fa fa-eye" aria-hidden="true"></i>';
					+'Alasan';
                    +'</a>';
        };
		var t = $('#reject_table').DataTable({
			stateSave: true,
			dom: 'Blfrtip',
			ordering: false,
			processing: true,
			serverSide: true,
			ajax: '/rejection/json',
			lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'All' ]
			],
			columns: [
				{
					"data": null,
					"sortable": false, 
					render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
							}  
				},
				{ data: 'site_id', name: 'site_id' },
				{ data: 'site_name', name: 'site_name' },
				{ data: 'site_area', name: 'site_area' },
				{ data: 'site_loc', name: 'site_loc' },
				{ data: 'date', name: 'date' },
				{ data: 'user_name', name: 'user_name' },
				{
					class: 'text-center',
					sortable: false,
					searchable: false,
					render: function ( data, type, full, meta ) {
								id = full.id;
							return btnView(id);
					}
            	},
				{ data: 'doc_remarks', name: 'doc_remarks' }
			],
			columnDefs: [
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
        	],
			buttons: [
				{
					extend: 'excel',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4, 5, 6, 8 ]
					}
				},
			]
		});
	} );
</script>
@endsection
