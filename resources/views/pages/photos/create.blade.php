@extends('layouts.main')

@section('title','Create ATP Report')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>
<div class="section-body mt-2">
		<div class="container-fluid">
			<h1 class="page-title">Create Photos</h1>
			<div class="tab-content mt-2">
<form action="{{ url('photos/') }}" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}
	<div class="step step-1 active">
	<div class="card">
	<div class="card-header text-white bg-info">General Information Photo</div>
	<div class="card-body">
		<div class="form-group">
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<p>Site Information</p>
						<input type="file" accept="image/*" name="gi_site_information">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<p>Site Access</p>
						<input type="file" accept="image/*" name="gi_site_access">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<p>Site - Front View</p>
						<input type="file" accept="image/*" name="gi_site_front_view">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<p>Site - Rear View</p>
						<input type="file" accept="image/*" name="gi_site_rear_view">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<p>Site - Left View</p>
						<input type="file" accept="image/*" name="gi_site_left_view">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<p>Site - Right View</p>
						<input type="file" accept="image/*" name="gi_site_right_view">
					</div>
				</div>
			</div>
		</div>
		</div>
	<div class="card-footer">
		<button type="button" class="next-btn btn btn-success float-right">Next</button>
	</div>
</div>
</div>
<div class="step step-2">
		<div class="card">
			<div class="card-header text-white bg-info">New Foundation Installation Photo</div>
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<p>Before Installation</p>
								<input type="file" accept="image/*" name="nf_installation_before">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<p>Installation #1</p>
								<input type="file" accept="image/*" name="nf_installation_1">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<p>Installation #2</p>
								<input type="file" accept="image/*" name="nf_installation_2">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<p>Installation #3</p>
								<input type="file" accept="image/*" name="nf_installation_3">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<p>Installation #4</p>
								<input type="file" accept="image/*" name="nf_installation_4">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<p>Installation #5</p>
								<input type="file" accept="image/*" name="nf_installation_5">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<p>Installation #6</p>
								<input type="file" accept="image/*" name="nf_installation_6">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<p>Installation #7</p>
								<input type="file" accept="image/*" name="nf_installation_7">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<p>Foundation Wide Measurement</p>
								<input type="file" accept="image/*" name="nf_foundation_wide">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<p>Foundation Height Measurement</p>
								<input type="file" accept="image/*" name="nf_foundation_weight">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<p>Foundation Long Measurement</p>
								<input type="file" accept="image/*" name="nf_foundation_long">
							</div>
						</div>
						<div class="col-6">
							<div class="form-group">
								<p>After Installation</p>
								<input type="file" accept="image/*" name="nf_installation_after">
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="card-footer">
				<button type="button" class="next-btn btn btn-success float-right">Next</button>
				<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
			</div>
		</div>
	</div>
	<div class="step step-3">
			<div class="card">
				<div class="card-header text-white bg-info">Site Layout New Installation Foundation</div>
				<div class="card-body">
					<div class="form-group">
						<p>Site Layout New Installation Foundation</p>
						<input type="file" accept="image/*" name="site_layout">
					</div>
				</div>
				<div class="card-footer">
					<button type="button" class="next-btn btn btn-success float-right">Next</button>
					<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
				</div>
			</div>
		</div>
		<div class="step step-4">
				<div class="card">
					<div class="card-header text-white bg-info">Rectifier Installation Photo</div>
					<div class="card-body">
						<div class="form-group">
							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Base Frame Before Installation</p>
										<input type="file" accept="image/*" name="ri_base_frame_before">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Base Frame After Installation</p>
										<input type="file" accept="image/*" name="ri_base_frame_after">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Main Rack</p>
										<input type="file" accept="image/*" name="ri_main_rack_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Slave Rack</p>
										<input type="file" accept="image/*" name="ri_slave_rack_1">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Main Rack - 2</p>
										<input type="file" accept="image/*" name="ri_main_rack_2">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Slave Rack - 2</p>
										<input type="file" accept="image/*" name="ri_slave_rack_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>PLN KWH - Before</p>
										<input type="file" accept="image/*" name="ri_pln_kwh_before">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>PLN KWH - After</p>
										<input type="file" accept="image/*" name="ri_pln_kwh_after">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>ACPDB - Before</p>
										<input type="file" accept="image/*" name="ri_acpdb_before">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>ACPDB - After</p>
										<input type="file" accept="image/*" name="ri_acpdb_after">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>MCB ACPDB - Before</p>
										<input type="file" accept="image/*" name="ri_acpdb_mcb_before">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>MCB ACPDB - After</p>
										<input type="file" accept="image/*" name="ri_acpdb_mcb_after">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Rectifier Front</p>
										<input type="file" accept="image/*" name="ri_rectifier_front">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Main MCB AC Input Rectifier</p>
										<input type="file" accept="image/*" name="ri_main_mcb_ac_input">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>MCB DC Distribution</p>
										<input type="file" accept="image/*" name="ri_mcb_dc_dist">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Controller Module</p>
										<input type="file" accept="image/*" name="ri_controller_module">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Module #1</p>
										<input type="file" accept="image/*" name="ri_module_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Module #2</p>
										<input type="file" accept="image/*" name="ri_module_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Module #3</p>
										<input type="file" accept="image/*" name="ri_module_3">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Module #4</p>
										<input type="file" accept="image/*" name="ri_module_4">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>DC Cable Connection – Main Rack</p>
										<input type="file" accept="image/*" name="ri_dc_cable_con_main_rack">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>DC Cable p – Main Rack</p>
										<input type="file" accept="image/*" name="ri_dc_cable_label_main_rack">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>DC Cable Connection – Slave Rack</p>
										<input type="file" accept="image/*" name="ri_dc_cable_con_slave_rack">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>DC Cable p – Slave Rack</p>
										<input type="file" accept="image/*" name="ri_dc_cable_label_slave_rack">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>GND Connection #1</p>
										<input type="file" accept="image/*" name="ri_gnd_con_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>GND Connection #2</p>
										<input type="file" accept="image/*" name="ri_gnd_con_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>GND Connection #3</p>
										<input type="file" accept="image/*" name="ri_gnd_con_3">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>GND Connection #4</p>
										<input type="file" accept="image/*" name="ri_gnd_con_4">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Laying Cable #1</p>
										<input type="file" accept="image/*" name="ri_laying_cable_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Laying Cable #2</p>
										<input type="file" accept="image/*" name="ri_laying_cable_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Laying Cable #3</p>
										<input type="file" accept="image/*" name="ri_laying_cable_3">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Laying Cable #4</p>
										<input type="file" accept="image/*" name="ri_laying_cable_4">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Fan – Main Rack</p>
										<input type="file" accept="image/*" name="ri_fan_main">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Fan – Slave rack</p>
										<input type="file" accept="image/*" name="ri_fan_slave">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Lamp – Main Rack</p>
										<input type="file" accept="image/*" name="ri_lamp_main">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Lamp – Slave Rack</p>
										<input type="file" accept="image/*" name="ri_lamp_slave">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Panel Distribution CDC</p>
										<input type="file" accept="image/*" name="ri_panel_dist">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (R – S)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_rs">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (S – T)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_st">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (R – T)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_rt">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (R – N)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_rn">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (S – N)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_sn">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (T – N)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_tn">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>AC Measurement  (N – G)</p>
										<input type="file" accept="image/*" name="ri_ac_measure_ng">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>DC Output/Distribution Measurement</p>
										<input type="file" accept="image/*" name="ri_dc_output">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>AC Output Measurement</p>
										<input type="file" accept="image/*" name="ri_ac_output">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Controller Module Setting #1</p>
										<input type="file" accept="image/*" name="ri_control_module_setting_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Controller Module Setting #2</p>
										<input type="file" accept="image/*" name="ri_control_module_setting_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Controller Module Setting #3</p>
										<input type="file" accept="image/*" name="ri_control_module_setting_3">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Controller Module Setting #4</p>
										<input type="file" accept="image/*" name="ri_control_module_setting_4">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Alarm Connection #1</p>
										<input type="file" accept="image/*" name="ri_alarm_connection_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Alarm Connection #2</p>
										<input type="file" accept="image/*" name="ri_alarm_connection_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Alarm p #1</p>
										<input type="file" accept="image/*" name="ri_alarm_label_1">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Alarm p #2</p>
										<input type="file" accept="image/*" name="ri_alarm_label_2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Battery Rack - Front View</p>
										<input type="file" accept="image/*" name="ri_battery_rack">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank Installation #1</p>
										<input type="file" accept="image/*" name="ri_battery_bank_inst_1">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank Installation #2</p>
										<input type="file" accept="image/*" name="ri_battery_bank_inst_2">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank Installation #3</p>
										<input type="file" accept="image/*" name="ri_battery_bank_inst_3">
									</div>
								</div>
							</div>
							
							<div class="row">
							    <div class="col-6">
									<div class="form-group">
										<p>Battery Bank Installation #4</p>
										<input type="file" accept="image/*" name="ri_battery_bank_inst_4">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank #1 DC Measurement</p>
										<input type="file" accept="image/*" name="ri_battery_bank_1_dc_measure">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank #2 DC Measurement</p>
										<input type="file" accept="image/*" name="ri_battery_bank_2_dc_measure">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank #3 DC Measurement</p>
										<input type="file" accept="image/*" name="ri_battery_bank_3_dc_measure">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Battery Bank #4 DC Measurement</p>
										<input type="file" accept="image/*" name="ri_battery_bank_4_dc_measure">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Battery layout</p>
										<input type="file" accept="image/*" name="ri_battery_label">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<p>Battery Main Bar installation</p>
										<input type="file" accept="image/*" name="ri_battery_main_bar_inst">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<p>Battery MCB/Fuse</p>
										<input type="file" accept="image/*" name="ri_battery_mcb_fuse">
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="card-footer">
						<button type="button" class="next-btn btn btn-success float-right">Next</button>
						<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
					</div>
				</div>
			</div>
			<div class="step step-14">
					<div class="card">
						<div class="card-header text-white bg-info">Site Layout Rectifier Installation</div>
						<div class="card-body">
							<div class="form-group">
								<p>Site Layout Rectifier Installation</p>
								<input type="file" accept="image/*" name="site_layout_rectifier">
							</div>
						</div>
						<div class="card-footer">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button type="submit" class="btn btn-success float-right">Submit</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">

const steps = Array.from(document.querySelectorAll("form .step"));
const nextBtn = document.querySelectorAll("form .next-btn");
const prevBtn = document.querySelectorAll("form .previous-btn");
const form = document.querySelector("form");

nextBtn.forEach((button) => {
	button.addEventListener("click", () => {
		changeStep("next");
	});
});

prevBtn.forEach((button) => {
	button.addEventListener("click", () => {
		changeStep("prev");
	});
});

form.addEventListener("submit", (e) => {
	e.preventDefault();
	const inputs = [];
	form.querySelectorAll("input").forEach((input) => {
		const { name, value } = input;
		inputs.push({ name, value });
	});
	console.log(inputs);
	form.reset();
});

function changeStep(btn) {
	let index = 0;
	const active = document.querySelector(".active");
	index = steps.indexOf(active);
	steps[index].classList.remove("active");
	if (btn === "next") {
		index++;
	} else if (btn === "prev") {
		index--;
	}
	steps[index].classList.add("active");
}

</script>

<script src="assets/bundles/lib.vendor.bundle.js"></script>
<script src="assets/plugins/dropify/js/dropify.min.js"></script>
<script src="assets/js/dropify.js"></script>

@endsection
