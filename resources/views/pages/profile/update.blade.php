@extends('layouts.main')

@section('title','Edit Profile')

@section('main-content')

<div class="section-body mt-2">
    @if(Session::get('message'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"></button>
			<p> {{ Session::get('message') }} </p>
		</div>
	@endif
	<div class="container-fluid">
		<h1 class="page-title">Edit Profile</h1>

		<div class="tab-content mt-2">
			<form action="/profile/{{ $users->id }}" method="POST" enctype="multipart/form-data">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Nama</label>
							<input id="nama" type="text" placeholder="Nama" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $users->name }}" autocomplete="nama" disabled>

							@error('nama')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<label>Email</label>
							<input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $users->email }}" autocomplete="email" disabled>

							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<label>Jobdesk</label>
							<select class="form-control @error('jobdesk') is-invalid @enderror" id="jobdesk" name="jobdesk" disabled>
                                @if($users->jobdesk === 'webadmin')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin" selected>Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'vlcadm')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm" selected>Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'vlccord')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord" selected>Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'vlcpm')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm" selected>Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'tkminframanager')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager" selected>Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'tkmmanagerns')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns" selected>Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'reviewertkmrto')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto" selected>Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'tkmmanagercpo')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo">Telkomsel CPO</option>
                                    <option value="tkmmanagercpo" selected>Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'tkmcpo')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo" selected>Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'tkmrevrectbatt')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo" >Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt" selected>Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
                                @elseif($users->jobdesk === 'tkmpmrectbatt')
                                    <option disabled>-- Jobdesk --</option>
                                    <option value="webadmin">Web Admin</option>
                                    <option value="vlcadm">Velacom Administrator</option>
                                    <option value="vlccord">Velacom Coordinator</option>
                                    <option value="vlcpm">Velacom Project Manager</option>
                                    <option value="tkminframanager">Telkom Infra Manager</option>
                                    <option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                    <option value="tkmmanagerns">Telkomsel Manager NS</option>
                                    <option value="tkmcpo" >Telkomsel CPO</option>
                                    <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                    <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                    <option value="tkmpmrectbatt" selected>Telkomsel PM Rect Batt</option>
                                @endif
							</select>

							@error('jobdesk')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>

                        <div class="form-group">
							<label>Password</label>
							<input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" autofocus>

							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>

                        <div class="form-group">
							<label>Confirm Password</label>
							<input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required autocomplete="new-password" autofocus>
						</div>
					<div class="card-footer">
						<input type="hidden" name="_method" value="put">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="next-btn btn btn-success float-right">Edit</button>
                        <a href="/" class="previous-btn btn btn-secondary float-left">Kembali</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection
