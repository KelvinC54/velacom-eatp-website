@extends('layouts.main')

@section('title','Email Log')

@section('main-content')
<div class="section-body">
	@if(Session::get('message'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"></button>
			<p> {{ Session::get('message') }} </p>
		</div>
	@endif
	<div class="container-fluid mt-2">
		<div class="d-flex justify-content-between align-items-center ">
			<div class="header-action">
				<h1 class="page-title">Email Notification Log</h1>
			</div>
		</div>
	</div>
</div>
<div class="section-body mt-2">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="table-responsive">
						@if (Auth::user()->jobdesk == "vlcadm")
						<table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="tabel_log">
							<thead>
                                <tr>
                                    <th rowspan="2">No</th>
									<th rowspan="2">Date</th>
									<th colspan="3">Recipient</th>
									<th colspan="5">Document Detail</th>
                                    <th rowspan="2">Status</th>
								</tr>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Location</th>
									<th>Site ID</th>
									<th>Project Name</th>
									<th>Site Name</th>
									<th>Site Location</th>
                                    <th>Site Area</th>
								</tr>
							</thead>
						</table>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $('#tabel_log').DataTable({
		dom: 'Blfrtip',
        processing: true,
        serverSide: true,
        ajax: '/emaillog/json',
		lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'All' ]
		],
        columns: [
            {
					"data": null,
					"sortable": false, 
					render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
							}  
			},
            { data: 'created_at', name: 'created_at' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'location', name: 'location' },
            { data: 'site_id', name: 'site_id' },
			{ data: 'project_name', name: 'project_name' },
            { data: 'site_name', name: 'site_name' },
            { data: 'site_loc', name: 'site_loc' },
            { data: 'site_area', name: 'site_area' },
            { data: 'status', name: 'status' },
        ],
		buttons:[
			'excel',
		]
    });	
</script>
@endsection
