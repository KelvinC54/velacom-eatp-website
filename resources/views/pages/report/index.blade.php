@extends('layouts.main')

@section('title','ATP Report')

@section('main-content')
    <div class="section-body">
        @if(Session::get('message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> {{ Session::get('message') }} </p>
            </div>
        @endif
        <div class="container-fluid mt-2">
            <div class="d-flex justify-content-between align-items-center ">
                <div class="header-action">
                    <h1 class="page-title">ATP Report</h1>
                </div>
                @if (Auth::user()->jobdesk === "vlcadm")
                    <a href="{{ url('/report/create') }}" class="btn btn-info btn-sm">Create ATP</a>
                @endif
            </div>
        </div>
    </div>
    <div class="section-body mt-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="report_table">
                                <thead>
                                <tr>
                                    <th>Date Created</th>
                                    <th>Project</th>
                                    <th>Site ID</th>
                                    <th>Site Name</th>
                                    <th>Area</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                    <th>Alasan Reject</th>
                                    <th>BoQ</th>
                                    @if (Auth::user()->jobdesk == "reviewertkmrto" || Auth::user()->jobdesk == "tkmmanagerns")
                                        <th></th>
                                    @else
                                        <th>BAPA</th>
                                    @endif
                                    @if (Auth::user()->location == "RTPO PALANGKARAYA" || Auth::user()->location == "NS PALANGKARAYA" || Auth::user()->jobdesk == "vlcadm" ||
									Auth::user()->jobdesk == "vlcpm" || Auth::user()->jobdesk == "tkminframanager" ||
									Auth::user()->jobdesk == "tkmcpo" || Auth::user()->jobdesk == "tkmmanagercpo")
											<th>File Tambahan</th>
									@else
											<th></th>
									@endif
                                    <th>ATP</th>
                                    @if (Auth::user()->jobdesk == "reviewertkmrto" || Auth::user()->jobdesk == "tkmmanagerns")
                                        <th></th>
                                    @else
                                        <th>BAL</th>
                                    @endif

                                    {{--
                                    **************************************
                                    * Date Modified : 17/06/2021 11:16   *
                                    * Author        : Setiawan Joko      *
                                    **************************************
                                    --}}

                                    @if(request()->is('summary/need-review'))
                                        <th>Aksi</th>
                                    @endif


                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documents as $d)
                                        <tr>
                                            <td>{{$d->doc_date}}</td>
                                            <td>{{$d->project_name}}</td>
                                            <td>{{$d->site_id}}</td>
                                            <td>{{$d->site_name}}</td>
                                            <td>{{$d->site_area}}</td>
                                            <td>{{$d->site_loc}}</td>
                                            <td>
                                                {{-- Status Code --}}
                                                <?php
                                                switch ($d->doc_status) {
                                                    case '0':
                                                        echo "Belum di Review";
                                                        break;
                                                    case '1':
                                                        echo "Rejected";
                                                        break;
                                                    case '2':
                                                        echo "Signed by Velacom PM";
                                                        break;
                                                    case '3':
                                                        echo "Signed by TelkomInfra Manager";
                                                        break;
                                                    case '4':
                                                        echo "Approved by Telkomsel RTPO";
                                                        break;
                                                    case '5':
                                                        echo "Signed by Telkomsel Manager NS";
                                                        break;
                                                    case '6':
                                                        echo "Approved by Staff Telkomsel CPO";
                                                        break;
                                                    case '7':
                                                        echo "Signed by Telkomsel Manager CPO";
                                                        break;
                                                    default:

                                                        break;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#remarks_{{ $d->doc_no }}">Lihat</button>

                                                <!-- Modal HTML -->
                                                <div id="remarks_{{ $d->doc_no }}" class="modal fade">
                                                    <div class="modal-dialog modal-dialog-scrollable">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Alasan Reject :</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>{{ $d->doc_remarks }}</p>
                                                            </div>
                                                            <div class="modal-footer justify-content-center">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            @if($d->file_boq != NULL)
                                                <td><a target="_blank" href="{{ url('boq/' . $d->file_boq) }}"><i class="fa fa-file"></i></a></td>
                                            @else
                                                <td></td>
                                            @endif
                                            @if($d->file_bapa != NULL)
                                                @if (Auth::user()->jobdesk == "reviewertkmrto" || Auth::user()->jobdesk == "tkmmanagerns")
                                                    <td></td>
                                                @else
                                                    <td><a target="_blank" href="{{ url('bapa/' . $d->file_bapa) }}"><i class="fa fa-file"></i></a></td>
                                                @endif
                                            @endif
                                            @if(Auth::user()->location == "RTPO PALANGKARAYA" || Auth::user()->location == "NS PALANGKARAYA" || Auth::user()->jobdesk == "vlcadm" ||
									        Auth::user()->jobdesk == "vlcpm" || Auth::user()->jobdesk == "tkminframanager"
									        || Auth::user()->jobdesk == "tkmcpo" || Auth::user()->jobdesk == "tkmmanagercpo")
    										    @if ($d->file_tambahan != NULL)
    											    <td><a target="_blank" href="{{ url('tambahan/' . $d->file_tambahan) }}"><i class="fa fa-file"></i></a></td>
    									        @else
    											    <td></td>
    									        @endif
										    @endif
                                            <td><a target="_blank" href="{{ url('report/cetak-atp/' . $d->doc_no) }}"><i class="fa fa-file"></i></a></td>
                                            @if (Auth::user()->jobdesk == "reviewertkmrto" || Auth::user()->jobdesk == "tkmmanagerns")
                                                <td></td>
                                            @else
                                                <td><a target="_blank" href="{{ url('report/cetak-bal/' . $d->doc_no) }}"><i class="fa fa-file"></i></a></td>
                                            @endif

                                            @if(request()->is('summary/need-review'))
                                                <td>
                                                    {{-- vlcadmn status 0, 1, dapat edit dan hapus dokumen --}}
                                                    @if(Auth::user()->jobdesk == "vlcadm" && ($d->doc_status == 0 || $d->doc_status == 1))
                                                        <div class="ui-group-buttons">
                                                            <a href="{{ url('report/'. $d->doc_no .'/edit') }}" class="btn btn-primary">Edit</a>
                                                        {{--<div class="or"></div>--}}
                                                        <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hapusDoc{{ $d->doc_no }}">Hapus</button>

                                                            <!-- Modal HTML -->
                                                            <div id="hapusDoc{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk menghapus data?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk menghapus data? Data tidak dapat dikembalikan.</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <form action="report/{{ $d->doc_no }}" method="POST">
                                                                                <input type="hidden" name="_method" value="delete">
                                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                                <button type="submit" class="button btn btn-danger">Hapus</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- vlcadmn selain status 0, 1, tidak bisa edit dan hapus dokumen --}}
                                                    @elseif(Auth::user()->jobdesk == "vlcadm" && $d->doc_status > 1)
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Edit</a>
                                                            <a href="#" class="button btn btn-danger disabled">Hapus</a>
                                                        </div>

                                                        {{-- vlcpm status 0, dapat approve / reject dokumen dari vlcadmin --}}
                                                    @elseif(Auth::user()->jobdesk == "vlcpm" && $d->doc_status == 0)
                                                        <div class="ui-group-buttons">
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve_0_{{ $d->doc_no }}">Approve</button>

                                                            <!-- Modal HTML -->
                                                            <div id="approve_0_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk Approve Dokumen?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk Approve Dokumen?</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <a href="{{ url('report/approve-0/'. $d->doc_no) }}" class="btn btn-primary">Approve</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="or"></div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_0_{{ $d->doc_no }}">Reject</button>

                                                            <!-- Modal HTML -->
                                                            <div id="reject_0_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="{{ url('report/reject/' . $d->doc_no . '/' . Auth::user()->id) }}" method="GET">
                                                                                <div class="form-group">
                                                                                    <label for="doc-remarks" class="col-form-label">Alasan Reject:</label>
                                                                                    <textarea class="form-control" id="doc-remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <button type="submit" class="button btn btn-danger">Reject</a>
                                                                                </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- vlcpm status slain 0, tidak dapat approve / reject dokumen dari vlcadmin --}}
                                                    @elseif(Auth::user()->jobdesk == "vlcpm" && $d->doc_status >= 1)
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Approve</a>
                                                            <a href="#" class="button btn btn-danger disabled">Reject</a>
                                                        </div>

                                                        {{-- tkminframanager status 2, dapat approve / reject dokumen dari vlcpm --}}
                                                    @elseif((Auth::user()->jobdesk == "tkminframanager" && $d->doc_status == 2))
                                                        <div class="ui-group-buttons">
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve_2_{{ $d->doc_no }}">Approve</button>

                                                            <!-- Modal HTML -->
                                                            <div id="approve_2_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk Approve Dokumen?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk Approve Dokumen?</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <a href="{{ url('report/approve-2/'. $d->doc_no) }}" class="btn btn-primary">Approve</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="or"></div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_2_{{ $d->doc_no }}">Reject</button>

                                                            <!-- Modal HTML -->
                                                            <div id="reject_2_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="{{ url('report/reject/' . $d->doc_no . '/' . Auth::user()->id) }}" method="GET">
                                                                                <div class="form-group">
                                                                                    <label for="doc-remarks" class="col-form-label">Alasan Reject:</label>
                                                                                    <textarea class="form-control" id="doc-remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <button type="submit" class="button btn btn-danger">Reject</a>
                                                                                </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- tkminframanager selain status 2, tidak dapat approve / reject dokumen dari vlcpm --}}
                                                    @elseif((Auth::user()->jobdesk == "tkminframanager" && $d->doc_status > 2))
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Approve</a>
                                                            <a href="#" class="button btn btn-danger disabled">Reject</a>
                                                        </div>

                                                        {{-- reviewertkmrto status 3, dapat approve / reject dokumen dari tkminframanager --}}
                                                    @elseif((Auth::user()->jobdesk == "reviewertkmrto" && $d->doc_status == 3))
                                                        <div class="ui-group-buttons">
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve_3_{{ $d->doc_no }}">Approve</button>

                                                            <!-- Modal HTML -->
                                                            <div id="approve_3_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk Approve Dokumen?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk Approve Dokumen?</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <a href="{{ url('report/approve-3/'. $d->doc_no) }}" class="btn btn-primary">Approve</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="or"></div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_3_{{ $d->doc_no }}">Reject</button>

                                                            <!-- Modal HTML -->
                                                            <div id="reject_3_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="{{ url('report/reject/' . $d->doc_no . '/' . Auth::user()->id) }}" method="GET">
                                                                                <div class="form-group">
                                                                                    <label for="doc-remarks" class="col-form-label">Alasan Reject:</label>
                                                                                    <textarea class="form-control" id="doc-remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <button type="submit" class="button btn btn-danger">Reject</a>
                                                                                </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- reviewertkmrto selain status 3, tidak dapat approve / reject dokumen dari tkminframanager --}}
                                                    @elseif((Auth::user()->jobdesk == "reviewertkmrto" && $d->doc_status > 3))
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Approve</a>
                                                            <a href="#" class="button btn btn-danger disabled">Reject</a>
                                                        </div>

                                                        {{-- tkmmanagerns status 4, dapat approve / reject dokumen dari tkmmanagerns --}}
                                                    @elseif((Auth::user()->jobdesk == "tkmmanagerns" && $d->doc_status == 4))
                                                        <div class="ui-group-buttons">
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve_4_{{ $d->doc_no }}">Approve</button>

                                                            <!-- Modal HTML -->
                                                            <div id="approve_4_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk Approve Dokumen?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk Approve Dokumen?</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <a href="{{ url('report/approve-4/'. $d->doc_no) }}" class="btn btn-primary">Approve</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="or"></div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_4_{{ $d->doc_no }}">Reject</button>

                                                            <!-- Modal HTML -->
                                                            <div id="reject_4_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="{{ url('report/reject/' . $d->doc_no . '/' . Auth::user()->id) }}" method="GET">
                                                                                <div class="form-group">
                                                                                    <label for="doc-remarks" class="col-form-label">Alasan Reject:</label>
                                                                                    <textarea class="form-control" id="doc-remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <button type="submit" class="button btn btn-danger">Reject</a>
                                                                                </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- tkmmanagerns selain status 4, tidak dapat approve / reject dokumen dari tkmmanagerns --}}
                                                    @elseif((Auth::user()->jobdesk == "tkmmanagerns" && $d->doc_status > 4))
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Approve</a>
                                                            <a href="#" class="button btn btn-danger disabled">Reject</a>
                                                        </div>

                                                        {{-- tkmcpo status 5, dapat approve / reject dokumen dari tkmstaff --}}
                                                    @elseif((Auth::user()->jobdesk == "tkmcpo" && $d->doc_status == 5))
                                                        <div class="ui-group-buttons">
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve_5_{{ $d->doc_no }}">Approve</button>

                                                            <!-- Modal HTML -->
                                                            <div id="approve_5_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk Approve Dokumen?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk Approve Dokumen?</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <a href="{{ url('report/approve-5/'. $d->doc_no) }}" class="btn btn-primary">Approve</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="or"></div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_5_{{ $d->doc_no }}">Reject</button>

                                                            <!-- Modal HTML -->
                                                            <div id="reject_5_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="{{ url('report/reject-tkmcpo/' . $d->doc_no . '/' . Auth::user()->id) }}" method="GET">
                                                                                <div class="form-group">
                                                                                    <label for="doc-remarks" class="col-form-label">Alasan Reject:</label>
                                                                                    <textarea class="form-control" id="doc-remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <button type="submit" class="button btn btn-danger">Reject</a>
                                                                                </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- tkmcpo selain status 5, tidak dapat approve / reject dokumen dari tkmstaff --}}
                                                    @elseif((Auth::user()->jobdesk == "tkmcpo" && $d->doc_status > 5))
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Approve</a>
                                                            <div class="or"></div>
                                                            <a href="#" class="button btn btn-danger disabled">Reject</a>
                                                        </div>

                                                        {{-- tkmmanagercpo status 6, dapat approve / reject dokumen dari tkmstaff --}}
                                                    @elseif((Auth::user()->jobdesk == "tkmmanagercpo" && $d->doc_status == 6))
                                                        <div class="ui-group-buttons">
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve_6_{{ $d->doc_no }}">Approve</button>

                                                            <!-- Modal HTML -->
                                                            <div id="approve_6_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Yakin untuk Approve Dokumen?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>Anda yakin untuk Approve Dokumen?</p>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <a href="{{ url('report/approve-6/'. $d->doc_no) }}" class="btn btn-primary">Approve</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="or"></div>
                                                            <!-- Button trigger modal -->
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#reject_6_{{ $d->doc_no }}">Reject</button>

                                                            <!-- Modal HTML -->
                                                            <div id="reject_6_{{ $d->doc_no }}" class="modal fade">
                                                                <div class="modal-dialog modal-dialog-scrollable">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title">Anda yakin untuk Reject?</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form action="{{ url('report/reject-tkmmanagercpo/' . $d->doc_no . '/' . Auth::user()->id) }}" method="GET">
                                                                                <div class="form-group">
                                                                                    <label for="doc-remarks" class="col-form-label">Alasan Reject:</label>
                                                                                    <textarea class="form-control" id="doc-remarks" name="doc_remarks" rows="20" placeholder="Berikan Alasan reject..." required></textarea>
                                                                                </div>
                                                                        </div>
                                                                        <div class="modal-footer justify-content-center">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                            <button type="submit" class="button btn btn-danger">Reject</a>
                                                                                </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        {{-- tkmmanagercpo selain status 6, tidak dapat approve / reject dokumen dari tkmstaff --}}
                                                    @elseif((Auth::user()->jobdesk == "tkmmanagercpo" && $d->doc_status > 6))
                                                        <div class="ui-group-buttons">
                                                            <a href="#" class="btn btn-primary disabled">Approve</a>
                                                            <a href="#" class="button btn btn-danger disabled">Reject</a>
                                                        </div>

                                                        {{-- status: 7 = smua tidak dapat mengedit / approve karena smua sudah acc dan di ttd --}}
                                                    @endif
                                                </td>
                                            @endif


                                        </tr>
                                        {{--
                                        **************************************
                                        * End modified by Setiawan Joko      *
                                        **************************************
                                        --}}
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready( function () {
            $('#report_table').DataTable({
                stateSave: true,
                dom: 'Blfrtip',
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10', '25', '50', 'All' ]
                ],
                buttons:[
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                        }
                    },
                ]
            });
        } );
    </script>
@endsection
