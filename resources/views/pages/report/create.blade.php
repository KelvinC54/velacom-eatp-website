@extends('layouts.main')

@section('title','Create ATP Report')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

<div class="section-body mt-2">
	<div class="container-fluid">
		<h1 class="page-title">Create ATP</h1>
		<div class="tab-content mt-2">
			<form action="{{ url('report/') }}" method="POST" enctype="multipart/form-data">
				{{csrf_field()}}
				<div class="step step-1 active">
					<div class="card">
						<div class="card-header text-white bg-info">File BoQ & BAPA</div>
						<div class="card-body">
							<div class="form-group">
								<label>File BoQ</label>
								<input type="file" accept="application/pdf" class="form-control @error('boq') is-invalid @enderror" id="boq" name="boq" autofocus required>

								@error('boq')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
							<div class="form-group">
								<label>File BAPA</label>
								<input type="file" accept="application/pdf" class="form-control @error('bapa') is-invalid @enderror" id="bapa" name="bapa" autofocus required>

								@error('bapa')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
							<div class="form-group">
								<label>File Tambahan</label>
								<input type="file" accept="application/pdf" class="form-control @error('bapa') is-invalid @enderror" id="tambahan" name="tambahan" autofocus>

								@error('tambahan')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="card-footer">
							<button type="button" name="" class="next-btn btn btn-success float-right">Next</button>
						</div>
					</div>
				</div>

				<div class="step step-2">
					<div class="card">
						<div class="card-header text-white bg-info">Basic Information</div>
						<div class="card-body">
							<div class="form-group">
								<label>Document Title</label>
								<input type="text" name="doc_name" class="form-control">
							</div>

							<div class="form-group">
								<label>Document Subtitle</label>
								<input type="text" name="doc_info" class="form-control">
							</div>

							<div class="form-group">
								<label>Project</label>
								<input type="text" name="project_name" class="form-control">
							</div>

							<div class="form-group">
								<label>Project No</label>
								<input type="text" name="project_id" class="form-control">
							</div>
							
							<div class="form-group">
                                <div><label>Document Type</label></div>
                                <select class="form-control" id="" name="doc_type">
                                    <option value="0" selected>Battery</option>
                                    <option value="1">Rect Batt</option>
                                </select>
                            </div>

							<div class="form-group">
								<label>Location</label>
								<input type="text" name="site_loc" class="form-control">
							</div>

							<div class="form-group">
								<label>Site Name</label>
								<input type="text" name="site_name" class="form-control">
							</div>

							<div class="form-group">
								<label>Site ID</label>
								<input type="text" name="site_id" class="form-control">
							</div>

							<div class="form-group">
								<label>Area</label>
								<input type="text" name="site_area" class="form-control">
							</div>
						</div>
						<div class="card-footer">
							<button type="button" name="" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-3">
					<div class="card">
						<div class="card-header text-white bg-info">Rectifier System</div>
						<div class="card-body">
							<div class="form-group">
								<label>Rectifier Series</label>
								<input type="text" name="rectifier_series" class="form-control">
							</div>

							<div class="form-group">
								<label>Rectifier Type</label>
								<input type="text" name="rectifier_type" class="form-control">
							</div>

							<div class="form-group">
								<label>Rectifier Capacity</label>
								<input type="text" name="rectifier_capacity" class="form-control">
							</div>

							<div class="form-group">
								<label>Voltage Input</label>
								<input type="text" name="voltage_input" class="form-control">
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-4">
					<div class="card">
						<div class="card-header text-white bg-info">Visual Check - 1.1 System</div>
						<div class="card-body">
							<div class="form-group">
								<label>1. Rack :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rack Type</label>
											<input type="text" name="rack_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div><label>Condition</label></div>
										{{-- <select class="form-control @error('rack_type_cond') is-invalid @enderror" id="" name="rack_type_cond"> --}}
										<select class="form-control" id="" name="rack_type_cond">
											<option value="NULL">Choose...</option>
											<option value="OK">OK</option>
											<option value="NOK">NOK</option>
										</select>
										{{-- @error('rack_type_cond')
											<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror --}}
										{{-- <div class="form-group">

											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rack_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rack_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div>
										</div> --}}
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Rack Dimension (l x w x h - mm)</label>
											<input type="text" name="rack_dimension" class="form-control">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>2. Name Plate :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Telkomsel Name Plate</label>
											<input type="text" name="tsel_name_plate" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="tsel_name_plate_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="tsel_name_plate_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="tsel_name_plate_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Vendor Name Plate</label>
											<input type="text" name="vendor_name_plate" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="vendor_name_plate_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="vendor_name_plate_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="vendor_name_plate_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>3. Incoming AC Distribution :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Incoming AC cable type & size</label>
											<input type="text" name="ac_cable_type_size" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ac_cable_type_size_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_cable_type_size_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_cable_type_size_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MBC Brand</label>
											<input type="text" name="ac_mcb_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ac_mcb_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_mcb_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_mcb_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Type</label>
											<input type="text" name="ac_mcb_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ac_mcb_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_mcb_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_mcb_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Rate (Amp)</label>
											<input type="text" name="ac_mcb_rate" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ac_mcb_rate_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_mcb_rate_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ac_mcb_rate_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>4. Rectifier Module Distribution :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MBC Brand</label>
											<input type="text" name="rec_mcb_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="rec_mcb_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mcb_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mcb_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Type</label>
											<input type="text" name="rec_mcb_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="rec_mcb_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mcb_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mcb_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Rate (Amp)</label>
											<input type="text" name="rec_mcb_rate" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="rec_mcb_rate_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mcb_rate_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mcb_rate_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>5. Battery Distribution :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB / NH-Fuse Brand</label>
											<input type="text" name="batt_mcb_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_mcb_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB / NH-Fuse Type</label>
											<input type="text" name="batt_mcb_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_mcb_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_type_cond" id="inlineRadio2" value="OK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB / NH-Fuse Rate (Amp)</label>
											<input type="text" name="batt_mcb_rate" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_mcb_rate_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_rate_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_rate_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB / NH-Fuse Quantity</label>
											<input type="text" name="batt_mcb_quantity" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_mcb_quantity_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_quantity_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_mcb_quantity_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>6. Load Distribution :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MBC Brand</label>
											<input type="text" name="load_mcb_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="load_mcb_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Type</label>
											<input type="text" name="load_mcb_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="load_mcb_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Rate (Amp) x Quantity…..[1]</label>
											<input type="text" name="load_mcb_rate_1" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="load_mcb_rate_1_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_rate_1_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_rate_1_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Rate (Amp) x Quantity…..[2]</label>
											<input type="text" name="load_mcb_rate_2" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="load_mcb_rate_2_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_rate_2_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_rate_2_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>MCB Rate (Amp) x Quantity…..[3]</label>
											<input type="text" name="load_mcb_rate_3" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="load_mcb_rate_3_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_rate_3_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="load_mcb_rate_3_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>7. Incoming Arrester :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Arrester Brand</label>
											<input type="text" name="arrester_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="arrester_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="arrester_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="arrester_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Arrester Type</label>
											<input type="text" name="arrester_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="arrester_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="arrester_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="arrester_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>8. Subrack System Module :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Subrack Quantity</label>
											<input type="text" name="subtrack_quantity" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="subtrack_quantity_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_quantity_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_quantity_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>S/N Subrack #1</label>
											<input type="text" name="subtrack_serial_number_1" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="subtrack_serial_number_1_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_serial_number_1_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_serial_number_1_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>S/N Subrack #2</label>
											<input type="text" name="subtrack_serial_number_2" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="subtrack_serial_number_2_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_serial_number_2_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_serial_number_2_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>S/N Subrack #3</label>
											<input type="text" name="subtrack_serial_number_3" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="subtrack_serial_number_3_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_serial_number_3_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="subtrack_serial_number_3_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>9. Battery Rack (separated) :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery Rack Type</label>
											<input type="text" name="batt_rack_type" class="form-control">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rack dimension (l x w x h - mm)</label>
											<input type="text" name="batt_rack_dimension" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_rack_dimension_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_rack_dimension_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_rack_dimension_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Capacity per Rack (bank & block)</label>
											<input type="text" name="batt_rack_capacity_bank_block" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_rack_capacity_bank_block_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_rack_capacity_bank_block_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_rack_capacity_bank_block_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery Rack Quantity</label>
											<input type="text" name="batt_rack_quantity" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_rack_quantity_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_rack_quantity_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_rack_quantity_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label>10. Battery Security system :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery Cage</label>
											<input type="text" name="batt_cage" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_cage_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_cage_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_cage_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Limit switch alarm</label>
											<input type="text" name="batt_limit_switch_alarm" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_limit_switch_alarm_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_limit_switch_alarm_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_limit_switch_alarm_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

							</div>

							<div class="form-group">
								<label>11. DDF for Alarm :</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>DDF wiring/connection</label>
											<input type="text" name="ddf_wiring" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ddf_wiring_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ddf_wiring_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ddf_wiring_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>DDF Alarm labeling</label>
											<input type="text" name="ddf_alarm" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ddf_alarm_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ddf_alarm_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ddf_alarm_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-5">
					<div class="card">
						<div class="card-header text-white bg-info">Visual Check - 1.2 Monitor & Control Card</div>
						<div class="card-body">
							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Monitor & Control Card Type</label>
											<input type="text" name="monitor_control_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="monitor_control_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="monitor_control_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="monitor_control_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Serial number of Monitor & Control Card</label>
											<input type="text" name="monitor_control_serial_number" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="monitor_control_serial_number_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="monitor_control_serial_number_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="monitor_control_serial_number_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-6">
					<div class="card">
						<div class="card-header text-white bg-info">Visual Check - 1.3 Battery</div>
						<div class="card-body">
							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery Brand</label>
											<input type="text" name="batt_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery Type</label>
											<input type="text" name="batt_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Total Bank Battery</label>
											<input type="text" name="batt_bank" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_bank_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_bank_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_bank_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery physical Condition</label>
											<input type="text" name="batt_phy_cond" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_phy_cond_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_phy_cond_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_phy_cond_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery S/N Label</label>
											<input type="text" name="batt_serial_number_label" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_serial_number_label_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_serial_number_label_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_serial_number_label_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Battery Bank Label</label>
											<input type="text" name="batt_bank_label" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="batt_bank_label_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_bank_label_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="batt_bank_label_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="form-group">
									<label>Serial Number Battery</label>
									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label>Battery 1 S/N :</label>
												<input type="text" name="batt_sn_1" class="form-control">
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Bank :</label>
												<input type="text" name="batt_sn_1_bank" class="form-control">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label>Battery 2 S/N :</label>
												<input type="text" name="batt_sn_2" class="form-control">
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Bank :</label>
												<input type="text" name="batt_sn_2_bank" class="form-control">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label>Battery 3 S/N :</label>
												<input type="text" name="batt_sn_3" class="form-control">
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Bank :</label>
												<input type="text" name="batt_sn_3_bank" class="form-control">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label>Battery 4 S/N :</label>
												<input type="text" name="batt_sn_4" class="form-control">
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Bank :</label>
												<input type="text" name="batt_sn_4_bank" class="form-control">
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-6">
											<div class="form-group">
												<label>Battery 5 S/N :</label>
												<input type="text" name="batt_sn_5" class="form-control">
											</div>
										</div>
										<div class="col-6">
											<div class="form-group">
												<label>Bank :</label>
												<input type="text" name="batt_sn_5_bank" class="form-control">
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-7">
					<div class="card">
						<div class="card-header text-white bg-info">Visual Check - 1.4 Rectifier Module</div>
						<div class="card-body">
							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier Module Brand</label>
											<input type="text" name="rec_mod_brand" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="rec_mod_brand_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mod_brand_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mod_brand_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier Module Type</label>
											<input type="text" name="rec_mod_type" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="rec_mod_type_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mod_type_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mod_type_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Module Capacity (Watt @ 48 Vdc)</label>
											<input type="text" name="rec_mod_capacity" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="rec_mod_capacity_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mod_capacity_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="rec_mod_capacity_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>Serial Number module rectifier</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 1 S/N :</label>
											<input type="text" name="rec_sn1" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn1_phase" class="form-control">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 2 S/N :</label>
											<input type="text" name="rec_sn2" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn2_phase" class="form-control">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 3 S/N :</label>
											<input type="text" name="rec_sn3" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn3_phase" class="form-control">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 4 S/N :</label>
											<input type="text" name="rec_sn4" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn4_phase" class="form-control">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 5 S/N :</label>
											<input type="text" name="rec_sn5" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn5_phase" class="form-control">
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 6 S/N :</label>
											<input type="text" name="rec_sn6" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn6_phase" class="form-control">
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 7 S/N :</label>
											<input type="text" name="rec_sn7" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn7_phase" class="form-control">
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 8 S/N :</label>
											<input type="text" name="rec_sn8" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn8_phase" class="form-control">
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Rectifier 9 S/N :</label>
											<input type="text" name="rec_sn9" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<label>Phase :</label>
											<input type="text" name="rec_sn9_phase" class="form-control">
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-8">
					<div class="card">
						<div class="card-header text-white bg-info">Visual Check - 1.5 Installation</div>
						<div class="card-body">
							<div class="form-group">
								<label>1. Rectifier</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation AC cable to rectifier</label>
											<input type="text" name="inst_ac_to_rec" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_ac_to_rec_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_ac_to_rec_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_ac_to_rec_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation Alarm cable to DDF Rectifier</label>
											<input type="text" name="inst_alarm_to_rec" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_alarm_to_rec_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_alarm_to_rec_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_alarm_to_rec_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Labelling of DDF Alarm</label>
											<input type="text" name="label_ddf" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="label_ddf_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="label_ddf_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="label_ddf_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation Blank Panel Module</label>
											<input type="text" name="inst_blank_panel" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_blank_panel_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_blank_panel_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_blank_panel_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>2. Battery</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation Battery Position</label>
											<input type="text" name="inst_batt_pos" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_batt_pos_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_batt_pos_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_batt_pos_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation Battery Bank Cable</label>
											<input type="text" name="inst_batt_bank" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_batt_bank_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_batt_bank_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_batt_bank_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation Battery Temperature Sensor</label>
											<input type="text" name="inst_batt_temp" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_batt_temp_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_batt_temp_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_batt_temp_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>3. SDPAC</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation AC cable to rectifier</label>
											<input type="text" name="inst_ac_to_rec_sdpac" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_ac_to_rec_sdpac_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_ac_to_rec_sdpac_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_ac_to_rec_sdpac_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Arrester Installation</label>
											<input type="text" name="inst_arrester" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_arrester_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_arrester_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_arrester_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>4. Grounding</label>
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Grounding cable size (mm)</label>
											<input type="text" name="ground_cable_size" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="ground_cable_size_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ground_cable_size_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="ground_cable_size_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<label>Installation grounding cable to Rectifier</label>
											<input type="text" name="inst_ground_to_rectifier" class="form-control">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<div><label>Condition</label></div>
											<select class="form-control" id="" name="inst_ground_to_rectifier_cond">
												<option value="NULL">Choose...</option>
												<option value="OK">OK</option>
												<option value="NOK">NOK</option>
											</select>
											{{-- <div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_ground_to_rectifier_cond" id="inlineRadio1" value="OK">
												<label class="form-check-label" for="inlineRadio1">OK</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="radio" name="inst_ground_to_rectifier_cond" id="inlineRadio2" value="NOK">
												<label class="form-check-label" for="inlineRadio2">NOK</label>
											</div> --}}
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>

				<div class="step step-9">
					<div class="card">
						<div class="card-header text-white bg-info">Functional - 2.1 Monitor & Control Card</div>
						<div class="card-body">
							<div class="form-group">
								<label>1. Current Display</label>
								<br>
								<label>Rectifier</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_cdisplay_rectifier_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_cdisplay_rectifier_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Load</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_cdisplay_load_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_cdisplay_load_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Battery</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_cdisplay_battery_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_cdisplay_battery_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>2. Current Measurement</label>
								<br>
								<label>Load</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_cmeasure_load_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_cmeasure_load_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Battery</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_cmeasure_batt_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_cmeasure_batt_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>3. Bus Voltage Display</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_bus_volt_display_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_bus_volt_display_res" class="form-control">
										</div>
									</div>
							    </div>
								<label>Bus Voltage Measurement</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_bus_volt_measure_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_bus_volt_measure_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>4. AC Voltage Display</label>
								<br>
								<label>V phase - Netral</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_ac_volt_display_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_ac_volt_display_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>5. AC Voltage Measurement</label>
								<br>
								<label>V phase R – Netral</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_ac_volt_measure_v_phase_r_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_ac_volt_measure_v_phase_r_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>V phase S – Netral</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_ac_volt_measure_v_phase_s_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_ac_volt_measure_v_phase_s_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>V phase T – Netral</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_ac_volt_measure_v_phase_t_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_ac_volt_measure_v_phase_t_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>V Netral – Ground</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_ac_volt_measure_v_netral_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_ac_volt_measure_v_netral_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>6. LVD Setting</label>
								<br>
								<label>LVD1 Disconnect (LVLD)</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_lvd_1_dc_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_lvd_1_dc_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>LVD1 Reconnect (LVLD)</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_lvd_1_rc_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_lvd_1_rc_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>LVD2 Disconnect (LVBD)</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_lvd_2_dc_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_lvd_2_dc_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>LVD2 Reconnect (LVBD)</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_lvd_2_rc_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_lvd_2_rc_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>7. Alarm Setting</label>
								<br>
								<label>High Float</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_high_float_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_high_float_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Low Float</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_low_float_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_low_float_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Low Load</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_low_load_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_low_load_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>AC Fail Alarm</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_ac_fail_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_ac_fail_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Partial AC Fail</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_partial_ac_fail_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_partial_ac_fail_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Battery Fuse Fail</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_battery_fuse_fail_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_battery_fuse_fail_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Rectifier Fail Alarm</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_rectifier_fail_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_rectifier_fail_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Battery high Temperature</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_battery_high_temp_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_battery_high_temp_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

							<div class="form-group">
								<label>8. Alarm Relay Mapping & Simulation</label>
								<br>
								<label>Relay #1</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_1_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_1_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #2</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_2_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_2_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #3</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_3_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_3_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #4</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_4_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_4_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #5</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_5_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_5_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #6</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_6_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_6_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #7</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_7_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_7_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #8</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_8_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_8_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>

								<label>Relay #9</label>
								<div class="row">
									<div class="col-4">
										<div class="form-group">
											<label>Reference</label>
											<input type="text" name="func_mc_alarm_relay_9_ref" class="form-control">
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>Result</label>
											<input type="text" name="func_mc_alarm_relay_9_res" class="form-control">
										</div>
									</div>
									{{-- <div class="col-4">
										<div class="form-group">
											<label>Condition / Rem</label>
											<input type="text" name="" class="form-control">
										</div>
									</div> --}}
								</div>
							</div>

						</div>
						<div class="card-footer">
							<button type="submit" class="btn btn-success float-right">Submit</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
			</form>

			{{-- Photo Upload --}}
			<form>
				<div class="step step-10">
					<div class="card">
						<div class="card-header text-white bg-info">General Information Photo</div>
						<div class="card-body">
							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Site Information</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Site Access</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Site - Front View</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Site - Rear View</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Site - Left View</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Site - Right View</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>


							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
			</form>

			<form>
				<div class="step step-11">
					<div class="card">
						<div class="card-header text-white bg-info">New Foundation Installation Photo</div>
						<div class="card-body">
							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Before Installation</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Installation #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Installation #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Installation #3</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Installation #4</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Installation #5</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Installation #6</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Installation #7</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Foundation Wide Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Foundation Height Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Foundation Long Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>After Installation</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
			</form>

			<form>
				<div class="step step-12">
					<div class="card">
						<div class="card-header text-white bg-info">Site Layout</div>
						<div class="card-body">
							<div class="form-group">
								<p>Site Layout</p>
								<input type="file">
								<input type="button" class="btn btn-info" value="Upload">
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
			</form>

			<form>
				<div class="step step-13">
					<div class="card">
						<div class="card-header text-white bg-info">Rectifier Installation Photo</div>
						<div class="card-body">
							<div class="form-group">
								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Base Frame Before Installation</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Base Frame After Installation</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Main Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Slave Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>PLN KWH - Before</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>PLN KWH - After</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>ACPDB - Before</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>ACPDB - After</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>MCB ACPDB - Before</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>MCB ACPDB - After</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Rectifier Front</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Main MCB AC Input Rectifier</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>MCB DC Distribution</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Controller Module</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Module #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Module #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Module #3</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Module #4</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>DC Cable Connection – Main Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>DC Cable p – Main Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>DC Cable Connection – Slave Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>DC Cable p – Slave Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>GND Connection #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>GND Connection #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>GND Connection #3</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>GND Connection #4</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Laying Cable #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Laying Cable #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Laying Cable #3</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Laying Cable #4</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Fan – Main Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Fan – Slave rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Lamp – Main Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Lamp – Slave Rack</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Panel Distribution CDC</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (R – S)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (S – T)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (R – T)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (R – N)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (S – N)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (T – N)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>AC Measurement  (N – G)</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>DC Output/Distribution Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>AC Output Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Controller Module Setting #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Controller Module Setting #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Controller Module Setting #3</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Controller Module Setting #4</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Alarm Connection #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Alarm Connection #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Alarm p #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Alarm p #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Battery Rack - Front View</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Battery Bank Installation #1</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Battery Bank Installation #2</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Battery Bank Installation #3</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Battery Bank #1 DC Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Battery Bank #2 DC Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Battery Bank #3 DC Measurement</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Battery p</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-6">
										<div class="form-group">
											<p>Battery Main Bar installation</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
									<div class="col-6">
										<div class="form-group">
											<p>Battery MCB/Fuse</p>
											<input type="file">
											<input type="button" class="btn btn-info" value="Upload">
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="next-btn btn btn-success float-right">Next</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
			</form>

			<form>
				<div class="step step-14">
					<div class="card">
						<div class="card-header text-white bg-info">Site Layout</div>
						<div class="card-body">
							<div class="form-group">
								<p>Site Layout</p>
								<input type="file">
								<input type="button" class="btn btn-info" value="Upload">
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="btn btn-success float-right">Submit</button>
							<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

const steps = Array.from(document.querySelectorAll("form .step"));
const nextBtn = document.querySelectorAll("form .next-btn");
const prevBtn = document.querySelectorAll("form .previous-btn");
const form = document.querySelector("form");

nextBtn.forEach((button) => {
	button.addEventListener("click", () => {
		changeStep("next");
	});
});
prevBtn.forEach((button) => {
	button.addEventListener("click", () => {
		changeStep("prev");
	});
});

form.addEventListener("submit", (e) => {
	e.preventDefault();
	const inputs = [];
	form.querySelectorAll("input").forEach((input) => {
		const { name, value } = input;
		inputs.push({ name, value });
	});
	console.log(inputs);
	form.reset();
});

function changeStep(btn) {
	let index = 0;
	const active = document.querySelector(".active");
	index = steps.indexOf(active);
	steps[index].classList.remove("active");
	if (btn === "next") {
		index++;
	} else if (btn === "prev") {
		index--;
	}
	steps[index].classList.add("active");
}

</script>

<script src="assets/bundles/lib.vendor.bundle.js"></script>
<script src="assets/plugins/dropify/js/dropify.min.js"></script>
<script src="assets/js/dropify.js"></script>

@endsection
