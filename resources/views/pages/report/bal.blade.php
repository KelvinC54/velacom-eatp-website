<?php
	$vlcpm = DB::table('users')
		->select('sign')
		->where('jobdesk', 'vlcpm')
		->value('sign');
	$tkminframanager = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkminframanager')
		->value('sign');
	$tkmmanagerns = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkmmanagerns')
		->value('sign');
	$reviewertkmrto = DB::table('users')
		->select('sign')
		->where('jobdesk', 'reviewertkmrto')
		->value('sign');
	$tkmmanagercpo = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkmmanagercpo')
		->value('sign');
	$tkmcpo = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkmcpo')
		->value('sign');
?>
<!DOCTYPE html>
<html>
<head>
	<title>BAL-Report-{{ $documents->doc_no }}</title>
<style>
table.tb-list td {
  border: 1px solid black;
  padding: 2px;
}

table.tb-list {
  width: 100%;
  border-collapse: collapse;
}
</style>
</head>
<body>
	<h2 style="text-align: center;">BERITA ACARA LAPANGAN (BAL)</h2>
	<table>
		<tr>
			<td>Pekerjaan</td>
			<td>:</td>
			<td>{{$documents->doc_info}}</td>
		</tr>
		<tr>
			<td>Site ID/Site Name</td>
			<td>:</td>
			<td>{{$documents->site_id}} / {{$documents->site_name}}</td>
		</tr>
		<tr>
			<td>Regional</td>
			<td>:</td>
			<td>{{$documents->site_loc}}</td>
		</tr>
	</table>
	<hr>
	<p style="text-align: justify;">Pada hari ini {{$documents->doc_date}}, berdasarkan Kontrak Nomor :  {{$documents->project_id}} Telkominfra telah menyelesaikan PO {{$documents->doc_info}} dan Telkomsel menyatakan bahwa site tersebut di atas telah diterima dengan status OA (On Air).</p>
	<p>Dengan hasil :</p>
	<h4 style="text-align: center;">BAIK DAN SESUAI DENGAN RUANG LINGKUP PEKERJAAN</h4>
	<p>Serah Terima Dokumen Pendukung :</p>
	<table class="tb-list" style="width: 30%;">
		<tr>
			<td style="text-align: center;"><img src="{{ url('images/cek.jpg') }}" alt="V" width="25" height="25"></td>
			<td>Document ATP</td>
		</tr>
		<tr>
			<td style="text-align: center;"><img src="{{ url('images/cek.jpg') }}" alt="V" width="25" height="25"></td>
			<td>Foto Instalasi </td>
		</tr>
		<tr>
			<td style="text-align: center;"><img src="{{ url('images/cek.jpg') }}" alt="V" width="25" height="25"></td>
			<td>Site Layout</td>
		</tr>
		<tr>
			<td></td>
			<td>.........................</td>
		</tr>
		<tr>
			<td></td>
			<td>.........................</td>
		</tr>
		<tr>
			<td></td>
			<td>.........................</td>
		</tr>
		<tr>
			<td></td>
			<td>.........................</td>
		</tr>
	</table>
	<p>Demikian Berita Acara ini dibuat untuk kepentingan kedua belah pihak dalam rangka penerbitan Berita Acara Uji Terima dan Berita Acara Serah Terima.</p>
	<br>
	@if($documents->doc_type == 1)
	<table style="width: 100%">
		<tr>
			<td style="width: 50%;text-align: center;">
				<p><b>PT. Telekomunikasi Selular</b></p>
				@if($documents->doc_status >= 10)
					<br>
					<img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
					{{--<img src="{{ public_path('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
					<p>(Edo Hapsoro)<br>_________________________</p>
				@else
					<br><br><br><br><br>
					<p>_________________________</p>
				@endif
			</td>
			<td style="text-align: center;">
				<p><b>PT. Infrastruktur Telekomunikasi Indonesia</b></p>
				@if($documents->doc_status >= 8)
					<br>
					<img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
					{{--<img src="{{ public_path('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
					<p>(Budi Setiawan)<br>_________________________</p>
				@else
					<br><br><br><br><br>
					<p>_________________________</p>
				@endif
			</td>
		</tr>
	</table>
	@else
    	<table style="width: 100%">
    		<tr>
    			<td style="width: 50%;text-align: center;">
    				<p><b>PT. Telekomunikasi Selular</b></p>
    				@if($documents->doc_status >= 7)
    					<br>
    					<img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
    					{{--<img src="{{ public_path('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
    					<p>(Matius Lamba)<br>_________________________</p>
    				@else
    					<br><br><br><br><br>
    					<p>_________________________</p>
    				@endif
    			</td>
    			<td style="text-align: center;">
    				<p><b>PT. Infrastruktur Telekomunikasi Indonesia</b></p>
    				@if($documents->doc_status >= 3)
    					<br>
    					<img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
    					{{--<img src="{{ public_path('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
    					<p>(Budi Setiawan)<br>_________________________</p>
    				@else
    					<br><br><br><br><br>
    					<p>_________________________</p>
    				@endif
    			</td>
    		</tr>
    	</table>
	@endif
</body>
</html>
