<?php

	$vlcpm = DB::table('users')
		->select('sign')
		->where('jobdesk', 'vlcpm')
		->value('sign');
	$tkminframanager = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkminframanager')
		->value('sign');
	$tkmmanagerns = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkmmanagerns')
		->value('sign');
	$reviewertkmrto = DB::table('users')
		->select('sign')
		->where('jobdesk', 'reviewertkmrto')
		->value('sign');
	$tkmmanagercpo = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkmmanagercpo')
		->value('sign');
	$tkmcpo = DB::table('users')
		->select('sign')
		->where('jobdesk', 'tkmcpo')
		->value('sign');

	// Preview Photo
	$gi_site_information = DB::table('photos')
		->select('gi_site_information')
		->where('doc_no', $documents->doc_no)
		->value('gi_site_information');
	$gi_site_access = DB::table('photos')
		->select('gi_site_access')
		->where('doc_no', $documents->doc_no)
		->value('gi_site_access');
	$gi_site_front_view = DB::table('photos')
		->select('gi_site_front_view')
		->where('doc_no', $documents->doc_no)
		->value('gi_site_front_view');
	$gi_site_rear_view = DB::table('photos')
		->select('gi_site_rear_view')
		->where('doc_no', $documents->doc_no)
		->value('gi_site_rear_view');
	$gi_site_left_view = DB::table('photos')
		->select('gi_site_left_view')
		->where('doc_no', $documents->doc_no)
		->value('gi_site_left_view');
	$gi_site_right_view = DB::table('photos')
		->select('gi_site_right_view')
		->where('doc_no', $documents->doc_no)
		->value('gi_site_right_view');
	$nf_installation_before = DB::table('photos')
		->select('nf_installation_before')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_before');
	$nf_installation_1 = DB::table('photos')
		->select('nf_installation_1')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_1');
	$nf_installation_2 = DB::table('photos')
		->select('nf_installation_2')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_2');
	$nf_installation_3 = DB::table('photos')
		->select('nf_installation_3')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_3');
	$nf_installation_4 = DB::table('photos')
		->select('nf_installation_4')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_4');
	$nf_installation_5 = DB::table('photos')
		->select('nf_installation_5')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_5');
	$nf_installation_6 = DB::table('photos')
		->select('nf_installation_6')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_6');
	$nf_installation_7 = DB::table('photos')
		->select('nf_installation_7')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_7');
	$nf_foundation_wide = DB::table('photos')
		->select('nf_foundation_wide')
		->where('doc_no', $documents->doc_no)
		->value('nf_foundation_wide');
	$nf_foundation_weight = DB::table('photos')
		->select('nf_foundation_weight')
		->where('doc_no', $documents->doc_no)
		->value('nf_foundation_weight');
	$nf_foundation_long = DB::table('photos')
		->select('nf_foundation_long')
		->where('doc_no', $documents->doc_no)
		->value('nf_foundation_long');
	$nf_installation_after = DB::table('photos')
		->select('nf_installation_after')
		->where('doc_no', $documents->doc_no)
		->value('nf_installation_after');
	$site_layout = DB::table('photos')
		->select('site_layout')
		->where('doc_no', $documents->doc_no)
		->value('site_layout');
	$ri_base_frame_before = DB::table('photos')
		->select('ri_base_frame_before')
		->where('doc_no', $documents->doc_no)
		->value('ri_base_frame_before');
	$ri_base_frame_after = DB::table('photos')
		->select('ri_base_frame_after')
		->where('doc_no', $documents->doc_no)
		->value('ri_base_frame_after');
	$ri_main_rack_1 = DB::table('photos')
		->select('ri_main_rack_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_main_rack_1');
	$ri_slave_rack_1 = DB::table('photos')
		->select('ri_slave_rack_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_slave_rack_1');
	$ri_main_rack_2 = DB::table('photos')
		->select('ri_main_rack_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_main_rack_2');
	$ri_slave_rack_2 = DB::table('photos')
		->select('ri_slave_rack_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_slave_rack_2');
	$ri_pln_kwh_before = DB::table('photos')
		->select('ri_pln_kwh_before')
		->where('doc_no', $documents->doc_no)
		->value('ri_pln_kwh_before');
	$ri_pln_kwh_after = DB::table('photos')
		->select('ri_pln_kwh_after')
		->where('doc_no', $documents->doc_no)
		->value('ri_pln_kwh_after');
	$ri_acpdb_before = DB::table('photos')
		->select('ri_acpdb_before')
		->where('doc_no', $documents->doc_no)
		->value('ri_acpdb_before');
	$ri_acpdb_after = DB::table('photos')
		->select('ri_acpdb_after')
		->where('doc_no', $documents->doc_no)
		->value('ri_acpdb_after');
	$ri_acpdb_mcb_before = DB::table('photos')
		->select('ri_acpdb_mcb_before')
		->where('doc_no', $documents->doc_no)
		->value('ri_acpdb_mcb_before');
	$ri_acpdb_mcb_after = DB::table('photos')
		->select('ri_acpdb_mcb_after')
		->where('doc_no', $documents->doc_no)
		->value('ri_acpdb_mcb_after');
	$ri_rectifier_front = DB::table('photos')
		->select('ri_rectifier_front')
		->where('doc_no', $documents->doc_no)
		->value('ri_rectifier_front');
	$ri_main_mcb_ac_input = DB::table('photos')
		->select('ri_main_mcb_ac_input')
		->where('doc_no', $documents->doc_no)
		->value('ri_main_mcb_ac_input');
	$ri_mcb_dc_dist = DB::table('photos')
		->select('ri_mcb_dc_dist')
		->where('doc_no', $documents->doc_no)
		->value('ri_mcb_dc_dist');
	$ri_controller_module = DB::table('photos')
		->select('ri_controller_module')
		->where('doc_no', $documents->doc_no)
		->value('ri_controller_module');
	$ri_module_1 = DB::table('photos')
		->select('ri_module_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_module_1');
	$ri_module_2 = DB::table('photos')
		->select('ri_module_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_module_2');
	$ri_module_3 = DB::table('photos')
		->select('ri_module_3')
		->where('doc_no', $documents->doc_no)
		->value('ri_module_3');
	$ri_module_4 = DB::table('photos')
		->select('ri_module_4')
		->where('doc_no', $documents->doc_no)
		->value('ri_module_4');
	$ri_dc_cable_con_main_rack = DB::table('photos')
		->select('ri_dc_cable_con_main_rack')
		->where('doc_no', $documents->doc_no)
		->value('ri_dc_cable_con_main_rack');
	$ri_dc_cable_label_main_rack = DB::table('photos')
		->select('ri_dc_cable_label_main_rack')
		->where('doc_no', $documents->doc_no)
		->value('ri_dc_cable_label_main_rack');
	$ri_dc_cable_con_slave_rack = DB::table('photos')
		->select('ri_dc_cable_con_slave_rack')
		->where('doc_no', $documents->doc_no)
		->value('ri_dc_cable_con_slave_rack');
	$ri_dc_cable_label_slave_rack = DB::table('photos')
		->select('ri_dc_cable_label_slave_rack')
		->where('doc_no', $documents->doc_no)
		->value('ri_dc_cable_label_slave_rack');
	$ri_gnd_con_1 = DB::table('photos')
		->select('ri_gnd_con_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_gnd_con_1');
	$ri_gnd_con_2 = DB::table('photos')
		->select('ri_gnd_con_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_gnd_con_2');
	$ri_gnd_con_3 = DB::table('photos')
		->select('ri_gnd_con_3')
		->where('doc_no', $documents->doc_no)
		->value('ri_gnd_con_3');
	$ri_gnd_con_4 = DB::table('photos')
		->select('ri_gnd_con_4')
		->where('doc_no', $documents->doc_no)
		->value('ri_gnd_con_4');
	$ri_laying_cable_1 = DB::table('photos')
		->select('ri_laying_cable_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_laying_cable_1');
	$ri_laying_cable_2 = DB::table('photos')
		->select('ri_laying_cable_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_laying_cable_2');
	$ri_laying_cable_3 = DB::table('photos')
		->select('ri_laying_cable_3')
		->where('doc_no', $documents->doc_no)
		->value('ri_laying_cable_3');
	$ri_laying_cable_4 = DB::table('photos')
		->select('ri_laying_cable_4')
		->where('doc_no', $documents->doc_no)
		->value('ri_laying_cable_4');
	$ri_fan_main = DB::table('photos')
		->select('ri_fan_main')
		->where('doc_no', $documents->doc_no)
		->value('ri_fan_main');
	$ri_fan_slave = DB::table('photos')
		->select('ri_fan_slave')
		->where('doc_no', $documents->doc_no)
		->value('ri_fan_slave');
	$ri_lamp_main = DB::table('photos')
		->select('ri_lamp_main')
		->where('doc_no', $documents->doc_no)
		->value('ri_lamp_main');
	$ri_lamp_slave = DB::table('photos')
		->select('ri_lamp_slave')
		->where('doc_no', $documents->doc_no)
		->value('ri_lamp_slave');
	$ri_panel_dist = DB::table('photos')
		->select('ri_panel_dist')
		->where('doc_no', $documents->doc_no)
		->value('ri_panel_dist');
	$ri_ac_measure_rs = DB::table('photos')
		->select('ri_ac_measure_rs')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_rs');
	$ri_ac_measure_st = DB::table('photos')
		->select('ri_ac_measure_st')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_st');
	$ri_ac_measure_rt = DB::table('photos')
		->select('ri_ac_measure_rt')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_rt');
	$ri_ac_measure_rn = DB::table('photos')
		->select('ri_ac_measure_rn')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_rn');
	$ri_ac_measure_sn = DB::table('photos')
		->select('ri_ac_measure_sn')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_sn');
	$ri_ac_measure_tn = DB::table('photos')
		->select('ri_ac_measure_tn')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_tn');
	$ri_ac_measure_ng = DB::table('photos')
		->select('ri_ac_measure_ng')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_measure_ng');
	$ri_dc_output = DB::table('photos')
		->select('ri_dc_output')
		->where('doc_no', $documents->doc_no)
		->value('ri_dc_output');
	$ri_ac_output = DB::table('photos')
		->select('ri_ac_output')
		->where('doc_no', $documents->doc_no)
		->value('ri_ac_output');
	$ri_control_module_setting_1 = DB::table('photos')
		->select('ri_control_module_setting_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_control_module_setting_1');
	$ri_control_module_setting_2 = DB::table('photos')
		->select('ri_control_module_setting_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_control_module_setting_2');
	$ri_control_module_setting_3 = DB::table('photos')
		->select('ri_control_module_setting_3')
		->where('doc_no', $documents->doc_no)
		->value('ri_control_module_setting_3');
	$ri_control_module_setting_4 = DB::table('photos')
		->select('ri_control_module_setting_4')
		->where('doc_no', $documents->doc_no)
		->value('ri_control_module_setting_4');
	$ri_alarm_connection_1 = DB::table('photos')
		->select('ri_alarm_connection_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_alarm_connection_1');
	$ri_alarm_connection_2 = DB::table('photos')
		->select('ri_alarm_connection_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_alarm_connection_2');
	$ri_alarm_label_1 = DB::table('photos')
		->select('ri_alarm_label_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_alarm_label_1');
	$ri_alarm_label_2 = DB::table('photos')
		->select('ri_alarm_label_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_alarm_label_2');
	$ri_battery_rack = DB::table('photos')
		->select('ri_battery_rack')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_rack');
	$ri_battery_bank_inst_1 = DB::table('photos')
		->select('ri_battery_bank_inst_1')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_inst_1');
	$ri_battery_bank_inst_2 = DB::table('photos')
		->select('ri_battery_bank_inst_2')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_inst_2');
	$ri_battery_bank_inst_3 = DB::table('photos')
		->select('ri_battery_bank_inst_3')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_inst_3');
	$ri_battery_bank_inst_4 = DB::table('photos')
		->select('ri_battery_bank_inst_4')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_inst_4');
	$ri_battery_bank_1_dc_measure = DB::table('photos')
		->select('ri_battery_bank_1_dc_measure')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_1_dc_measure');
	$ri_battery_bank_2_dc_measure = DB::table('photos')
		->select('ri_battery_bank_2_dc_measure')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_2_dc_measure');
	$ri_battery_bank_3_dc_measure = DB::table('photos')
		->select('ri_battery_bank_3_dc_measure')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_3_dc_measure');
	$ri_battery_bank_4_dc_measure = DB::table('photos')
		->select('ri_battery_bank_4_dc_measure')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_bank_4_dc_measure');
	$ri_battery_label = DB::table('photos')
		->select('ri_battery_label')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_label');
	$ri_battery_main_bar_inst = DB::table('photos')
		->select('ri_battery_main_bar_inst')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_main_bar_inst');
	$ri_battery_mcb_fuse = DB::table('photos')
		->select('ri_battery_mcb_fuse')
		->where('doc_no', $documents->doc_no)
		->value('ri_battery_mcb_fuse');
	$site_layout_rectifier = DB::table('photos')
		->select('site_layout_rectifier')
		->where('doc_no', $documents->doc_no)
		->value('site_layout_rectifier');
?>
<html>
<head>
	<title>ATP-Report-{{ $documents->doc_no }}</title>
<style>
table.tb-border, table.tb-border td {
  border: 1px solid black;
  padding: 2px;
}

table.tb-border {
  width: 100%;
  border-collapse: collapse;
}
.page_break {
	page-break-before: always;
}
.container {
	position: absolute;
	top: 50%;
	left: 50%;
	-moz-transform: translateX(-50%) translateY(-50%);
	-webkit-transform: translateX(-50%) translateY(-50%);
	transform: translateX(-50%) translateY(-50%);
}
</style>
</head>
<body>
	<table class="tb-border">
				<tr>
					<td rowspan="3" style="width: 20%"><img src="{{url('/assets/images/TelkomInfra.jpg')}}" width="130"></td>
					<td style="text-align: center;">{{$documents->doc_name}}</td>
					<td style="width: 10%">Doc. No. </td>
					<td style="width: 10%">1.0</td>
					<td rowspan="3" style="width: 20%"><img src="{{url('/assets/images/Tsel_New_Logo.png')}}" width="130"></td>
				</tr>
				<tr>
					<td style="text-align: center;"><b>TECHNICAL TEST PROTOCOL</b></td>
					<td>Version</td>
					<td>1.0</td>
				</tr>
				<tr>
					<td style="text-align: center;">{{$documents->doc_info}}</td>
					<td>Date</td>
					<td></td>
				</tr>
	</table>
	<table class="tb-border">
		<tr>
			<td>
				<p>Project : {{$documents->project_name}}</p>
				<p>No. Project : {{$documents->project_id}}</p>
			</td>
			<td>
				<p>Location	: {{$documents->site_name}}</p>
				<p>Site ID	: {{$documents->site_id}}</p>
			</td>
			<td>
				<p>Area : {{$documents->site_area}}</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<h5>RECTIFIER SYSTEM</h5>
				<p>Rectifier Series	P/N : {{$documents->rectifier_series}}</p>
				<p>Rectifier Type S/N : {{$documents->rectifier_type}}</p>
				<p>Rectifier Capacity : {{$documents->rectifier_capacity}}</p>
				<p>Voltage Input : {{$documents->voltage_input}}</p>
			</td>
			<td>
				<h3>V = OK</h3>
				<h3>X = NOK</h3>
			</td>
		</tr>
	</table>

	<h5>1. VISUAL CHECK</h5>
	<h5>1.1 SYSTEM</h5>
	<table class="tb-border">
		<tr>
			<td>NO</td>
			<td>DESCRIPTION</td>
			<td>RESULT</td>
			<td>CONDITION</td>
			<td>REMARKS</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Rack Type:</td>
			<td>{{$documents->rack_type}}</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Rack dimension (l x w x h - mm)</td>
			<td>{{$documents->rack_dimension}}</td>
			<td>{{$documents->rack_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Name Plate :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Telkomsel Name Plate</td>
			<td>{{$documents->tsel_name_plate}}</td>
			<td>{{$documents->tsel_name_plate_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Vendor Name Plate</td>
			<td>{{$documents->vendor_name_plate}}</td>
			<td>{{$documents->vendor_name_plate_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Incoming AC Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		
		@if($documents->doc_type == 1)
		<tr>
			<td></td>
			<td>Incoming AC cable type & size</td>
			<td>{{$documents->ac_cable_type_size}}</td>
			<td>{{$documents->ac_cable_type_size_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MBC Brand</td>
			<td>{{$documents->ac_mcb_brand}}</td>
			<td>{{$documents->ac_mcb_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Type</td>
			<td>{{$documents->ac_mcb_type}}</td>
			<td>{{$documents->ac_mcb_type_cond}}</td>
			<td></td>

		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp)</td>
			<td>{{$documents->ac_mcb_rate}}</td>
			<td>{{$documents->ac_mcb_rate_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>4</td>
			<td>Rectifier Module Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MBC Brand</td>
			<td>{{$documents->rec_mcb_brand}}</td>
			<td>{{$documents->rec_mcb_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Type</td>
			<td>{{$documents->rec_mcb_type}}</td>
			<td>{{$documents->rec_mcb_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp)</td>
			<td>{{$documents->rec_mcb_rate}}</td>
			<td>{{$documents->rec_mcb_rate_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>5</td>
			<td>Battery Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Brand</td>
			<td>{{$documents->batt_mcb_brand}}</td>
			<td>{{$documents->batt_mcb_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Type</td>
			<td>{{$documents->batt_mcb_type}}</td>
			<td>{{$documents->batt_mcb_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Rate (Amp)</td>
			<td>{{$documents->batt_mcb_rate}}</td>
			<td>{{$documents->batt_mcb_rate_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Quantity</td>
			<td>{{$documents->batt_mcb_quantity}}</td>
			<td>{{$documents->batt_mcb_quantity_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>6</td>
			<td>Load Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MBC Brand</td>
			<td>{{$documents->load_mcb_brand}}</td>
			<td>{{$documents->load_mcb_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Type</td>
			<td>{{$documents->load_mcb_type}}</td>
			<td>{{$documents->load_mcb_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp) x Quantity…..[1]</td>
			<td>{{$documents->load_mcb_rate_1}}</td>
			<td>{{$documents->load_mcb_rate_1_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp) x Quantity…..[2]</td>
			<td>{{$documents->load_mcb_rate_2}}</td>
			<td>{{$documents->load_mcb_rate_2_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp) x Quantity…..[3]</td>
			<td>{{$documents->load_mcb_rate_3}}</td>
			<td>{{$documents->load_mcb_rate_3_cond}}</td>
			<td></td>
		</tr>
		@else
		<tr>
			<td></td>
			<td>Incoming AC cable type & size</td>
			<td></td>
			<td>{{$documents->ac_cable_type_size_cond}}</td>
			<td>{{$documents->ac_cable_type_size}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MBC Brand</td>
			<td></td>
			<td>{{$documents->ac_mcb_brand_cond}}</td>
			<td>{{$documents->ac_mcb_brand}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Type</td>
			<td></td>
			<td>{{$documents->ac_mcb_type_cond}}</td>
			<td>{{$documents->ac_mcb_type}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp)</td>
			<td></td>
			<td>{{$documents->ac_mcb_rate_cond}}</td>
			<td>{{$documents->ac_mcb_rate}}</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Rectifier Module Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MBC Brand</td>
			<td></td>
			<td>{{$documents->rec_mcb_brand_cond}}</td>
			<td>{{$documents->rec_mcb_brand}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Type</td>
			<td></td>
			<td>{{$documents->rec_mcb_type_cond}}</td>
			<td>{{$documents->rec_mcb_type}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp)</td>
			<td></td>
			<td>{{$documents->rec_mcb_rate_cond}}</td>
			<td>{{$documents->rec_mcb_rate}}</td>
		</tr>
		<tr>
			<td>5</td>
			<td>Battery Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Brand</td>
			<td></td>
			<td>{{$documents->batt_mcb_brand_cond}}</td>
			<td>{{$documents->batt_mcb_brand}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Type</td>
			<td></td>
			<td>{{$documents->batt_mcb_type_cond}}</td>
			<td>{{$documents->batt_mcb_type}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Rate (Amp)</td>
			<td></td>
			<td>{{$documents->batt_mcb_rate_cond}}</td>
			<td>{{$documents->batt_mcb_rate}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB / NH-Fuse Quantity</td>
			<td></td>
			<td>{{$documents->batt_mcb_quantity_cond}}</td>
			<td>{{$documents->batt_mcb_quantity}}</td>
		</tr>
		<tr>
			<td>6</td>
			<td>Load Distribution :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>MBC Brand</td>
			<td></td>
			<td>{{$documents->load_mcb_brand_cond}}</td>
			<td>{{$documents->load_mcb_brand}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Type</td>
			<td></td>
			<td>{{$documents->load_mcb_type_cond}}</td>
			<td>{{$documents->load_mcb_type}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp) x Quantity…..[1]</td>
			<td></td>
			<td>{{$documents->load_mcb_rate_1_cond}}</td>
			<td>{{$documents->load_mcb_rate_1}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp) x Quantity…..[2]</td>
			<td></td>
			<td>{{$documents->load_mcb_rate_2_cond}}</td>
			<td>{{$documents->load_mcb_rate_2}}</td>
		</tr>
		<tr>
			<td></td>
			<td>MCB Rate (Amp) x Quantity…..[3]</td>
			<td></td>
			<td>{{$documents->load_mcb_rate_3_cond}}</td>
			<td>{{$documents->load_mcb_rate_3}}</td>
		</tr>
		@endif
		
		<tr>
			<td>7</td>
			<td>Incoming Arrester :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Arrester Brand :</td>
			<td>{{$documents->arrester_brand}}</td>
			<td>{{$documents->arrester_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Arrester Type :</td>
			<td>{{$documents->arrester_type}}</td>
			<td>{{$documents->arrester_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>8</td>
			<td>Subrack System Module</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Subrack Quantity</td>
			<td>{{$documents->subtrack_quantity}}</td>
			<td>{{$documents->subtrack_quantity_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>S/N Subrack #1</td>
			<td>{{$documents->subtrack_serial_number_1}}</td>
			<td>{{$documents->subtrack_serial_number_1_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>S/N Subrack #2</td>
			<td>{{$documents->subtrack_serial_number_2}}</td>
			<td>{{$documents->subtrack_serial_number_2_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>S/N Subrack #3</td>
			<td>{{$documents->subtrack_serial_number_3}}</td>
			<td>{{$documents->subtrack_serial_number_3_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>9</td>
			<td>Battery Rack Type (separated) :</td>
			<td>{{$documents->batt_rack_type}}</td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Rack dimension (l x w x h - mm)</td>
			<td>{{$documents->batt_rack_dimension}}</td>
			<td>{{$documents->batt_rack_dimension_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Capacity per Rack (bank & block)</td>
			<td>{{$documents->batt_rack_capacity_bank_block}}</td>
			<td>{{$documents->batt_rack_capacity_bank_block_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery Rack Quantity</td>
			<td>{{$documents->batt_rack_quantity}}</td>
			<td>{{$documents->batt_rack_quantity_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>10</td>
			<td>Battery Security system :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery Cage</td>
			<td>{{$documents->batt_cage}}</td>
			<td>{{$documents->batt_cage_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Limit switch alarm</td>
			<td>{{$documents->batt_limit_switch_alarm}}</td>
			<td>{{$documents->batt_limit_switch_alarm_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>11</td>
			<td>DDF for Alarm :</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>DDF wiring/connection</td>
			<td>{{$documents->ddf_wiring}}</td>
			<td>{{$documents->ddf_wiring_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>DDF Alarm labeling</td>
			<td>{{$documents->ddf_alarm}}</td>
			<td>{{$documents->ddf_alarm_cond}}</td>
			<td></td>
		</tr>
	</table>

	<h5>1.2 Monitor & Control Card</h5>
	<table class="tb-border">
		<tr>
			<td>NO</td>
			<td>DESCRIPTION</td>
			<td>RESULT</td>
			<td>CONDITION</td>
			<td>REMARKS</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Monitor & Control Card Type</td>
			<td>{{$documents->monitor_control_type}}</td>
			<td>{{$documents->monitor_control_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Serial number of Monitor & Control Card</td>
			<td>{{$documents->monitor_control_serial_number}}</td>
			<td>{{$documents->monitor_control_serial_number_cond}}</td>
			<td></td>
		</tr>
	</table>

	<h5>1.3 BATTERY</h5>
	<table class="tb-border">
		<tr>
			<td>NO</td>
			<td>DESCRIPTION</td>
			<td>RESULT</td>
			<td>CONDITION</td>
			<td>REMARKS</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Battery Brand</td>
			<td>{{$documents->batt_brand}}</td>
			<td>{{$documents->batt_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery Brand</td>
			<td>{{$documents->batt_type}}</td>
			<td>{{$documents->batt_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Total Bank Battery</td>
			<td>{{$documents->batt_bank}}</td>
			<td>{{$documents->batt_bank_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Battery physical Condition</td>
			<td>{{$documents->batt_phy_cond}}</td>
			<td>{{$documents->batt_phy_cond_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>4</td>
			<td>Battery S/N Label</td>
			<td>{{$documents->batt_serial_number_label}}</td>
			<td>{{$documents->batt_serial_number_label_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>5</td>
			<td>Battery Bank Label</td>
			<td>{{$documents->batt_bank_label}}</td>
			<td>{{$documents->batt_bank_label_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>6</td>
			<td>Serial Number Battery</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery 1 ; S/N : {{$documents->batt_sn_1}}</td>
			<td>Bank #{{$documents->batt_sn_1_bank}}</td>
			<td>Battery 6 ; S/N : _________________</td>
			<td>Bank #</td>
		</tr>
		<tr>
			<td></td>
			<td>Battery 2 ; S/N : {{$documents->batt_sn_2}}</td>
			<td>Bank #{{$documents->batt_sn_2_bank}}</td>
			<td>Battery 7 ; S/N : _________________</td>
			<td>Bank #</td>
		</tr>
		<tr>
			<td></td>
			<td>Battery 3 ; S/N : {{$documents->batt_sn_3}}</td>
			<td>Bank #{{$documents->batt_sn_3_bank}}</td>
			<td>Battery 8 ; S/N : _________________</td>
			<td>Bank #</td>
		</tr>
		<tr>
			<td></td>
			<td>Battery 4 ; S/N : {{$documents->batt_sn_4}}</td>
			<td>Bank #{{$documents->batt_sn_4_bank}}</td>
			<td>Battery 9 ; S/N : _________________</td>
			<td>Bank #</td>
		</tr>
		<tr>
			<td></td>
			<td>Battery 5 ; S/N : {{$documents->batt_sn_5}}</td>
			<td>Bank #{{$documents->batt_sn_5_bank}}</td>
			<td>Battery 10 ; S/N : _________________</td>
			<td>Bank #</td>
		</tr>
	</table>

	<h5>1.4 RECTIFIER MODULE</h5>
	<table class="tb-border">
		<tr>
			<td>NO</td>
			<td>DESCRIPTION</td>
			<td>RESULT</td>
			<td>CONDITION</td>
			<td>REMARKS</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Rectifier Module Brand</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier Module Type</td>
			<td>{{$documents->rec_mod_brand}}</td>
			<td>{{$documents->rec_mod_brand_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Module Capacity (Watt @ 48 Vdc)</td>
			<td>{{$documents->rec_mod_type}}</td>
			<td>{{$documents->rec_mod_type_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Serial Number module rectifier</td>
			<td>{{$documents->rec_mod_capacity}}</td>
			<td>{{$documents->rec_mod_capacity_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier 1 ; S/N : {{$documents->rec_sn1}}</td>
			<td>Phase : {{$documents->rec_sn1_phase}}</td>
			<td>Rectifier 6 ; S/N : {{$documents->rec_sn6}}</td>
			<td>Phase : {{$documents->rec_sn6_phase}}</td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier 2 ; S/N : {{$documents->rec_sn2}}</td>
			<td>Phase : {{$documents->rec_sn2_phase}}</td>
			<td>Rectifier 7 ; S/N : {{$documents->rec_sn7}}</td>
			<td>Phase : {{$documents->rec_sn7_phase}}</td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier 3 ; S/N : {{$documents->rec_sn3}}</td>
			<td>Phase : {{$documents->rec_sn3_phase}}</td>
			<td>Rectifier 8 ; S/N : {{$documents->rec_sn8}}</td>
			<td>Phase : {{$documents->rec_sn8_phase}}</td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier 4 ; S/N : {{$documents->rec_sn4}}</td>
			<td>Phase : {{$documents->rec_sn4_phase}}</td>
			<td>Rectifier 9 ; S/N : {{$documents->rec_sn9}}</td>
			<td>Phase : {{$documents->rec_sn9_phase}}</td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier 5 ; S/N : {{$documents->rec_sn5}}</td>
			<td>Phase : {{$documents->rec_sn5_phase}}</td>
			<td>Rectifier 10 ; S/N : ________________</td>
			<td>Phase : </td>
		</tr>
	</table>

	<h5>1.5 INSTALLATION</h5>
	<table class="tb-border">
		<tr>
			<td>NO</td>
			<td>DESCRIPTION</td>
			<td>RESULT</td>
			<td>CONDITION</td>
			<td>REMARKS</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Rectifier</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation AC cable to rectifier</td>
			<td>{{$documents->inst_ac_to_rec}}</td>
			<td>{{$documents->inst_ac_to_rec_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation Alarm cable to DDF Rectifier</td>
			<td>{{$documents->inst_alarm_to_rec}}</td>
			<td>{{$documents->inst_alarm_to_rec_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Labelling of DDF Alarm</td>
			<td>{{$documents->label_ddf}}</td>
			<td>{{$documents->label_ddf_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation Blank Panel Module</td>
			<td>{{$documents->inst_blank_panel}}</td>
			<td>{{$documents->inst_blank_panel_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Battery</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation Battery Position</td>
			<td>{{$documents->inst_batt_pos}}</td>
			<td>{{$documents->inst_batt_pos_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation Battery Bank Cable </td>
			<td>{{$documents->inst_batt_bank}}</td>
			<td>{{$documents->inst_batt_bank_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation Battery Temperature Sensor </td>
			<td>{{$documents->inst_batt_temp}}</td>
			<td>{{$documents->inst_batt_temp_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>3</td>
			<td>SDPAC</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation AC cable to rectifier</td>
			<td>{{$documents->inst_ac_to_rec_sdpac}}</td>
			<td>{{$documents->inst_ac_to_rec_sdpac_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Arrester Installation</td>
			<td>{{$documents->inst_arrester}}</td>
			<td>{{$documents->inst_arrester_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td>4</td>
			<td>Grounding</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Grounding cable size (mm)</td>
			<td>{{$documents->ground_cable_size}}</td>
			<td>{{$documents->ground_cable_size_cond}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Installation grounding cable to Rectifier</td>
			<td>{{$documents->inst_ground_to_rectifier}}</td>
			<td>{{$documents->inst_ground_to_rectifier_cond}}</td>
			<td></td>
		</tr>
	</table>

	<h5>2. FUNCTIONAL</h5>
	<h5>2.1 MONITOR & CONTROL CARD</h5>
	<table class="tb-border">
		<tr>
			<td>NO</td>
			<td>DESCRIPTION</td>
			<td>RESULT</td>
			<td>CONDITION</td>
			<td>REMARKS</td>
		</tr>
		<tr>
			<td>1</td>
			<td>Current Display</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier</td>
			<td>{{$documents->func_mc_cdisplay_rectifier_ref}}</td>
			<td>{{$documents->func_mc_cdisplay_rectifier_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Load</td>
			<td>{{$documents->func_mc_cdisplay_load_ref}}</td>
			<td>{{$documents->func_mc_cdisplay_load_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery </td>
			<td>{{$documents->func_mc_cdisplay_battery_ref}}</td>
			<td>{{$documents->func_mc_cdisplay_battery_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Current Measurement</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Load</td>
			<td>{{$documents->func_mc_cmeasure_load_ref}}</td>
			<td>{{$documents->func_mc_cmeasure_load_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery </td>
			<td>{{$documents->func_mc_cmeasure_batt_ref}}</td>
			<td>{{$documents->func_mc_cmeasure_batt_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Bus Voltage Display</td>
			<td>{{$documents->func_mc_bus_volt_display_ref}}</td>
			<td>{{$documents->func_mc_bus_volt_display_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Bus Voltage Measurement</td>
			<td>{{$documents->func_mc_bus_volt_measure_ref}}</td>
			<td>{{$documents->func_mc_bus_volt_measure_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td>4</td>
			<td>AC Voltage Display (V phase - Netral)</td>
			<td>{{$documents->func_mc_ac_volt_display_ref}}</td>
			<td>{{$documents->func_mc_ac_volt_display_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td>5</td>
			<td>AC Voltage Measurement</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>V phase R – Netral</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_phase_r_ref}}</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_phase_r_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>V phase S – Netral</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_phase_s_ref}}</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_phase_s_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>V phase T – Netral</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_phase_t_ref}}</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_phase_t_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>V Netral – Ground</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_netral_ref}}</td>
			<td>{{$documents->func_mc_ac_volt_measure_v_netral_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td>6</td>
			<td>LVD Setting</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>LVD1 Disconnect (LVLD)</td>
			<td>{{$documents->func_mc_lvd_1_dc_ref}}</td>
			<td>{{$documents->func_mc_lvd_1_dc_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>LVD1 Reconnect (LVLD)</td>
			<td>{{$documents->func_mc_lvd_1_rc_ref}}</td>
			<td>{{$documents->func_mc_lvd_1_rc_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>LVD2 Disconnect (LVBD)</td>
			<td>{{$documents->func_mc_lvd_2_dc_ref}}</td>
			<td>{{$documents->func_mc_lvd_2_dc_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>LVD2 Reconnect (LVBD)</td>
			<td>{{$documents->func_mc_lvd_2_rc_ref}}</td>
			<td>{{$documents->func_mc_lvd_2_rc_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td>7</td>
			<td>Alarm Setting </td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>High Float</td>
			<td>{{$documents->func_mc_alarm_high_float_ref}}</td>
			<td>{{$documents->func_mc_alarm_high_float_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Low Float</td>
			<td>{{$documents->func_mc_alarm_low_float_ref}}</td>
			<td>{{$documents->func_mc_alarm_low_float_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Low Load</td>
			<td>{{$documents->func_mc_alarm_low_load_ref}}</td>
			<td>{{$documents->func_mc_alarm_low_load_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>AC Fail Alarm</td>
			<td>{{$documents->func_mc_alarm_ac_fail_ref}}</td>
			<td>{{$documents->func_mc_alarm_ac_fail_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Partial AC Fail</td>
			<td>{{$documents->func_mc_alarm_partial_ac_fail_ref}}</td>
			<td>{{$documents->func_mc_alarm_partial_ac_fail_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery Fuse Fail</td>
			<td>{{$documents->func_mc_alarm_battery_fuse_fail_ref}}</td>
			<td>{{$documents->func_mc_alarm_battery_fuse_fail_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Rectifier Fail Alarm</td>
			<td>{{$documents->func_mc_alarm_rectifier_fail_ref}}</td>
			<td>{{$documents->func_mc_alarm_rectifier_fail_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Battery high Temperature</td>
			<td>{{$documents->func_mc_alarm_battery_high_temp_ref}} C</td>
			<td>{{$documents->func_mc_alarm_battery_high_temp_res}} C</td>
			<td></td>
		</tr>
		<tr>
			<td>8</td>
			<td>Alarm Relay Mapping & Simulation</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #1</td>
			<td>{{$documents->func_mc_alarm_relay_1_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_1_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #2</td>
			<td>{{$documents->func_mc_alarm_relay_2_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_2_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #3</td>
			<td>{{$documents->func_mc_alarm_relay_3_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_3_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #4</td>
			<td>{{$documents->func_mc_alarm_relay_4_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_4_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #5</td>
			<td>{{$documents->func_mc_alarm_relay_5_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_5_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #6</td>
			<td>{{$documents->func_mc_alarm_relay_6_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_6_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #7</td>
			<td>{{$documents->func_mc_alarm_relay_7_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_7_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #8</td>
			<td>{{$documents->func_mc_alarm_relay_8_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_8_res}}</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>Relay #9</td>
			<td>{{$documents->func_mc_alarm_relay_9_ref}}</td>
			<td>{{$documents->func_mc_alarm_relay_9_res}}</td>
			<td></td>
		</tr>
	</table>

	<p>Test  Date : …………..................</p>

    @if ($documents->doc_type == 1)
        <table style="width: 100%">
                <tr>
                    <td style="width: 50%;text-align: center;">
                        <p><b>PT. Telkominfra</b></p>
                        @if($documents->doc_status >= 8)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_infra}}</p>
                            <p>(Budi Setiawan)<br>Telkom Infra<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                    <td style="width: 50%;text-align: center;">
                        <p><b>PT. Telkomsel INDONESIA</b></p>
                        @if($documents->doc_status >= 10)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_pm_rect_batt}}</p>
                            <p>(Edo Hapsoro)<br>Manager Project Management Kalimantan<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;text-align: center;">
                        @if($documents->doc_status >= 2)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $vlcpm) }}" alt="Sign {{ $vlcpm }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_pm}}</p>
                            <p>(Dindin)<br>Telkom Infra<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                            @endif
                    </td>
                    <td style="width: 50%;text-align: center;">
                        @if($documents->doc_status >= 9)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkmcpo) }}" alt="Sign {{ $tkmcpo }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_rev_rect_batt}}</p>
                            <p>(Ridwan Kadir)<br>Project Management Kalimantan<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                </tr>
        </table>
    @else
        <table style="width: 100%">
                <tr>
                    <td style="width: 50%;text-align: center;">
                        <p><b>PT. Telkominfra</b></p>
                        @if($documents->doc_status >= 3)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_infra}}</p>
                            <p>(Budi Setiawan)<br>Telkom Infra<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                    <td style="width: 50%;text-align: center;">
                        <p><b>PT. Telkomsel Indonesia</b></p>
                        @if($documents->doc_status >= 5)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkmmanagerns) }}" alt="Sign {{ $tkmmanagerns }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_ns}}</p>
                            @if($documents->site_area == "NS PALANGKARAYA")
                            <p>(Dana Utama)<br>Manager NS PALANGKARAYA<br>_________________________</p>
                            @elseif($documents->site_area == "NS TARAKAN")
                            <p>(Zulkifli Farma)<br>Manager NS TARAKAN<br>_________________________</p>
                            @elseif($documents->site_area == "NS SAMARINDA")
                            <p>(Sahat V. Silalahi)<br>Manager NS SAMARINDA<br>_________________________</p>
                            @elseif($documents->site_area == "NS PONTIANAK")
                            <p>(Roni Donbosco Manurung)<br>Manager NS PONTIANAK<br>_________________________</p>
                            @elseif($documents->site_area == "NS BANJARMASIN")
                            <p>(Fredy Siswanto)<br>Manager NS BANJARMASIN<br>_________________________</p>
                            @elseif($documents->site_area == "NS BALIKPAPAN")
                            <p>(Benny Hasiholan Rumahorbo)<br>Manager NS BALIKPAPAN<br>_________________________</p>
                            @else
                            <P>Error site area not found</P>
                            @endif

                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                    <td style="width: 50%;text-align: center;">
                        <p><b>PT. Telkomsel INDONESIA</b></p>
                        @if($documents->doc_status >= 7)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_cpo}}</p>
                            <p>(Matius Lamba)<br>Manager CPO Kalimantan<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;text-align: center;">
                        @if($documents->doc_status >= 2)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $vlcpm) }}" alt="Sign {{ $vlcpm }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_pm}}</p>
                            <p>(Dindin)<br>Telkom Infra<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                            @endif
                    </td>
                    <td style="width: 50%;text-align: center;">
                        @if($documents->doc_status >= 4)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $reviewertkmrto) }}" alt="Sign {{ $reviewertkmrto }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_rtpo}}</p>
                            @if($documents->site_loc == "RTPO BANJARMASIN")
                                <p>(Hendra A. Kuen)<br>RTPO BANJARMASIN<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO BERAU")
                                <p>(Nana Hardiana)<br>RTPO BERAU<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO SAMARINDA")
                                <p>(Victor B. Sitepu)<br>RTPO SAMARINDA<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO TARAKAN")
                                <p>(Agung Rihanto)<br>RTPO TARAKAN<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO SINTANG")
                                <p>(Chatur R. Novianggoro)<br>RTPO SINTANG<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO SAMBAS")
                                <p>(Harun Sarim)<br>RTPO SAMBAS<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO PONTIANAK")
                                <p>(Widodo)<br>RTPO PONTIANAK<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO PALANGKARAYA")
                                <p>(Saprino E. Palumpun)<br>RTPO PALANGKARAYA<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO MARTAPURA")
                                <p>(Aghia P. Fauzan)<br>RTPO MARTAPURA<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO KUTAI")
                                <p>(Muarif F. Akhmad)<br>RTPO KUTAI<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO SAMPIT")
                                <p>(Rahmad T. Wasono)<br>RTPO SAMPIT<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO KOTA BARU")
                                <p>(Tri Yugowibowo)<br>RTPO KOTA BARU<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO KETAPANG KUBURAYA")
                                <p>(Audriyanto Paramata)<br>RTPO KETAPANG KUBURAYA<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO BONTANG")
                                <p>(Muhammad J. Musthofa)<br>RTPO BONTANG<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO BARITO RAYA")
                                <p>(Hartanto)<br>RTPO BARITO RAYA<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO BANUA ENAM")
                                <p>(Juanda I. Gaol)<br>RTPO BANUA ENAM<br>_________________________</p>
                            @elseif($documents->site_loc == "RTPO BALIKPAPAN")
                                <p>(Irwan Hasmin)<br>RTPO BALIKPAPAN<br>_________________________</p>
                            @else
                                <P>ERROR no site location detected</P>
                            @endif

                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                    <td style="width: 50%;text-align: center;">
                        @if($documents->doc_status >= 6)
                            <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="100" height="100">
                            {{--<img src="{{ url('images/sign/' . $tkmcpo) }}" alt="Sign {{ $tkmcpo }}" width="200" height="100">--}}
                            <p>{{$documents->datesign_stafcpo}}</p>
                            <p>(Wihasto D Prasetyo)<br>Staff CPO Kalimantan<br>_________________________</p>
                        @else
                            <br><br><br><br><br>
                            <p>_________________________</p>
                        @endif
                    </td>
                </tr>
        </table>
    @endif

	<div class="page_break"></div>
	<h1 class="container" style="text-align: center;">INSTALLATION PHOTO</h1>
	<div class="page_break"></div>
	<h1 class="container" style="text-align: center;">GENERAL INFORMATION PHOTO</h1>
	<div class="page_break"></div>
	<table class="tb-border" style="width: 100%;text-align: center;">
		<tr>
			@if($gi_site_information == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $gi_site_information) }}" width="300" height="300"></td>
			@endif

			@if($gi_site_access == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $gi_site_access) }}" width="300" height="300"></td>
			@endif

		</tr>
		<tr>
			<td>Site Information</td>
			<td>Site Access</td>
		</tr>
		<tr>
			@if($gi_site_front_view == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $gi_site_front_view) }}" width="300" height="300"></td>
			@endif

			@if($gi_site_rear_view == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $gi_site_rear_view) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Site - Front View</td>
			<td>Site - Rear View</td>
		</tr>
		<tr>
			@if($gi_site_left_view == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $gi_site_left_view) }}" width="300" height="300"></td>
			@endif

			@if($gi_site_right_view == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $gi_site_right_view) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Site - Left View</td>
			<td>Site - Right View</td>
		</tr>
	</table>
    @if ($documents->doc_type == 1)
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 8)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 10)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_pm_rect_batt}}<br>(Edo Hapsoro)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @else
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 3)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 7)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_cpo}}<br>(Matius Lamba)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @endif
	<div class="page_break"></div>
	<h1 class="container" style="text-align: center;">NEW FOUNDATION INSTALLATION PHOTO</h1>
	<div class="page_break"></div>
	<table class="tb-border" style="width: 100%;text-align: center;">
		<tr>
			@if($nf_installation_before == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $nf_installation_before) }}" width="300" height="300"></td>
			@endif

			@if($nf_installation_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $nf_installation_1) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Before Installation</td>
			<td>Installation #1</td>
		</tr>
		<tr>
			@if($nf_installation_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $nf_installation_2) }}" width="300" height="300"></td>
			@endif

			@if($nf_installation_3 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $nf_installation_3) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Installation #2</td>
			<td>Installation #3</td>
		</tr>
		<tr>
			@if($nf_installation_4 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $nf_installation_4) }}" width="300" height="300"></td>
			@endif

			@if($nf_installation_5 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $nf_installation_5) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Installation #4</td>
			<td>Installation #5</td>
		</tr>
		<tr>
			@if($nf_installation_6 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $nf_installation_6) }}" width="300" height="300"></td>
			@endif

			@if($nf_installation_7 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $nf_installation_7) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Installation #6</td>
			<td>Installation #7</td>
		</tr>
		<tr>
			@if($nf_foundation_wide == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $nf_foundation_wide) }}" width="300" height="300"></td>
			@endif

			@if($nf_foundation_weight == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $nf_foundation_weight) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Foundation Wide Measurement</td>
			<td>Foundation Height Measurement</td>
		</tr>
		<tr>
			@if($nf_foundation_long == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $nf_foundation_long) }}" width="300" height="300"></td>
			@endif

			@if($nf_installation_after == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $nf_installation_after) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Foundation Long Measurement</td>
			<td>After Installation</td>
		</tr>
	</table>
	@if ($documents->doc_type == 1)
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 8)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 10)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_pm_rect_batt}}<br>(Edo Hapsoro)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @else
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 3)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 7)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_cpo}}<br>(Matius Lamba)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @endif
	<div class="page_break"></div>
	<h2 style="text-align: center;">SITE LAYOUT</h2>
	<br><br><br><br><br><br><br><br><br><br>
	@if($site_layout == null)
	<br><br><br><br><br><br><br><br><br><br>
	@else
		<img style="text-align : center" src="{{ url('images/report/' . $site_layout) }}" width="300" height="300">
	@endif
	<br><br><br><br><br><br><br><br><br><br>
	@if ($documents->doc_type == 1)
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 8)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 10)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_pm_rect_batt}}<br>(Edo Hapsoro)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @else
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 3)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 7)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_cpo}}<br>(Matius Lamba)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @endif
	<div class="page_break"></div>
	<h1 class="container" style="text-align: center;">RECTIFIER INSTALLATION PHOTO</h1>
	<div class="page_break"></div>
	<table class="tb-border" style="width: 100%;text-align: center;">
		<tr>
			@if($ri_base_frame_before == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_base_frame_before) }}" width="300" height="300"></td>
			@endif

			@if($ri_base_frame_after == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_base_frame_after) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Base Frame Before Installation</td>
			<td>Base Frame After Installation</td>
		</tr>
		<tr>
			@if($ri_main_rack_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_main_rack_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_slave_rack_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_slave_rack_1) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Main Rack</td>
			<td>Slave Rack</td>
		</tr>
		<tr>
			@if($ri_main_rack_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_main_rack_2) }}" width="300" height="300"></td>
			@endif

			@if($ri_slave_rack_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_slave_rack_2) }}" width="300" height="300"></td>
			@endif


		</tr>
		<tr>
			<td>Main Rack</td>
			<td>Slave Rack</td>
		</tr>
		<tr>
			@if($ri_pln_kwh_before == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_pln_kwh_before) }}" width="300" height="300"></td>
			@endif

			@if($ri_pln_kwh_after == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_pln_kwh_after) }}" width="300" height="300"></td>
			@endif

		</tr>
		<tr>
			<td>PLN KWH - Before</td>
			<td>PLN KWH - After</td>
		</tr>
		<tr>
			@if($ri_acpdb_before == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_acpdb_before) }}" width="300" height="300"></td>
			@endif

			@if($ri_acpdb_after == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_acpdb_after) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>ACPDB - Before</td>
			<td>ACPDB - After</td>
		</tr>
		<tr>
			@if($ri_acpdb_mcb_before == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_acpdb_mcb_before) }}" width="300" height="300"></td>
			@endif

			@if($ri_acpdb_mcb_after == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_acpdb_mcb_after) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>MCB ACPDB - Before</td>
			<td>MCB ACPDB - After</td>
		</tr>
		<tr>
			@if($ri_rectifier_front == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_rectifier_front) }}" width="300" height="300"></td>
			@endif

			@if($ri_main_mcb_ac_input == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_main_mcb_ac_input) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Rectifier Front</td>
			<td>Main MCB AC Input Rectifier</td>
		</tr>
		<tr>
			@if($ri_mcb_dc_dist == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_mcb_dc_dist) }}" width="300" height="300"></td>
			@endif

			@if($ri_controller_module == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_controller_module) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>MCB DC Distribution</td>
			<td>Controller Module</td>
		</tr>
		<tr>
			@if($ri_module_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_module_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_module_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_module_2) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Module #1</td>
			<td>Module #2</td>
		</tr>
		<tr>
			@if($ri_module_3 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_module_3) }}" width="300" height="300"></td>
			@endif

			@if($ri_module_4 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_module_4) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Module #3</td>
			<td>Module #4</td>
		</tr>
		<tr>
			@if($ri_dc_cable_con_main_rack == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_dc_cable_con_main_rack) }}" width="300" height="300"></td>
			@endif

			@if($ri_dc_cable_label_main_rack == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_dc_cable_label_main_rack) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>DC Cable Connection – Main Rack</td>
			<td>DC Cable Label – Main Rack</td>
		</tr>
		<tr>
			@if($ri_dc_cable_con_slave_rack == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_dc_cable_con_slave_rack) }}" width="300" height="300"></td>
			@endif

			@if($ri_dc_cable_label_slave_rack == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_dc_cable_label_slave_rack) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>DC Cable Connection – Slave Rack</td>
			<td>DC Cable Label – Slave Rack</td>
		</tr>
		<tr>
			@if($ri_gnd_con_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_gnd_con_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_gnd_con_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_gnd_con_2) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>GND Connection #1</td>
			<td>GND Connection #2</td>
		</tr>
		<tr>
			@if($ri_gnd_con_3 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_gnd_con_3) }}" width="300" height="300"></td>
			@endif

			@if($ri_gnd_con_4 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_gnd_con_4) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>GND Connection #3</td>
			<td>GND Connection #4</td>
		</tr>
		<tr>
			@if($ri_laying_cable_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_laying_cable_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_laying_cable_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_laying_cable_2) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Laying Cable #1</td>
			<td>Laying Cable #2</td>
		</tr>
		<tr>
			@if($ri_laying_cable_3 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_laying_cable_3) }}" width="300" height="300"></td>
			@endif

			@if($ri_laying_cable_4 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_laying_cable_4) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Laying Cable #3</td>
			<td>Laying Cable #4</td>
		</tr>
		<tr>
			@if($ri_fan_main == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_fan_main) }}" width="300" height="300"></td>
			@endif

			@if($ri_fan_slave == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_fan_slave) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Fan – Main Rack</td>
			<td>Fan – Slave rack</td>
		</tr>
		<tr>
			@if($ri_lamp_main == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_lamp_main) }}" width="300" height="300"></td>
			@endif

			@if($ri_lamp_slave == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_lamp_slave) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Lamp – Main Rack</td>
			<td>Lamp – Slave Rack</td>
		</tr>
		<tr>
			@if($ri_panel_dist == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_panel_dist) }}" width="300" height="300"></td>
			@endif

			@if($ri_ac_measure_rs == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_ac_measure_rs) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Panel Distribution CDC</td>
			<td>AC Measurement  (R – S)</td>
		</tr>
		<tr>
			@if($ri_ac_measure_st == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_ac_measure_st) }}" width="300" height="300"></td>
			@endif

			@if($ri_ac_measure_rt == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_ac_measure_rt) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>AC Measurement  (S – T)</td>
			<td>AC Measurement  (R – T)</td>
		</tr>
		<tr>
			@if($ri_ac_measure_rn == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_ac_measure_rn) }}" width="300" height="300"></td>
			@endif

			@if($ri_ac_measure_sn == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_ac_measure_sn) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>AC Measurement  (R – N)</td>
			<td>AC Measurement  (S – N)</td>
		</tr>
		<tr>
			@if($ri_ac_measure_tn == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_ac_measure_tn) }}" width="300" height="300"></td>
			@endif

			@if($ri_ac_measure_ng == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_ac_measure_ng) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>AC Measurement  (T – N)</td>
			<td>AC Measurement  (N – G)</td>
		</tr>
		<tr>
			@if($ri_dc_output == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_dc_output) }}" width="300" height="300"></td>
			@endif

			@if($ri_ac_output == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_ac_output) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>DC Output/Distribution Measurement</td>
			<td>AC Output Measurement</td>
		</tr>
		<tr>
			@if($ri_control_module_setting_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_control_module_setting_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_control_module_setting_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_control_module_setting_2) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Controller Module Setting #1</td>
			<td>Controller Module Setting #2</td>
		</tr>
		<tr>
			@if($ri_control_module_setting_3 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_control_module_setting_3) }}" width="300" height="300"></td>
			@endif

			@if($ri_control_module_setting_4 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_control_module_setting_4) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Controller Module Setting #3</td>
			<td>Controller Module Setting #4</td>
		</tr>
		<tr>
			@if($ri_alarm_connection_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_alarm_connection_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_alarm_connection_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_alarm_connection_2) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Alarm Connection #1</td>
			<td>Alarm Connection #2</td>
		</tr>
		<tr>
			@if($ri_alarm_label_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_alarm_label_1) }}" width="300" height="300"></td>
			@endif

			@if($ri_alarm_label_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_alarm_label_2) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Alarm Label #1</td>
			<td>Alarm Label #2</td>
		</tr>
		<tr>
			@if($ri_battery_rack == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_battery_rack) }}" width="300" height="300"></td>
			@endif

			@if($ri_battery_bank_inst_1 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_battery_bank_inst_1) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Battery Rack - Front View</td>
			<td>Battery Bank Installation #1</td>
		</tr>
		<tr>
			@if($ri_battery_bank_inst_2 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_battery_bank_inst_2) }}" width="300" height="300"></td>
			@endif

			@if($ri_battery_bank_inst_3 == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_battery_bank_inst_3) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Battery Bank Installation #2</td>
			<td>Battery Bank Installation #3</td>
		</tr>
		<tr>
			@if($ri_battery_bank_inst_4 == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_battery_bank_inst_4) }}" width="300" height="300"></td>
			@endif

			@if($ri_battery_bank_1_dc_measure == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_battery_bank_1_dc_measure) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Battery Bank Installation #4</td>
			<td>Battery Bank #1 DC Measurement</td>
		</tr>
		<tr>
			@if($ri_battery_bank_2_dc_measure == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_battery_bank_2_dc_measure) }}" width="300" height="300"></td>
			@endif

			@if($ri_battery_bank_3_dc_measure == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_battery_bank_3_dc_measure) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Battery Bank #2 DC Measurement</td>
			<td>Battery Bank #3 DC Measurement</td>
		</tr>
		<tr>
			@if($ri_battery_bank_4_dc_measure == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_battery_bank_4_dc_measure) }}" width="300" height="300"></td>
			@endif

			@if($ri_battery_label == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_battery_label) }}" width="300" height="300"></td>
			@endif
		</tr>
        <tr>
            <td>Battery Bank #4 DC Measurement</td>
            <td>Battery Label</td>
        </tr>
		<tr>
			@if($ri_battery_main_bar_inst == null)
				<td><h1>N/A</h1></td>
			@else
				<td style="width: 50%"><img src="{{ url('images/report/' . $ri_battery_main_bar_inst) }}" width="300" height="300"></td>
			@endif

			@if($ri_battery_mcb_fuse == null)
				<td><h1>N/A</h1></td>
			@else
				<td><img src="{{ url('images/report/' . $ri_battery_mcb_fuse) }}" width="300" height="300"></td>
			@endif
		</tr>
		<tr>
			<td>Battery Main Bar installation</td>
			<td>Battery MCB/Fuse</td>
		</tr>
	</table>
	@if ($documents->doc_type == 1)
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 8)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 10)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_pm_rect_batt}}<br>(Edo Hapsoro)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @else
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 3)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 7)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_cpo}}<br>(Matius Lamba)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @endif
	<div class="page_break"></div>
	<h2 style="text-align: center;">SITE LAYOUT</h2>
	<br><br><br><br><br><br><br><br>
	<table class="tb-border">
		<tr>
			<td>
				@if($site_layout_rectifier == null)
					<br><br><br><br><br><br><br><br><br><br>
				@else
					<img style="text-align : center" src="{{ url('images/report/' . $site_layout_rectifier) }}" width="300" height="300">
				@endif
			</td>
		</tr>
	</table>
	<br><br><br><br><br><br><br><br><br><br>
	@if ($documents->doc_type == 1)
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 8)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 10)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_pm_rect_batt}}<br>(Edo Hapsoro)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @else
        <table class="tb-border" style="width: 100%;text-align: center;">
            <tr style="text-align: left;">
                <td style="width: 50%">
                    <p>Site ID:{{$documents->site_id}}</p>
                    <p>Site Name:{{$documents->site_name}}</p>
                </td>
                <td>Project ID :{{$documents->project_id}}</td>
            </tr>
            <tr>
                <td>
                    <p>Telkominfra</p>
                    <br>
                    @if($documents->doc_status >= 3)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkminframanager) }}" alt="Sign {{ $tkminframanager }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_infra}}<br>(Budi Setiawan)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif

                </td>
                <td>
                    <p>Telkomsel</p>
                    <br>
                    @if($documents->doc_status >= 7)
                        <img src="{{ url('images/sign/png approved.png') }}" alt="Approved" width="125" height="125">
                        {{--<img src="{{ url('images/sign/' . $tkmmanagercpo) }}" alt="Sign {{ $tkmmanagercpo }}" width="200" height="100">--}}
                        <p>{{$documents->datesign_cpo}}<br>(Matius Lamba)<br>_________________________</p>
                    @else
                        <br><br><br><br><br>
                        <p>_________________________</p>
                    @endif
                </td>
            </tr>
        </table>
    @endif
</body>
</html>
