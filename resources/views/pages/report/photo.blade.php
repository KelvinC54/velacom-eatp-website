@extends('layouts.main')

@section('title','Create ATP Report')

@section('main-content')

<style type="text/css">

.step {
	display: none;
}
.step.active {
	display: block;
}

</style>

<form action="/photos/" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}
	<div class="card-header text-white bg-info">General Information Photo</div>
	<div class="card-body">
		<div class="form-group">
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<p>Site Information</p>
						<input type="file" accept="image/*" name="gi_site_information">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" name="" class="btn btn-info" value="Upload">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<p>Site Access</p>
						<input type="file" accept="image/*" name="gi_site_access">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" class="btn btn-info" value="Upload">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<p>Site - Front View</p>
						<input type="file">
						<input type="button" class="btn btn-info" value="Upload">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<p>Site - Rear View</p>
						<input type="file">
						<input type="button" class="btn btn-info" value="Upload">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<p>Site - Left View</p>
						<input type="file">
						<input type="button" class="btn btn-info" value="Upload">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<p>Site - Right View</p>
						<input type="file">
						<input type="button" class="btn btn-info" value="Upload">
					</div>
				</div>
			</div>


		</div>
	</div>
	<div class="card-footer">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="submit" class="btn btn-success float-right">Submit</button>
		<button type="button" class="previous-btn btn btn-secondary float-left">Previous</button>
	</div>
</form>

<script type="text/javascript">

const steps = Array.from(photo.querySelectorAll("form .step"));
const nextBtn = photo.querySelectorAll("form .next-btn");
const prevBtn = photo.querySelectorAll("form .previous-btn");
const form = photo.querySelector("form");

nextBtn.forEach((button) => {
	button.addEventListener("click", () => {
		changeStep("next");
	});
});

prevBtn.forEach((button) => {
	button.addEventListener("click", () => {
		changeStep("prev");
	});
});

form.addEventListener("submit", (e) => {
	e.preventDefault();
	const inputs = [];
	form.querySelectorAll("input").forEach((input) => {
		const { name, value } = input;
		inputs.push({ name, value });
	});
	console.log(inputs);
	form.reset();
});

function changeStep(btn) {
	let index = 0;
	const active = document.querySelector(".active");
	index = steps.indexOf(active);
	steps[index].classList.remove("active");
	if (btn === "next") {
		index++;
	} else if (btn === "prev") {
		index--;
	}
	steps[index].classList.add("active");
}

</script>

<script src="assets/bundles/lib.vendor.bundle.js"></script>
<script src="assets/plugins/dropify/js/dropify.min.js"></script>
<script src="assets/js/dropify.js"></script>

@endsection
