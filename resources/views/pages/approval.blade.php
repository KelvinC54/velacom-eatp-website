@extends('layouts.main')

@section('title','Approval')

@section('main-content')
<div class="section-body">
	@if(Session::get('message'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"></button>
			<p> {{ Session::get('message') }} </p>
		</div>
	@endif
	<div class="container-fluid mt-2">
		<div class="d-flex justify-content-between align-items-center ">
			<div class="header-action">
				<h1 class="page-title">ATP Approval Date</h1>
			</div>
		</div>
	</div>
</div>
<div class="section-body mt-2">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="table-responsive">
						@if (
							(Auth::user()->jobdesk == "vlcadm" || Auth::user()->jobdesk == "vlcpm" || Auth::user()->jobdesk === "tkminframanager")
						)
						<table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="tabel_approval">
							<thead>
								<tr>
									<th>Site ID</th>
									<th>Site Name</th>
									<th>Area</th>
									<th>Location</th>
									<th>Date sign Velacom PM</th>
									<th>Date sign TelkomInfra</th>
									<th>Date Sign Telkomsel RTPO</th>
									<th>Date sign Telkomsel Manager NS</th>
									<th>Date Sign Telkomsel Staff CPO</th>
									<th>Date sign Telkomsel Manager CPO</th>
								</tr>
							</thead>
						</table>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $('#tabel_approval').DataTable({
		dom: 'Blfrtip',
		ordering: false,
        processing: true,
        serverSide: true,
        ajax: '/document/json',
		lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'All' ]
		],
        columns: [
            { data: 'site_id', name: 'site_id' },
            { data: 'site_name', name: 'site_name' },
            { data: 'site_area', name: 'site_area' },
            { data: 'site_loc', name: 'site_loc' },
            { data: 'datesign_pm', name: 'datesign_pm' },
			{ data: 'datesign_infra', name: 'datesign_infra' },
            { data: 'datesign_rtpo', name: 'datesign_rtpo' },
            { data: 'datesign_ns', name: 'datesign_ns' },
            { data: 'datesign_stafcpo', name: 'datesign_stafcpo' },
            { data: 'datesign_cpo', name: 'datesign_cpo' },
        ],
		buttons:[
			'excel',
		]
    });	
</script>
@endsection
