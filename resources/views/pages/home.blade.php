@extends('layouts.main')

@section('title','Dashboard')

@section('main-content')
<div class="section-body mt-4">
	<div class="container-fluid">

        {{-- DASHBOARD WEBADMIN --}}
		@if (Auth::user()->jobdesk == "webadmin")
			<div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
					<div class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Users</div>
								<h4 class="mb-0">{{ count($users) }}</h4>
							</div>
						</div>
					</div>
				</div>
			</div>

        {{-- DASHBOARD VLCADM --}}
        @elseif(
                    (auth()->user()->jobdesk == 'vlcadm')
               )
            <div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
					<a href="{{ route('summary.total') }}" class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Dokumen (PSE)</div>
								<h4 class="mb-0">{{ $countTotalDocuments }}</h4>
							</div>
						</div>
					</a>
				</div>

				<div class="col-12 col-sm-6 col-md-3">
                    <a href="/rejection" class="card bg-danger">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Dokumen di Reject (PSE)</div>
								<h4 class="mb-0">{{ $rejectedDocuments }}</h4>
							</div>
						</div>
					</a>
				</div>
			</div>

            <div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Dokumen (RANE)</div>
								<h4 class="mb-0">{{ $countTotalDocumentsRectBatt }}</h4>
							</div>
						</div>
                    </div>
				</div>

				<div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-danger">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Dokumen di Reject (RANE)</div>
								<h4 class="mb-0">{{ $rejectedDocumentsRectBatt }}</h4>
							</div>
						</div>
                    </div>
				</div>
			</div>

        {{-- DASHBOARD VLCPM & TKMINFRA --}}
		@elseif(
                    (auth()->user()->jobdesk == 'vlcpm') ||
                    (auth()->user()->jobdesk == 'tkminframanager')
               )
            <div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
					<a href="{{ route('summary.total') }}" class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Dokumen (PSE)</div>
								<h4 class="mb-0">{{ $countTotalDocuments }}</h4>
							</div>
						</div>
					</a>
				</div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{ route('summary.needReview') }}" class="card bg-info">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen Perlu Approval (PSE)</div>
                                <h4 class="mb-0">{{ $needReviewDocuments }}</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{ route('summary.approved') }}" class="card bg-success">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen di Approved (PSE)</div>
                                <h4 class="mb-0">{{ $approvedDocuments }}</h4>
                            </div>
                        </div>
                    </a>
                </div>

				<div class="col-12 col-sm-6 col-md-3">
					<a href="{{ route('summary.rejected') }}" class="card bg-danger">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Dokumen di Reject (PSE)</div>
								<h4 class="mb-0">{{ $rejectedDocuments }}</h4>
							</div>
						</div>
					</a>
				</div>

			</div>

            <div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Dokumen (RANE)</div>
								<h4 class="mb-0">{{ $countTotalDocumentsRectBatt }}</h4>
							</div>
						</div>
                    </div>
				</div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-info">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen Perlu Approval (RANE)</div>
                                <h4 class="mb-0">{{ $needReviewDocumentsRectBatt }}</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-success">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen di Approved (RANE)</div>
                                <h4 class="mb-0">{{ $approvedDocumentsRectBatt }}</h4>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-danger">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Dokumen di Reject (RANE)</div>
								<h4 class="mb-0">{{ $rejectedDocumentsRectBatt }}</h4>
							</div>
					    </div>
                    </div>
				</div>

			</div>

        {{-- DASHBOARD PSC --}}
		@elseif(
                    (auth()->user()->jobdesk == 'reviewertkmrto') ||
                    (auth()->user()->jobdesk == 'tkmmanagerns') ||
                    (auth()->user()->jobdesk == 'tkmcpo') ||
                    (auth()->user()->jobdesk == 'tkmpmrectbatt')
               )

			<div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
					<a href="{{ route('summary.total') }}" class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Dokumen (PSE)</div>
								<h4 class="mb-0">{{ $countTotalDocuments }}</h4>
							</div>
						</div>
					</a>
				</div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{ route('summary.needReview') }}" class="card bg-info">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen Perlu Approval (PSE)</div>
                                <h4 class="mb-0">{{ $needReviewDocuments }}</h4>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <a href="{{ route('summary.approved') }}" class="card bg-danger">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen di Approved (PSE)</div>
                                <h4 class="mb-0">{{ $approvedDocuments }}</h4>
                            </div>
                        </div>
                    </a>
                </div>

				<div class="col-12 col-sm-6 col-md-3">
                    @if(auth()->user()->jobdesk == 'vlcadm')
                        <a href="/rejection" class="card bg-success">
                    @else
					    <a href="{{ route('summary.rejected') }}" class="card bg-success">
                    @endif
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Dokumen di Reject (PSE)</div>
								<h4 class="mb-0">{{ $rejectedDocuments }}</h4>
							</div>
						</div>
					</a>
				</div>

			</div>

        {{-- DASHBOARD RANE --}}
        @elseif(
                    (auth()->user()->jobdesk == 'tkmrevrectbatt') ||
                    (auth()->user()->jobdesk == 'tkmpmrectbatt')
               )
			<!-- Info boxes -->
			<div class="row mb-3">
				<div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-info">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Total Dokumen (RANE)</div>
								<h4 class="mb-0">{{ $countTotalDocumentsRectBatt }}</h4>
							</div>
						</div>
                    </div>
				</div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-info">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen Perlu Approval (RANE)</div>
                                <h4 class="mb-0">{{ $needReviewDocumentsRectBatt }}</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-success">
                        <div class="card-body w_user">
                            <div class="content text-white">
                                <div class="text">Dokumen di Approved (RANE)</div>
                                <h4 class="mb-0">{{ $approvedDocumentsRectBatt }}</h4>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="col-12 col-sm-6 col-md-3">
                    <div class="card bg-danger">
						<div class="card-body w_user">
							<div class="content text-white">
								<div class="text">Dokumen di Reject (RANE)</div>
								<h4 class="mb-0">{{ $rejectedDocumentsRectBatt }}</h4>
							</div>
						</div>
                    </div>
				</div>

			</div>
		@endif
	</div>
</div>

@if (Auth::user()->jobdesk == "vlcadm" || Auth::user()->jobdesk == "vlcpm" || Auth::user()->jobdesk == "tkminframanager" || Auth::user()->jobdesk == "tkmmanagercpo")

    <div>
        <div class="section-body mt-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="doc_status_table">
                                <thead>
                                    <tr>
                                        <th>Status PSE</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>Belum di Review</td>
                                            <td>{{$onPMDocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Velacom PM</td>
                                            <td>{{$onInfraDocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by TelkomInfra Manager</td>
                                            <td>{{$onRTPODocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Approved by Telkomsel RTPO</td>
                                            <td>{{$onNSDocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Telkomsel Manager NS</td>
                                            <td>{{$onStafCPODocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Approved by Staff Telkomsel CPO</td>
                                            <td>{{$onCPODocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Telkomsel Manager CPO</td>
                                            <td>{{$doneDocuments}}</td>
                                        </tr>
                                        <tr hidden>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Summary:</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Total Dokumen</td>
                                            <td>{{ $countTotalDocuments }}</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Dokumen di Approved</td>
                                            <td>{{ $approvedDocuments }}</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Dokumen di Reject</td>
                                            <td>{{ $rejectedDocuments }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div>
        <div class="section-body mt-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="doc_status_table">
                                <thead>
                                    <tr>
                                        <th>Status RANE</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>Belum di Review</td>
                                            <td>{{$onPMDocumentsRectBatt}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Velacom PM</td>
                                            <td>{{$onInfraDocumentsRectBatt}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by TelkomInfra Manager</td>
                                            <td>{{$onrevDocumentsRectBatt}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Telkomsel Reviewer Rect Batt</td>
                                            <td>{{$onPMTKMDocumentsRectBatt}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Telkomsel PM Rect Batt</td>
                                            <td>{{$doneDocumentsRectBatt}}</td>
                                        </tr>
                                        <tr hidden>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Summary:</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Total Dokumen</td>
                                            <td>{{ $countTotalDocumentsRectBatt }}</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Dokumen di Approved</td>
                                            <td>{{ $approvedDocumentsRectBatt }}</td>
                                        </tr>
                                        <tr hidden>
                                            <td>Dokumen di Reject</td>
                                            <td>{{ $rejectedDocumentsRectBatt }}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

@elseif(Auth::user()->jobdesk == "reviewertkmrto" || Auth::user()->jobdesk == "tkmmanagerns")
    <div>
        <div class="section-body mt-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-vcenter text-nowrap mb-0" id="doc_status_table">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>Signed by TelkomInfra Manager</td>
                                            <td>{{$onRTPODocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Approved by Telkomsel RTPO</td>
                                            <td>{{$onNSDocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Telkomsel Manager NS</td>
                                            <td>{{$onStafCPODocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Approved by Staff Telkomsel CPO</td>
                                            <td>{{$onCPODocuments}}</td>
                                        </tr>
                                        <tr>
                                            <td>Signed by Telkomsel Manager CPO</td>
                                            <td>{{$doneDocuments}}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

@endif
<script>
	$(document).ready( function () {

		var  CenterTable = $('#doc_status_table').DataTable({
			dom: 'Brtp',
			paging: false,
			ordering: false,
			buttons: [
				{
					extend: 'excelHtml5',
					text:'<i class="fa fa-table fainfo" aria-hidden="true" ></i> Unduh',
            		titleAttr: 'Export Excel',
					title: 'Data Status Dokumen',
					customize: function( xlsx ) {
						var sheet = xlsx.xl.worksheets['sheet1.xml'];
                		// $('row:first c', sheet).attr( 's', '42' );
						$('row:nth-child(11) c:nth-child(1)', sheet).attr('s', '2');
						$('row:nth-child(12) c', sheet).attr('s', '22');
						$('row:nth-child(13) c', sheet).attr('s', '17');
						$('row:nth-child(14) c', sheet).attr('s', '12');
					},
					exportOptions: {
                    	columns: ':visible'
                	},
                },
			]
		});
	} );
</script>
@endsection
