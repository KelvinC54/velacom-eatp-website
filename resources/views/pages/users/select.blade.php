@extends('layouts.main')

@section('title','Data User')

@section('main-content')
<div class="section-body">
	@if(Session::get('message'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"></button>
			<p> {{ Session::get('message') }} </p>
		</div>
	@endif
	<div class="container-fluid mt-2">
		<div class="d-flex justify-content-between align-items-center ">
			<div class="header-action">
				<h1 class="page-title">Data User</h1>
			</div>
			<a href="{{ url('/users/create') }}" class="btn btn-info btn-sm">Tambah User</a>
		</div>
	</div>
</div>
<div class="section-body mt-2">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="table-responsive">
						<table class="table table-hover table-striped table-vcenter text-nowrap mb-0">
							<thead>
								<tr>
									<th>Id</th>
									<th>Nama</th>
									<th>Email</th>
									<th>Password</th>
									<th>Jobdesk</th>
									<th>Sign</th>
									<th>Action</th>
								</tr>
							</thead>
							@foreach($users as $user)
							<tbody>
								<tr>
									<td>{{$user->id}}</td>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									<td>{{$user->real_password}}</td>
									<td>{{$user->jobdesk}}</td>
									@if ($user->sign !== null)
										<td><img class="img-fluid rounded" src="{{ asset('images/sign/' . $user->sign) }}" alt="{{$user->sign}}" width="100" height="100"></td>
									@else
										<td>-</td>
									@endif
									<td>
										<div class="ui-group-buttons">
											<a href="{{ url('users/'. $user->id. '/edit') }}" class="btn btn-primary">Ubah</a>
											<div class="or"></div>
											<!-- Button trigger modal -->
											<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal{{ $user->id }}">Hapus</button>

											<!-- Modal HTML -->
											<div id="myModal{{ $user->id }}" class="modal fade">
												<div class="modal-dialog modal-confirm">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Yakin untuk menghapus data?</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<p>Anda yakin untuk menghapus data? Data tidak dapat dikembalikan.</p>
														</div>
														<div class="modal-footer justify-content-center">
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
															<form action="users/{{ $user->id }}" method="POST">
																<input type="hidden" name="_method" value="delete">
																<input type="hidden" name="_token" value="{{ csrf_token() }}">
																<button type="submit" class="button btn btn-danger">Hapus</button>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
