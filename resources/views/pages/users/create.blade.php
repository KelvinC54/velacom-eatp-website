@extends('layouts.main')

@section('title','Create User')

@section('main-content')

<div class="section-body mt-2">
	<div class="container-fluid">
		<h1 class="page-title">Create User</h1>

		<div class="tab-content mt-2">
			<form action="{{url('/users')}}" method="POST" enctype="multipart/form-data">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Nama</label>
							<input id="nama" type="text" placeholder="Nama" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="nama" autofocus>

							@error('nama')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<label>Email</label>
							<input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<label>Password</label>
							<input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" autofocus>

							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<label>Jobdesk</label>
							<select class="form-control @error('jobdesk') is-invalid @enderror" id="jobdesk" name="jobdesk" required>
								<option selected disabled>-- Jobdesk --</option>
								<option value="webadmin">Web Admin</option>
								<option value="vlcadm">Velacom Administrator</option>
								<option value="vlccord">Velacom Coordinator</option>
								<option value="vlcpm">Velacom Project Manager</option>
								<option value="tkminframanager">Telkom Infra Manager</option>
								<option value="reviewertkmrto">Reviewer Telkomsel RTO</option>
                                <option value="tkmmanagerns">Telkomsel Manager NS</option>
								<option value="tkmcpo">Telkomsel CPO</option>
                                <option value="tkmmanagercpo">Telkomsel Manager CPO</option>
                                <option value="tkmrevrectbatt">Telkomsel Reviewer Rect Batt</option>
                                <option value="tkmpmrectbatt">Telkomsel PM Rect Batt</option>
							</select>

							@error('jobdesk')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<label>Sign</label>
							<input type="file" accept="image/*" class="form-control @error('sign') is-invalid @enderror" id="sign" name="sign" autofocus>

							@error('sign')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
					<div class="card-footer">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<button type="submit" class="next-btn btn btn-success float-right">Create</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection
