<!doctype html>
<html lang="en" dir="ltr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
	<title>@yield('title')</title>

	<!-- Bootstrap Core and vandor -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/css/dropify.min.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Core css -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.min.css') }}"/>
	<style>
		.ui-group-buttons .or{position:relative;float:left;width:.3em;height:1.3em;z-index:3;font-size:12px}
		.ui-group-buttons .or:before{position:absolute;top:50%;left:50%;content:'or';background-color:#5a5a5a;margin-top:-.1em;margin-left:-.9em;width:1.8em;height:1.8em;line-height:1.55;color:#fff;font-style:normal;font-weight:400;text-align:center;border-radius:500px;-webkit-box-shadow:0 0 0 1px rgba(0,0,0,0.1);box-shadow:0 0 0 1px rgba(0,0,0,0.1);-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box}
		.ui-group-buttons .or:after{position:absolute;top:0;left:0;content:' ';width:.3em;height:2.84em;background-color:rgba(0,0,0,0);border-top:.6em solid #5a5a5a;border-bottom:.6em solid #5a5a5a}
		.ui-group-buttons .or.or-lg{height:1.3em;font-size:16px}
		.ui-group-buttons .or.or-lg:after{height:2.85em}
		.ui-group-buttons .or.or-sm{height:1em}
		.ui-group-buttons .or.or-sm:after{height:2.5em}
		.ui-group-buttons .or.or-xs{height:.25em}
		.ui-group-buttons .or.or-xs:after{height:1.84em;z-index:-1000}
		.ui-group-buttons{display:inline-block;vertical-align:middle}
		.ui-group-buttons:after{content:".";display:block;height:0;clear:both;visibility:hidden}
		.ui-group-buttons .btn{float:left;border-radius:0}
		.ui-group-buttons .btn:first-child{margin-left:0;border-top-left-radius:.25em;border-bottom-left-radius:.25em;padding-right:15px}
		.ui-group-buttons .btn:last-child{border-top-right-radius:.25em;border-bottom-right-radius:.25em;padding-left:15px}
	</style>

	<!-- Start Main project js, jQuery, Bootstrap -->
	<script src="{{ asset('assets/plugins/jquery/jquery.js') }}"></script>
	<script src="{{ asset('assets/bundles/lib.vendor.bundle.js') }}"></script>

	<script src="{{ asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>

	<!-- Start project main js  and page js -->
	<script src="{{ asset('assets/js/core.js') }}"></script>

	<!-- Start datatable js and css -->
	<script src="{{ asset('assets/plugins/datatable/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatable/buttons/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-html5-1.6.1/b-print-1.6.1/datatables.min.css"/>

</head>

<body class="font-muli theme-cyan gradient">

	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
		</div>
	</div>

	<div id="main_content">
		<!-- Sidebar -->
		<div id="header_top" class="header_top">
			<div class="container">
				<div class="hleft">
					<a class="header-brand" href="/dashboard"><i class="fa fa-briefcase brand-logo"></i></a>
					<div class="dropdown">
						<a href="javascript:void(0)" class="nav-link icon menu_toggle"><i class="fe fe-align-center"></i></a>
					</div>
				</div>
				<div class="hright">
					<a href="{{ route('logout') }}" class="nav-link icon settingbar"
						onclick="event.preventDefault();
						document.getElementById('logout-form').submit();"><i class="fe fe-power"></i>
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
						@csrf
					</form>
				</div>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<div id="left-sidebar" class="sidebar">
			<h5 class="brand-name">E-ATP<a href="javascript:void(0)" class="menu_option float-right"><i class="icon-grid font-16" data-toggle="tooltip" data-placement="left" title="Grid & List Toggle"></i></a></h5>
			<ul class="nav nav-tabs">
				<li class="nav-item nav-link">Menu</li>
			</ul>
			<nav class="sidebar-nav mt-2">
				<ul class="metismenu">
					<li><a href="/dashboard"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
					@if (Auth::user()->jobdesk === "webadmin")
						<li><a href="/users"><i class="fa fa-calendar-check-o"></i><span>Users</span></a></li>
					@elseif(Auth::user()->jobdesk === "vlcpm" || Auth::user()->jobdesk === "vlcadm" || Auth::user()->jobdesk === "tkminframanager")
						<li><a href="/report"><i class="fa fa-calendar-check-o"></i><span>ATP Report (PSE)</span></a></li>
                        <li><a href="/reportrectbatt"><i class="fa fa-calendar-check-o"></i><span>ATP Report (RANE)</span></a></li>
						<li><a href="/approval"><i class="fa fa-check-square-o"></i><span>ATP Approval Date</span></a></li>
						<li><a href="/rejection"><i class="fa fa-times-circle-o"></i><span>ATP Rejection Records</span></a></li>
                    @elseif (Auth::user()->jobdesk === "tkmrevrectbatt" || Auth::user()->jobdesk === "tkmpmrectbatt")
                        <li><a href="/reportrectbatt"><i class="fa fa-calendar-check-o"></i><span>ATP Report (RANE)</span></a></li>
					@else
					    <li><a href="/report"><i class="fa fa-calendar-check-o"></i><span>ATP Report (PSE)</span></a></li>
					@endif
					@if(Auth::user()->jobdesk === "vlcadm")
						<li><a href="/emaillog"><i class="fa fa-envelope"></i><span>Email Notifications Log</span></a></li>
					@endif
				</ul>
			</nav>
		</div>

		<div class="page">
			<!-- Navbar -->
			<div class="section-body" id="page_top" >
				<div class="container-fluid">
					<div class="page-header">
						<div class="left">
							{{--<div class="input-group">
							    <form action="/report/search" method="GET">
								    <input type="text" class="form-control" name="cari" placeholder="What you want to find" value="{{old('cari')}}">
								    <div class="input-group-append">
									    <button class="btn btn-outline-secondary" type="submit" value="cari">Search</button>
								    </div>
								</form>
							</div>--}}
						</div>
						<div class="right">
							<div class="notification d-flex">
								<div class="dropdown d-flex">
									<a href="javascript:void(0)" class="chip ml-3" data-toggle="dropdown">
										<span class="avatar" style="background-image: {{ asset('assets/images/xs/avatar5.jpg') }}"></span> {{ Auth::user()->name }}</a>
										<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
											<a class="dropdown-item" href="{{ url('profile/' . Auth::user()->id . '/edit') }}"><i class="dropdown-icon fe fe-user"></i> Profile</a>
											<!--<a class="dropdown-item" href="app-setting.html"><i class="dropdown-icon fe fe-settings"></i> Settings</a>-->

											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon fe fe-help-circle"></i> Need help?</a>
											<a class="dropdown-item" href="{{ route('logout') }}"
												onclick="event.preventDefault();
												document.getElementById('logout-form').submit();"><i class="dropdown-icon fe fe-log-out"></i> Sign out</a>
											<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
												@csrf
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Main Content -->
				@yield('main-content')

				<!-- Footer -->
				<div class="section-body">
					<footer class="footer">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									Copyright © PT. Velacom Indonesia
									<br>Version : 1.9.0
								</div>
								<div class="col-md-6 col-sm-12 text-md-right">
									ATP Report
								</div>
							</div>
						</div>
					</footer>
				</div>
			</div>
		</div>
	</body>
	</html>
