<!doctype html>
<html lang="en" dir="ltr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="favicon.ico" type="image/x-icon"/>
	<title>Login E-ATP</title>

	<!-- Bootstrap Core and vandor -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/plugins/dropify/css/dropify.min.css">

	<!-- Core css -->
	<link rel="stylesheet" href="assets/css/style.min.css"/>
</head>

<body class="font-muli theme-cyan gradient"0>
	<div class="auth option2">
		<div class="auth_left">
			<div class="card">
					<div style="background-color: #212845">
						<a style="color: #ffffff; font-size: 15px; margin-left: 5.5px"; > E - ATP</a>
						{{--<img style="float: right" src="assets/images/Telkominfra-white.png" width="100">--}}
					</div>
				<div class="card-body">
					<form method="POST" action="{{ route('login') }}">
						@csrf
						<div class="text-center">
							<img src="assets/images/TelkomInfra.jpg" width="250">
							<div class="card-title mt-3"></div>
							<div class="card-title mt-3">Login untuk masuk</div>
						</div>
						<div class="form-group">
							<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email"  name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="form-group">
							<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password"  name="password" required autocomplete="current-password" autofocus>
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>

						<div class="text-center">
							<button type="submit" class="btn btn-primary btn-block" title="">Login</button>
						</div>
						<div class="card-title mt-3"></div>
						<div class="text-right">
                            <a> PT. Velacom Indonesia</a>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Start Main project js, jQuery, Bootstrap -->
	<script src="../assets/bundles/lib.vendor.bundle.js"></script>

	<!-- Start project main js  and page js -->
	<script src="../assets/js/core.js"></script>
</body>
</html>
