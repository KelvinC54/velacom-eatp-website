<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Noto+Sans&display=swap');
    </style>
</head>
<body style="background-color:rgb(245,245,245); font-family: 'Noto Sans', sans-serif;" style="display: block; clear: both; margin: 0 auto; max-width: 580px;" align="center">
    <table style="width: 100%; margin: 0 auto;">
        <tr>
            <td align="center" valign="top">
                <table style="width: 640px; background-color: white; margin-top: 30px; margin-bottom: 30px;">
                    <tbody>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="masthead" style="background-color:white; max-width:580px" align="center">
                                <a href="http://www.velosa.co.id/login"><img src="http://velosa.co.id/assets/images/TelkomInfra.jpg" alt="header" width="300px"></a> 
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding-top: 30px; padding-bottom:30px">
                                <table style="width: 580px;">
                                    <tbody>
                                        <tr>
                                            <td align="justify">
                                                <h2>Review Dokumen ATP</h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="justify" style="font-size: 14px; color: black;" id="isi-email">
                                                <p>Yth. Bapak {{ $nama }}</p>
                                                <p>Pesan ini merupakan layanan notifikasi website <a href="www.velosa.co.id">velosa.co.id</a> <br>
                                                    Mohon mereview dan Approval Dokumen ATP berikut : 
                                                </p>
                                                <table style="width:100%; border: 1px solid #ddd;" >
                                                    <tr style="background-color: #DC143C; color:white">
                                                        <th style="padding: 7px;">Site ID</th>
                                                        <th style="padding: 7px;" >Project Name</th> 
                                                        <th style="padding: 7px;" >Site Name</th>
                                                        <th style="padding: 7px;" >Site Loc</th>
                                                        <th style="padding: 7px;" >Site Area</th> 
                                                    </tr>
                                                    <tr style="background-color: white;">
                                                        <td style="padding: 7px;" >{{ $document->site_id }}</td>
                                                        <td style="padding: 7px;" >{{ $document->project_name }}</td>
                                                        <td style="padding: 7px;" >{{ $document->site_name }}</td>
                                                        <td style="padding: 7px;" >{{ $document->site_loc }}</td>
                                                        <td style="padding: 7px;" >{{ $document->site_area }}</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 14px; color: black;" id="isi-email">
                                                <br>
                                                <p>Salam, Admin Velacom.</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>                         
            </td>
        </tr>
    </table>
</body>
</html>