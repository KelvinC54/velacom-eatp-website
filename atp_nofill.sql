-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 11, 2021 at 10:52 PM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eatp`
--

-- --------------------------------------------------------

--
-- Table structure for table `atp_nofill`
--

CREATE TABLE `atp_nofill` (
  `id` int(10) NOT NULL,
  `site_id` varchar(30) DEFAULT NULL,
  `site_name` varchar(30) DEFAULT NULL,
  `doc_date` varchar(100) DEFAULT NULL,
  `doc_status` int(10) DEFAULT NULL,
  `site_region` varchar(30) DEFAULT NULL,
  `site_location` varchar(255) DEFAULT NULL,
  `bts_type` varchar(30) DEFAULT NULL,
  `doc_sow` varchar(50) DEFAULT NULL,
  `po_number` varchar(20) DEFAULT NULL,
  `site_config` varchar(30) DEFAULT NULL,
  `oa_date` varchar(50) DEFAULT NULL,
  `file_atp` varchar(255) DEFAULT NULL,
  `file_boq` varchar(255) DEFAULT NULL,
  `file_bapa` varchar(255) DEFAULT NULL,
  `file_tambahan` varchar(255) DEFAULT NULL,
  `namesign_pm_vlc` varchar(100) DEFAULT NULL,
  `datesign_pm_vlc` varchar(100) DEFAULT NULL,
  `namesign_infra` varchar(100) DEFAULT NULL,
  `datesign_infra` varchar(100) DEFAULT NULL,
  `namesign_reviewer` varchar(100) DEFAULT NULL,
  `datesign_reviewer` varchar(100) DEFAULT NULL,
  `namesign_pm_tkm` varchar(100) DEFAULT NULL,
  `datesign_pm_tkm` varchar(100) DEFAULT NULL,
  `new_site_status` int(5) DEFAULT NULL,
  `new_site_remark` varchar(255) DEFAULT NULL,
  `engpar_status` int(5) DEFAULT NULL,
  `engpar_remark` varchar(255) DEFAULT NULL,
  `bts_vswr_status` int(5) DEFAULT NULL,
  `bts_vswr_remark` varchar(255) DEFAULT NULL,
  `com_record_status` int(5) DEFAULT NULL,
  `com_record_remark` varchar(255) DEFAULT NULL,
  `capture_status` int(5) DEFAULT NULL,
  `capture_remark` varchar(255) DEFAULT NULL,
  `sp_tc_status` int(5) DEFAULT NULL,
  `sp_tc_remark` varchar(255) DEFAULT NULL,
  `inst_status` int(5) DEFAULT NULL,
  `inst_remark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atp_nofill`
--
ALTER TABLE `atp_nofill`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atp_nofill`
--
ALTER TABLE `atp_nofill`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
