<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/reportrectbatt', 'DocumentsController@indexdoc1');
    Route::get('/approval', 'DocumentsController@indexapproval');
    Route::get('/document/json','DocumentsController@json');
    Route::get('/rejection', 'RejectionController@index');
    Route::get('/rejection/json','RejectionController@json');
    Route::get('/rejection/viewremarks/{id}','RejectionController@viewremarks');

    Route::get('/report/cetak-atp/{id}', 'DocumentsController@cetak_atp');
    Route::get('/report/cetak-bal/{id}', 'DocumentsController@cetak_bal');
    Route::get('/report/approve-0/{id}', 'DocumentsController@approve_0');
    Route::get('/report/approve-2/{id}', 'DocumentsController@approve_2');
    Route::get('/report/approve-3/{id}', 'DocumentsController@approve_3');
    Route::get('/report/approve-4/{id}', 'DocumentsController@approve_4');
    Route::get('/report/approve-5/{id}', 'DocumentsController@approve_5');
    Route::get('/report/approve-6/{id}', 'DocumentsController@approve_6');
    Route::get('/report/approve-rect-rev/{id}', 'DocumentsController@approve_rect_batt_reviewer');
    Route::get('/report/approve-rect-pm/{id}', 'DocumentsController@approve_rect_batt_pm');
    Route::get('/report/reject/{id}/{id_user}', 'DocumentsController@reject');
    Route::get('/report/reject-tkmcpo/{id}/{id_user}', 'DocumentsController@reject_tkmcpo');
    Route::get('/report/reject-tkmmanagercpo/{id}/{id_user}', 'DocumentsController@reject_tkmmanagercpo');

    //Route::get('/mail/testpage', 'DocumentsController@mail_test');
    //Route::get('/mail/send', 'DocumentsController@send_mail');
    //Route::get('/mail/approve2', 'DocumentsController@test_approve_2');
    //Route::get('/mail/approve3', 'DocumentsController@test_approve_3');
    //Route::get('/mail/approve4', 'DocumentsController@test_approve_4');
    //Route::get('/mail/approve5', 'DocumentsController@test_approve_5');
    Route::get('/emaillog', 'EmailLogController@index');
    Route::get('/emaillog/json', 'EmailLogController@json');
});

Route::prefix('/summary')->name('summary.')->group(function(){
    Route::get('/all', 'DocumentsController@viewTotalDocs')->name('total');
    Route::get('/need-review', 'DocumentsController@viewNeedReviewDocs')->name('needReview');
    Route::get('/approved', 'DocumentsController@viewApprovedDocs')->name('approved');
    Route::get('/rejected', 'DocumentsController@viewRejectedDocs')->name('rejected');
});

Route::resources([
	'users' => 'UsersController',
	'report' => 'DocumentsController',
	'photos' => 'PhotosController',
	'profile' => 'ProfileController',
]);

Auth::routes();
