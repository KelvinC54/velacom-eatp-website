<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$users = DB::table('users')->get();
		$documents = DB::table('documents')->get();

        // Count BATT
		$countTotalDocuments = $this->countTotalDocs();
		$needReviewDocuments = $this->countNeedReviewDocs();
		$approvedDocuments = $this->countApprovedDocs();
		$rejectedDocuments = $this->countRejectedDocs();

        // Count RECT BATT
        $countTotalDocumentsRectBatt = $this->countTotalDocsRectBatt();
        $needReviewDocumentsRectBatt = $this->countNeedReviewDocsRectBatt();
		$approvedDocumentsRectBatt = $this->countApprovedDocsRectBatt();
		$rejectedDocumentsRectBatt = $this->countRejectedDocsRectBatt();

		// Status Table BATT
		$onPMDocuments = DB::table('documents')
			->where([
                ['doc_type', '=', 0],
                ['doc_status', '=', '0']
            ])->orWhere([
                ['doc_type', null],
                ['doc_status', '=', '0']
            ])->count();
		$onInfraDocuments = DB::table('documents')
			->where('doc_status', '=', '2')
			->count();
		$onRTPODocuments = $this->onRTPODocuments();
		$onNSDocuments = $this->onNSDocuments();
		$onStafCPODocuments = $this->onStafCPODocuments();
		$onCPODocuments = $this->onCPODocuments();
        $doneDocuments = $this->doneDocuments();

        // Status Table RECT BATT
        $onPMDocumentsRectBatt = DB::table('documents')
			->where([
                ['doc_type', '=', 1],
                ['doc_status', '=', '0']
            ])->count();
		$onInfraDocumentsRectBatt = DB::table('documents')
			->where([
                ['doc_type', '=', 1],
                ['doc_status', '=', '2']
            ])->count();
        $onrevDocumentsRectBatt = DB::table('documents')
			->where('doc_status', '=', '8')
			->count();
        $onPMTKMDocumentsRectBatt = DB::table('documents')
			->where('doc_status', '=', '9')
			->count();
        $doneDocumentsRectBatt = DB::table('documents')
            ->where('doc_status', '=', '10')
            ->count();

		return view('pages.home', [
			'documents' => $documents,
			'users' => $users,

			'countTotalDocuments' => $countTotalDocuments,
			'needReviewDocuments' => $needReviewDocuments,
			'approvedDocuments' => $approvedDocuments,
			'rejectedDocuments' => $rejectedDocuments,

            'countTotalDocumentsRectBatt' => $countTotalDocumentsRectBatt,
            'needReviewDocumentsRectBatt' => $needReviewDocumentsRectBatt,
			'approvedDocumentsRectBatt' => $approvedDocumentsRectBatt,
			'rejectedDocumentsRectBatt' => $rejectedDocumentsRectBatt,

			'onPMDocuments' =>$onPMDocuments,
			'onInfraDocuments'=>$onInfraDocuments,
			'onRTPODocuments'=>$onRTPODocuments,
			'onNSDocuments'=>$onNSDocuments,
			'onStafCPODocuments'=>$onStafCPODocuments,
			'onCPODocuments'=>$onCPODocuments,
            'doneDocuments'=>$doneDocuments,

            'onPMDocumentsRectBatt'=>$onPMDocumentsRectBatt,
            'onInfraDocumentsRectBatt'=>$onInfraDocumentsRectBatt,
            'onrevDocumentsRectBatt'=>$onrevDocumentsRectBatt,
            'onPMTKMDocumentsRectBatt'=>$onPMTKMDocumentsRectBatt,
            'doneDocumentsRectBatt'=>$doneDocumentsRectBatt
			]
		);
	}

    public function countTotalDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where([
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_type', null],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', '>=',2],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', '>=',2],
                    ['doc_type', null],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', '>=', 3],
                    ['doc_type', 0],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_status', '>=', 3],
                    ['doc_type', null],
                    ['site_loc', $user->location]
                ])->get(); break;
			case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', '>=', 3],
                    ['doc_type', 0],
                    ['site_area', $user->location]
            	])->orWhere([
                    ['doc_status', '>=', 3],
                    ['doc_type', null],
                    ['site_area', $user->location]
			    ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', '>=', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', '>=', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', '>=', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', '>=', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', '>=', 9],
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                ['doc_status', '>=', 10],
                ['doc_type', '=', 1],
            ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countNeedReviewDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 0]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 0]
                ])->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 0]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 0]
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 0]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 0]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 2]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 2]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 3],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 3],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 4],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 4],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 5]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 5]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type', '=', 0],
                    ['doc_status', 6]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 6]
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 8]
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 9]
                ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countApprovedDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>=',0]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>=',0]
                ])->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 0]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 1]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>', 2]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 2]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>', 3],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 3],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>', 4],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 4],
                    ['site_area', $user->location]
            ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>', 5]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 5]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', '>', 6]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 6]
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 8]
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 9]
                ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countRejectedDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where('doc_status', 1)->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1],
                    ['site_loc', $user->location]
            ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1],
                    ['site_area', $user->location]
            ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type','=', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countTotalDocsRectBatt(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where([
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', '>=',2],
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', '>=', 3],
                    ['doc_type', 1],
                    ['site_loc', $user->location]
                ])->get(); break;
			case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', '>=', 3],
                    ['doc_type', 1],
                    ['site_area', $user->location]
            	])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', '>=', 5],
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', '>=', 6],
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', '>=', 9],
                    ['doc_type', '=', 1],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                ['doc_status', '>=', 10],
                ['doc_type', '=', 1],
            ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countNeedReviewDocsRectBatt(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 0]
                ])->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 0]
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 0]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 2]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 3],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 4],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 5]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 6]
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 8]
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 9]
                ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countApprovedDocsRectBatt(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>=',0]
                ])->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>=',0]
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 1]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 2]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 3],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 4],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 5]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 6]
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 8]
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', '>', 9]
                ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

    public function countRejectedDocsRectBatt(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'webadmin': $documents = Document::where('doc_status', 1)->get(); break;
            case 'vlcadm': $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type','=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_type', '=', 1],
                    ['doc_status', 1]
                ])->get(); break;
            default: break;
        }

        return $documents->count();
    }

	public function onRTPODocuments(){
		$user = auth()->user();

		switch ($user->jobdesk){
		    case 'webadmin': $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
		    case 'vlcadm': $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_status', 3],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 3],
                    ['doc_type', null],
                ])->get(); break;
            default: break;
        }

        return $documents->count();

	}

	public function onNSDocuments(){
		$user = auth()->user();

		switch ($user->jobdesk){
		    case 'webadmin': $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
		    case 'vlcadm': $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                    ['site_loc', $user->location],
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                    ['site_area', $user->location],
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_status', 4],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 4],
                    ['doc_type', null],
                ])->get(); break;
            default: break;
        }

        return $documents->count();

	}

	public function onStafCPODocuments(){
		$user = auth()->user();

		switch ($user->jobdesk){
		    case 'webadmin': $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
		    case 'vlcadm': $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_status', 5],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 5],
                    ['doc_type', null],
                ])->get(); break;
            default: break;
        }

        return $documents->count();

	}

	public function onCPODocuments(){
		$user = auth()->user();

		switch ($user->jobdesk){
		    case 'webadmin': $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
		    case 'vlcadm': $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_status', 6],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 6],
                    ['doc_type', null],
                ])->get(); break;
            default: break;
        }

        return $documents->count();

	}

	public function doneDocuments(){
		$user = auth()->user();

		switch ($user->jobdesk){
		    case 'webadmin': $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
		    case 'vlcadm': $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            case 'vlcpm':  $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmrevrectbatt':    $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            case 'tkmpmrectbatt':    $documents = Document::where([
                    ['doc_status', 7],
                    ['doc_type', '=', 0],
                ])->orWhere([
                    ['doc_status', 7],
                    ['doc_type', null],
                ])->get(); break;
            default: break;
        }

        return $documents->count();

	}



}
