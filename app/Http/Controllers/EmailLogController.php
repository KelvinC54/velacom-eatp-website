<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Auth;

class EmailLogController extends Controller
{
    public function index()
	{   
        $role = Auth::user()->jobdesk;
        if ($role != 'vlcadm') {
            return redirect('/dashboard');
        } else {
            return view('pages.maillog');
        }	
	}

	public function json(){
        $email_logs = DB::table('email_logs')
            ->Join('users', 'email_logs.id_penerima', '=', 'users.id')
            ->Join('documents', 'email_logs.doc_no', '=', 'documents.doc_no')
            ->select(
                'email_logs.created_at',
                'email_logs.status',
                'users.name', 
                'users.email', 
                'users.location', 
                'documents.site_id',
                'documents.project_name',
                'documents.site_name',
                'documents.site_loc',
                'documents.site_area',
                )
            ->get();
        return Datatables::of($email_logs)->make(true);
    }
}
