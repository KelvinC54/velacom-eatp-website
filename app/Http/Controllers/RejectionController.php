<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class RejectionController extends Controller
{
    public function index()
	{   
		return view('pages.rejection');
	}

	public function json(){
		$reject = DB::table('rejection_records')->get();
        return Datatables::of($reject)->make(true);
    }
	
	public function viewremarks( $id ){
		$reject = DB::table('rejection_records')->where('id', $id)->first();
        return response()->json(['success'=>true,'reject'=>$reject]);;
    }

}
