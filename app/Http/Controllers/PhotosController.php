<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Photo;
use App\Models\Document;
use Illuminate\Support\Facades\Validator;
use Storage;


class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
		$photos = DB::table('photos')->get();
		return view('pages.photos.select', ['photos' => $photos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$photos = DB::table('photos')->get();
		return view('pages.photos.create', ['photos' => $photos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function upload_gi_site_information(Request $request) {
		$file = $request->file('gi_site_information');
		$gi_site_information = $file->getClientOriginalName();

		if($request->file('gi_site_information')->isValid()) {
			$filename = "report-siteInfo-" . $gi_site_information;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_gi_site_access(Request $request) {
		$file = $request->file('gi_site_access');
		$gi_site_access = $file->getClientOriginalName();

		if($request->file('gi_site_access')->isValid()) {
			$filename = "report-siteAccess-" . $gi_site_access;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_gi_site_front_view(Request $request) {
		$file = $request->file('gi_site_front_view');
		$gi_site_front_view = $file->getClientOriginalName();

		if($request->file('gi_site_front_view')->isValid()) {
			$filename = "report-signInfo-" . $gi_site_front_view;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_gi_site_rear_view(Request $request) {
		$file = $request->file('gi_site_rear_view');
		$gi_site_rear_view = $file->getClientOriginalName();

		if($request->file('gi_site_rear_view')->isValid()) {
			$filename = "report-signInfo-" . $gi_site_rear_view;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_gi_site_left_view(Request $request) {
		$file = $request->file('gi_site_left_view');
		$gi_site_left_view = $file->getClientOriginalName();

		if($request->file('gi_site_left_view')->isValid()) {
			$filename = "report-signInfo-" . $gi_site_left_view;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_gi_site_right_view(Request $request) {
		$file = $request->file('gi_site_right_view');
		$gi_site_right_view = $file->getClientOriginalName();

		if($request->file('gi_site_right_view')->isValid()) {
			$filename = "report-signInfo-" . $gi_site_right_view;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_nf_installation_before(Request $request) {
		$file = $request->file('nf_installation_before');
		$nf_installation_before = $file->getClientOriginalName();

		if($request->file('nf_installation_before')->isValid()) {
			$filename = "report-siteInfo-" . $nf_installation_before;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_nf_installation_1(Request $request) {
		$file = $request->file('nf_installation_1');
		$nf_installation_1 = $file->getClientOriginalName();

		if($request->file('nf_installation_1')->isValid()) {
			$filename = "report-siteAccess-" . $nf_installation_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_nf_installation_2(Request $request) {
		$file = $request->file('nf_installation_2');
		$nf_installation_2 = $file->getClientOriginalName();

		if($request->file('nf_installation_2')->isValid()) {
			$filename = "report-signInfo-" . $nf_installation_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_installation_3(Request $request) {
		$file = $request->file('nf_installation_3');
		$nf_installation_3 = $file->getClientOriginalName();

		if($request->file('nf_installation_3')->isValid()) {
			$filename = "report-signInfo-" . $nf_installation_3;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_installation_4(Request $request) {
		$file = $request->file('nf_installation_4');
		$nf_installation_4 = $file->getClientOriginalName();

		if($request->file('nf_installation_4')->isValid()) {
			$filename = "report-signInfo-" . $nf_installation_4;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_installation_5(Request $request) {
		$file = $request->file('nf_installation_5');
		$nf_installation_5 = $file->getClientOriginalName();

		if($request->file('nf_installation_5')->isValid()) {
			$filename = "report-signInfo-" . $nf_installation_5;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_installation_6(Request $request) {
		$file = $request->file('nf_installation_6');
		$nf_installation_6 = $file->getClientOriginalName();

		if($request->file('nf_installation_6')->isValid()) {
			$filename = "report-siteInfo-" . $nf_installation_6;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_nf_installation_7(Request $request) {
		$file = $request->file('nf_installation_7');
		$nf_installation_7 = $file->getClientOriginalName();

		if($request->file('nf_installation_7')->isValid()) {
			$filename = "report-siteAccess-" . $nf_installation_7;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_nf_foundation_wide(Request $request) {
		$file = $request->file('gi_nf_foundation_wide');
		$gi_nf_foundation_wide = $file->getClientOriginalName();

		if($request->file('nf_foundation_wide')->isValid()) {
			$filename = "report-signInfo-" . $nf_foundation_wide;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_foundation_weight(Request $request) {
		$file = $request->file('nf_foundation_weight');
		$nf_foundation_weight = $file->getClientOriginalName();

		if($request->file('nf_foundation_weight')->isValid()) {
			$filename = "report-signInfo-" . $nf_foundation_weight;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_foundation_long(Request $request) {
		$file = $request->file('nf_foundation_long');
		$nf_foundation_long = $file->getClientOriginalName();

		if($request->file('nf_foundation_long')->isValid()) {
			$filename = "report-signInfo-" . $nf_foundation_long;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_nf_installation_after(Request $request) {
		$file = $request->file('nf_installation_after');
		$nf_installation_after = $file->getClientOriginalName();

		if($request->file('nf_installation_after')->isValid()) {
			$filename = "report-signInfo-" . $nf_installation_after;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_site_layout(Request $request) {
		$file = $request->file('site_layout');
		$site_layout = $file->getClientOriginalName();

		if($request->file('site_layout')->isValid()) {
			$filename = "report-siteInfo-" . $site_layout;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_base_frame_before(Request $request) {
		$file = $request->file('ri_base_frame_before');
		$ri_base_frame_before = $file->getClientOriginalName();

		if($request->file('ri_base_frame_before')->isValid()) {
			$filename = "report-siteAccess-" . $ri_base_frame_before;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_base_frame_after(Request $request) {
		$file = $request->file('ri_base_frame_after');
		$ri_base_frame_after = $file->getClientOriginalName();

		if($request->file('ri_base_frame_after')->isValid()) {
			$filename = "report-signInfo-" . $ri_base_frame_after;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_main_rack_1(Request $request) {
		$file = $request->file('ri_main_rack_1');
		$ri_main_rack_1 = $file->getClientOriginalName();

		if($request->file('ri_main_rack_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_main_rack_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_slave_rack_1(Request $request) {
		$file = $request->file('ri_slave_rack_1');
		$ri_slave_rack_1 = $file->getClientOriginalName();

		if($request->file('ri_slave_rack_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_slave_rack_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_main_rack_2(Request $request) {
		$file = $request->file('ri_main_rack_2');
		$ri_main_rack_2 = $file->getClientOriginalName();

		if($request->file('ri_main_rack_2')->isValid()) {
			$filename = "report-signInfo-" . $ri_main_rack_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_slave_rack_2(Request $request) {
		$file = $request->file('ri_slave_rack_2');
		$ri_slave_rack_2 = $file->getClientOriginalName();

		if($request->file('ri_slave_rack_2')->isValid()) {
			$filename = "report-siteInfo-" . $ri_slave_rack_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_pln_kwh_before(Request $request) {
		$file = $request->file('ri_pln_kwh_before');
		$ri_pln_kwh_before = $file->getClientOriginalName();

		if($request->file('ri_pln_kwh_before')->isValid()) {
			$filename = "report-siteAccess-" . $ri_pln_kwh_before;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_pln_kwh_after(Request $request) {
		$file = $request->file('ri_pln_kwh_after');
		$ri_pln_kwh_after = $file->getClientOriginalName();

		if($request->file('ri_pln_kwh_after')->isValid()) {
			$filename = "report-signInfo-" . $ri_pln_kwh_after;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_acpdb_before(Request $request) {
		$file = $request->file('ri_acpdb_before');
		$ri_acpdb_before = $file->getClientOriginalName();

		if($request->file('ri_acpdb_before')->isValid()) {
			$filename = "report-signInfo-" . $ri_acpdb_before;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_acpdb_after(Request $request) {
		$file = $request->file('ri_acpdb_after');
		$ri_acpdb_after = $file->getClientOriginalName();

		if($request->file('ri_acpdb_after')->isValid()) {
			$filename = "report-signInfo-" . $ri_acpdb_after;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_acpdb_mcb_before(Request $request) {
		$file = $request->file('ri_acpdb_mcb_before');
		$ri_acpdb_mcb_before = $file->getClientOriginalName();

		if($request->file('ri_acpdb_mcb_before')->isValid()) {
			$filename = "report-signInfo-" . $ri_acpdb_mcb_before;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_acpdb_mcb_after(Request $request) {
		$file = $request->file('ri_acpdb_mcb_after');
		$ri_acpdb_mcb_after = $file->getClientOriginalName();

		if($request->file('ri_acpdb_mcb_after')->isValid()) {
			$filename = "report-siteInfo-" . $ri_acpdb_mcb_after;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_rectifier_front(Request $request) {
		$file = $request->file('ri_rectifier_front');
		$ri_rectifier_front = $file->getClientOriginalName();

		if($request->file('ri_rectifier_front')->isValid()) {
			$filename = "report-siteAccess-" . $ri_rectifier_front;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_main_mcb_ac_input(Request $request) {
		$file = $request->file('ri_main_mcb_ac_input');
		$ri_main_mcb_ac_input = $file->getClientOriginalName();

		if($request->file('ri_main_mcb_ac_input')->isValid()) {
			$filename = "report-signInfo-" . $ri_main_mcb_ac_input;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_mcb_dc_dist(Request $request) {
		$file = $request->file('ri_mcb_dc_dist');
		$ri_mcb_dc_dist = $file->getClientOriginalName();

		if($request->file('ri_mcb_dc_dist')->isValid()) {
			$filename = "report-signInfo-" . $ri_mcb_dc_dist;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_controller_module(Request $request) {
		$file = $request->file('ri_controller_module');
		$ri_controller_module = $file->getClientOriginalName();

		if($request->file('ri_controller_module')->isValid()) {
			$filename = "report-signInfo-" . $ri_controller_module;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_module_1(Request $request) {
		$file = $request->file('ri_module_1');
		$ri_module_1 = $file->getClientOriginalName();

		if($request->file('ri_module_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_module_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_module_2(Request $request) {
		$file = $request->file('ri_module_2');
		$ri_module_2 = $file->getClientOriginalName();

		if($request->file('ri_module_2')->isValid()) {
			$filename = "report-siteInfo-" . $ri_module_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_module_3(Request $request) {
		$file = $request->file('ri_module_3');
		$ri_module_3 = $file->getClientOriginalName();

		if($request->file('ri_module_3')->isValid()) {
			$filename = "report-siteAccess-" . $ri_module_3;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_module_4(Request $request) {
		$file = $request->file('ri_module_4');
		$ri_module_4 = $file->getClientOriginalName();

		if($request->file('ri_module_4')->isValid()) {
			$filename = "report-signInfo-" . $ri_module_4;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_dc_cable_con_main_rack(Request $request) {
		$file = $request->file('ri_dc_cable_con_main_rack');
		$ri_dc_cable_con_main_rack = $file->getClientOriginalName();

		if($request->file('ri_dc_cable_con_main_rack')->isValid()) {
			$filename = "report-signInfo-" . $ri_dc_cable_con_main_rack;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_dc_cable_label_main_rack(Request $request) {
		$file = $request->file('ri_dc_cable_label_main_rack');
		$ri_dc_cable_label_main_rack = $file->getClientOriginalName();

		if($request->file('ri_dc_cable_label_main_rack')->isValid()) {
			$filename = "report-signInfo-" . $ri_dc_cable_label_main_rack;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_dc_cable_con_slave_rack(Request $request) {
		$file = $request->file('ri_dc_cable_con_slave_rack');
		$ri_dc_cable_con_slave_rack = $file->getClientOriginalName();

		if($request->file('ri_dc_cable_con_slave_rack')->isValid()) {
			$filename = "report-signInfo-" . $ri_dc_cable_con_slave_rack;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_dc_cable_label_slave_rack(Request $request) {
		$file = $request->file('ri_dc_cable_label_slave_rack');
		$ri_dc_cable_label_slave_rack = $file->getClientOriginalName();

		if($request->file('ri_dc_cable_label_slave_rack')->isValid()) {
			$filename = "report-siteInfo-" . $ri_dc_cable_label_slave_rack;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_gnd_con_1(Request $request) {
		$file = $request->file('ri_gnd_con_1');
		$ri_gnd_con_1 = $file->getClientOriginalName();

		if($request->file('ri_gnd_con_1')->isValid()) {
			$filename = "report-siteAccess-" . $ri_gnd_con_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_gnd_con_2(Request $request) {
		$file = $request->file('ri_gnd_con_2');
		$ri_gnd_con_2 = $file->getClientOriginalName();

		if($request->file('ri_gnd_con_2')->isValid()) {
			$filename = "report-signInfo-" . $ri_gnd_con_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_gnd_con_3(Request $request) {
		$file = $request->file('ri_gnd_con_3');
		$ri_gnd_con_3 = $file->getClientOriginalName();

		if($request->file('ri_gnd_con_3')->isValid()) {
			$filename = "report-signInfo-" . $ri_gnd_con_3;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_gnd_con_4(Request $request) {
		$file = $request->file('ri_gnd_con_4');
		$ri_gnd_con_4 = $file->getClientOriginalName();

		if($request->file('ri_gnd_con_4')->isValid()) {
			$filename = "report-signInfo-" . $ri_gnd_con_4;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_laying_cable_1(Request $request) {
		$file = $request->file('ri_laying_cable_1');
		$ri_laying_cable_1 = $file->getClientOriginalName();

		if($request->file('ri_laying_cable_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_laying_cable_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_laying_cable_2(Request $request) {
		$file = $request->file('ri_laying_cable_2');
		$ri_laying_cable_2 = $file->getClientOriginalName();

		if($request->file('ri_laying_cable_2')->isValid()) {
			$filename = "report-siteInfo-" . $ri_laying_cable_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_laying_cable_3(Request $request) {
		$file = $request->file('ri_laying_cable_3');
		$ri_laying_cable_3 = $file->getClientOriginalName();

		if($request->file('ri_laying_cable_3')->isValid()) {
			$filename = "report-siteAccess-" . $ri_laying_cable_3;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_laying_cable_4(Request $request) {
		$file = $request->file('ri_laying_cable_4');
		$ri_laying_cable_4 = $file->getClientOriginalName();

		if($request->file('ri_laying_cable_4')->isValid()) {
			$filename = "report-signInfo-" . $ri_laying_cable_4;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_fan_main(Request $request) {
		$file = $request->file('ri_fan_main');
		$ri_fan_main = $file->getClientOriginalName();

		if($request->file('ri_fan_main')->isValid()) {
			$filename = "report-signInfo-" . $ri_fan_main;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_fan_slave(Request $request) {
		$file = $request->file('ri_fan_slave');
		$ri_fan_slave = $file->getClientOriginalName();

		if($request->file('ri_fan_slave')->isValid()) {
			$filename = "report-signInfo-" . $ri_fan_slave;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_lamp_main(Request $request) {
		$file = $request->file('ri_lamp_main');
		$ri_lamp_main = $file->getClientOriginalName();

		if($request->file('ri_lamp_main')->isValid()) {
			$filename = "report-signInfo-" . $ri_lamp_main;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_lamp_slave(Request $request) {
		$file = $request->file('ri_lamp_slave');
		$ri_lamp_slave = $file->getClientOriginalName();

		if($request->file('ri_lamp_slave')->isValid()) {
			$filename = "report-siteInfo-" . $ri_lamp_slave;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_panel_dist(Request $request) {
		$file = $request->file('ri_panel_dist');
		$ri_panel_dist = $file->getClientOriginalName();

		if($request->file('ri_panel_dist')->isValid()) {
			$filename = "report-siteAccess-" . $ri_panel_dist;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_ac_measure_rs(Request $request) {
		$file = $request->file('ri_ac_measure_rs');
		$ri_ac_measure_rs = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_rs')->isValid()) {
			$filename = "report-signInfo-" . $ri_ac_measure_rs;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_ac_measure_st(Request $request) {
		$file = $request->file('ri_ac_measure_st');
		$ri_ac_measure_st = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_st')->isValid()) {
			$filename = "report-signInfo-" . $ri_ac_measure_st;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_ac_measure_rt(Request $request) {
		$file = $request->file('ri_ac_measure_rt');
		$ri_ac_measure_rt = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_rt')->isValid()) {
			$filename = "report-signInfo-" . $ri_ac_measure_rt;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_ac_measure_rn(Request $request) {
		$file = $request->file('ri_ac_measure_rn');
		$ri_ac_measure_rn = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_rn')->isValid()) {
			$filename = "report-signInfo-" . $ri_ac_measure_rn;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_ac_measure_sn(Request $request) {
		$file = $request->file('ri_ac_measure_sn');
		$ri_ac_measure_sn = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_sn')->isValid()) {
			$filename = "report-siteInfo-" . $ri_ac_measure_sn;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_ac_measure_tn(Request $request) {
		$file = $request->file('ri_ac_measure_tn');
		$ri_ac_measure_tn = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_tn')->isValid()) {
			$filename = "report-siteAccess-" . $ri_ac_measure_tn;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_ac_measure_ng(Request $request) {
		$file = $request->file('ri_ac_measure_ng');
		$ri_ac_measure_ng = $file->getClientOriginalName();

		if($request->file('ri_ac_measure_ng')->isValid()) {
			$filename = "report-signInfo-" . $ri_ac_measure_ng;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_dc_output(Request $request) {
		$file = $request->file('ri_dc_output');
		$ri_dc_output = $file->getClientOriginalName();

		if($request->file('ri_dc_output')->isValid()) {
			$filename = "report-signInfo-" . $ri_dc_output;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_ac_output(Request $request) {
		$file = $request->file('ri_ac_output');
		$ri_ac_output = $file->getClientOriginalName();

		if($request->file('ri_ac_output')->isValid()) {
			$filename = "report-ri_ac_output-" . $ri_ac_output;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_control_module_setting_1(Request $request) {
		$file = $request->file('ri_control_module_setting_1');
		$ri_control_module_setting_1 = $file->getClientOriginalName();

		if($request->file('ri_control_module_setting_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_control_module_setting_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_control_module_setting_2(Request $request) {
		$file = $request->file('ri_control_module_setting_2');
		$ri_control_module_setting_2 = $file->getClientOriginalName();

		if($request->file('ri_control_module_setting_2')->isValid()) {
			$filename = "report-signInfo-" . $ri_control_module_setting_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_control_module_setting_3(Request $request) {
		$file = $request->file('ri_control_module_setting_3');
		$ri_control_module_setting_3 = $file->getClientOriginalName();

		if($request->file('ri_control_module_setting_3')->isValid()) {
			$filename = "report-siteInfo-" . $ri_control_module_setting_3;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_control_module_setting_4(Request $request) {
		$file = $request->file('ri_control_module_setting_4');
		$ri_control_module_setting_4 = $file->getClientOriginalName();

		if($request->file('ri_control_module_setting_4')->isValid()) {
			$filename = "report-siteAccess-" . $ri_control_module_setting_4;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_alarm_connection_1(Request $request) {
		$file = $request->file('ri_alarm_connection_1');
		$ri_alarm_connection_1 = $file->getClientOriginalName();

		if($request->file('ri_alarm_connection_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_alarm_connection_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_alarm_connection_2(Request $request) {
		$file = $request->file('ri_alarm_connection_2');
		$ri_alarm_connection_2 = $file->getClientOriginalName();

		if($request->file('ri_alarm_connection_2')->isValid()) {
			$filename = "report-signInfo-" . $ri_alarm_connection_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_alarm_label_1(Request $request) {
		$file = $request->file('ri_alarm_label_1');
		$ri_alarm_label_1 = $file->getClientOriginalName();

		if($request->file('ri_alarm_label_1')->isValid()) {
			$filename = "report-signInfo-" . $ri_alarm_label_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_alarm_label_2(Request $request) {
		$file = $request->file('ri_alarm_label_2');
		$ri_alarm_label_2 = $file->getClientOriginalName();

		if($request->file('ri_alarm_label_2')->isValid()) {
			$filename = "report-signInfo-" . $ri_alarm_label_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_rack(Request $request) {
		$file = $request->file('ri_battery_rack');
		$ri_battery_rack = $file->getClientOriginalName();

		if($request->file('ri_battery_rack')->isValid()) {
			$filename = "report-siteInfo-" . $ri_battery_rack;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_bank_inst_1(Request $request) {
		$file = $request->file('ri_battery_bank_inst_1');
		$ri_battery_bank_inst_1 = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_inst_1')->isValid()) {
			$filename = "report-siteAccess-" . $ri_battery_bank_inst_1;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_bank_inst_2(Request $request) {
		$file = $request->file('ri_battery_bank_inst_2');
		$ri_battery_bank_inst_2 = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_inst_2')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_bank_inst_2;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_battery_bank_inst_3(Request $request) {
		$file = $request->file('ri_battery_bank_inst_3');
		$ri_battery_bank_inst_3 = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_inst_3')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_bank_inst_3;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_bank_inst_4(Request $request) {
		$file = $request->file('ri_battery_bank_inst_4');
		$ri_battery_bank_inst_4 = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_inst_4')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_bank_inst_4;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_battery_bank_1_dc_measure(Request $request) {
		$file = $request->file('ri_battery_bank_1_dc_measure');
		$ri_battery_bank_1_dc_measure = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_1_dc_measure')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_bank_1_dc_measure;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_battery_bank_2_dc_measure(Request $request) {
		$file = $request->file('ri_battery_bank_2_dc_measure');
		$ri_battery_bank_2_dc_measure = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_2_dc_measure')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_bank_2_dc_measure;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_bank_3_dc_measure(Request $request) {
		$file = $request->file('ri_battery_bank_3_dc_measure');
		$ri_battery_bank_3_dc_measure = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_3_dc_measure')->isValid()) {
			$filename = "report-siteInfo-" . $ri_battery_bank_3_dc_measure;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_bank_4_dc_measure(Request $request) {
		$file = $request->file('ri_battery_bank_4_dc_measure');
		$ri_battery_bank_4_dc_measure = $file->getClientOriginalName();

		if($request->file('ri_battery_bank_4_dc_measure')->isValid()) {
			$filename = "report-siteInfo-" . $ri_battery_bank_4_dc_measure;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_label(Request $request) {
		$file = $request->file('ri_battery_label');
		$ri_battery_label = $file->getClientOriginalName();

		if($request->file('ri_battery_label')->isValid()) {
			$filename = "report-siteAccess-" . $ri_battery_label;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }
    
    private function upload_ri_battery_main_bar_inst(Request $request) {
		$file = $request->file('ri_battery_main_bar_inst');
		$ri_battery_main_bar_inst = $file->getClientOriginalName();

		if($request->file('ri_battery_main_bar_inst')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_main_bar_inst;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_ri_battery_mcb_fuse(Request $request) {
		$file = $request->file('ri_battery_mcb_fuse');
		$ri_battery_mcb_fuse = $file->getClientOriginalName();

		if($request->file('ri_battery_mcb_fuse')->isValid()) {
			$filename = "report-signInfo-" . $ri_battery_mcb_fuse;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    private function upload_site_layout_rectifier(Request $request) {
		$file = $request->file('site_layout_rectifier');
		$site_layout_rectifier = $file->getClientOriginalName();

		if($request->file('site_layout_rectifier')->isValid()) {
			$filename = "report-signInfo-" . $site_layout_rectifier;
			$uploadPath = "images/report/";
			$file->move($uploadPath, $filename);
			return $filename;
        }
        return false;
    }

    
    // private function deleteSign($request) {
	// 	$file = 'images/report/'.$request->sign;
	// 	if(is_file($file)) {
	// 		unlink($file);
	// 	}
    // }
    
    public function store(Request $request)
    { //HAPUS
        $this->validate($request, [
            'gi_site_information' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_access' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_front_view' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_rear_view' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_left_view' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_lamp_slave' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_5' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_6' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_7' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_foundation_wide' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_foundation_weight' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_foundation_long' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'site_layout' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_base_frame_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_base_frame_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_main_rack_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_slave_rack_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_main_rack_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_slave_rack_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_pln_kwh_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_pln_kwh_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_mcb_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_mcb_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_rectifier_front' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_main_mcb_ac_input' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_mcb_dc_dist' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_controller_module' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_con_main_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_label_main_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_con_slave_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_label_slave_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_fan_main' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_fan_slave' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_lamp_main' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_lamp_slave' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_panel_dist' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_rs' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_st' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_rt' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_rn' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_sn' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_tn' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_ng' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_output' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_output' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_connection_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_connection_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_label_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_label_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_1_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_2_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_3_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_4_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_label' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_main_bar_inst' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_mcb_fuse' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'site_layout_rectifier' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
        ]);

        $doc_no = DB::table('documents')
        ->latest('doc_no')
        ->first()
        ->doc_no;
        
        $photo = new Photo;
        if($request->hasFile('gi_site_information')) {
			// $this->deleteSign($photo);
			$photo->gi_site_information = $this->upload_gi_site_information($request);
        }
        
        if($request->hasFile('gi_site_access')) {
			// $this->deleteSign($photo);
            $photo->gi_site_access = $this->upload_gi_site_access($request);
        }

        if($request->hasFile('gi_site_front_view')) {
			// $this->deleteSign($photo);
			$photo->gi_site_front_view = $this->upload_gi_site_front_view($request);
        }
        
        if($request->hasFile('gi_site_rear_view')) {
			// $this->deleteSign($photo);
            $photo->gi_site_rear_view = $this->upload_gi_site_rear_view($request);
        }

        if($request->hasFile('gi_site_left_view')) {
			// $this->deleteSign($photo);
			$photo->gi_site_left_view = $this->upload_gi_site_left_view($request);
        }
        
        if($request->hasFile('gi_site_right_view')) {
			// $this->deleteSign($photo);
            $photo->gi_site_right_view = $this->upload_gi_site_right_view($request);
        }
        
        if($request->hasFile('nf_installation_before')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_before = $this->upload_nf_installation_before($request);
        }
        
        if($request->hasFile('nf_installation_1')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_1 = $this->upload_nf_installation_1($request);
        }

        if($request->hasFile('nf_installation_2')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_2 = $this->upload_nf_installation_2($request);
        }
        
        if($request->hasFile('nf_installation_3')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_3 = $this->upload_nf_installation_3($request);
        }

        if($request->hasFile('nf_installation_4')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_4 = $this->upload_nf_installation_4($request);
        }
        
        if($request->hasFile('nf_installation_5')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_5 = $this->upload_nf_installation_5($request);
        }

        if($request->hasFile('nf_installation_6')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_6 = $this->upload_nf_installation_6($request);
        }
        
        if($request->hasFile('nf_installation_7')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_7 = $this->upload_nf_installation_7($request);
        }

        if($request->hasFile('nf_foundation_wide')) {
			// $this->deleteSign($photo);
			$photo->nf_foundation_wide = $this->upload_nf_foundation_wide($request);
        }
        
        if($request->hasFile('nf_foundation_weight')) {
			// $this->deleteSign($photo);
            $photo->nf_foundation_weight = $this->upload_nf_foundation_weight($request);
        }

        if($request->hasFile('nf_foundation_long')) {
			// $this->deleteSign($photo);
			$photo->nf_foundation_long = $this->upload_nf_foundation_long($request);
        }
        
        if($request->hasFile('nf_installation_after')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_after = $this->upload_nf_installation_after($request);
        }
        
        if($request->hasFile('site_layout')) {
			// $this->deleteSign($photo);
			$photo->site_layout = $this->upload_site_layout($request);
        }
        
        if($request->hasFile('ri_base_frame_before')) {
			// $this->deleteSign($photo);
            $photo->ri_base_frame_before = $this->upload_ri_base_frame_before($request);
        }

        if($request->hasFile('ri_base_frame_after')) {
			// $this->deleteSign($photo);
			$photo->ri_base_frame_after = $this->upload_ri_base_frame_after($request);
        }
        
        if($request->hasFile('ri_main_rack_1')) {
			// $this->deleteSign($photo);
            $photo->ri_main_rack_1 = $this->upload_ri_main_rack_1($request);
        }

        if($request->hasFile('ri_slave_rack_1')) {
			// $this->deleteSign($photo);
			$photo->ri_slave_rack_1 = $this->upload_ri_slave_rack_1($request);
        }
        
        if($request->hasFile('ri_main_rack_2')) {
			// $this->deleteSign($photo);
            $photo->ri_main_rack_2 = $this->upload_ri_main_rack_2($request);
        }

        if($request->hasFile('ri_slave_rack_2')) {
			// $this->deleteSign($photo);
			$photo->ri_slave_rack_2 = $this->upload_ri_slave_rack_2($request);
        }
        
        if($request->hasFile('ri_pln_kwh_before')) {
			// $this->deleteSign($photo);
            $photo->ri_pln_kwh_before = $this->upload_ri_pln_kwh_before($request);
        }

        if($request->hasFile('ri_pln_kwh_after')) {
			// $this->deleteSign($photo);
			$photo->ri_pln_kwh_after = $this->upload_ri_pln_kwh_after($request);
        }
        
        if($request->hasFile('ri_acpdb_before')) {
			// $this->deleteSign($photo);
            $photo->ri_acpdb_before = $this->upload_ri_acpdb_before($request);
        }

        if($request->hasFile('ri_acpdb_after')) {
			// $this->deleteSign($photo);
			$photo->ri_acpdb_after = $this->upload_ri_acpdb_after($request);
        }
        
        if($request->hasFile('ri_acpdb_mcb_before')) {
			// $this->deleteSign($photo);
            $photo->ri_acpdb_mcb_before = $this->upload_ri_acpdb_mcb_before($request);
        }
        
        if($request->hasFile('ri_acpdb_mcb_after')) {
			// $this->deleteSign($photo);
			$photo->ri_acpdb_mcb_after = $this->upload_ri_acpdb_mcb_after($request);
        }
        
        if($request->hasFile('ri_rectifier_front')) {
			// $this->deleteSign($photo);
            $photo->ri_rectifier_front = $this->upload_ri_rectifier_front($request);
        }

        if($request->hasFile('ri_main_mcb_ac_input')) {
			// $this->deleteSign($photo);
			$photo->ri_main_mcb_ac_input = $this->upload_ri_main_mcb_ac_input($request);
        }
        
        if($request->hasFile('ri_mcb_dc_dist')) {
			// $this->deleteSign($photo);
            $photo->ri_mcb_dc_dist = $this->upload_ri_mcb_dc_dist($request);
        }

        if($request->hasFile('ri_controller_module')) {
			// $this->deleteSign($photo);
			$photo->ri_controller_module = $this->upload_ri_controller_module($request);
        }
        
        if($request->hasFile('ri_module_1')) {
			// $this->deleteSign($photo);
            $photo->ri_module_1 = $this->upload_ri_module_1($request);
        }

        if($request->hasFile('ri_module_2')) {
			// $this->deleteSign($photo);
			$photo->ri_module_2 = $this->upload_ri_module_2($request);
        }
        
        if($request->hasFile('ri_module_3')) {
			// $this->deleteSign($photo);
            $photo->ri_module_3 = $this->upload_ri_module_3($request);
        }

        if($request->hasFile('ri_module_4')) {
			// $this->deleteSign($photo);
			$photo->ri_module_4 = $this->upload_ri_module_4($request);
        }
        
        if($request->hasFile('ri_dc_cable_con_main_rack')) {
			// $this->deleteSign($photo);
            $photo->ri_dc_cable_con_main_rack = $this->upload_ri_dc_cable_con_main_rack($request);
        }

        if($request->hasFile('ri_dc_cable_label_main_rack')) {
			// $this->deleteSign($photo);
			$photo->ri_dc_cable_label_main_rack = $this->upload_ri_dc_cable_label_main_rack($request);
        }
        
        if($request->hasFile('ri_dc_cable_con_slave_rack')) {
			// $this->deleteSign($photo);
            $photo->ri_dc_cable_con_slave_rack = $this->upload_ri_dc_cable_con_slave_rack($request);
        }
        
        if($request->hasFile('ri_dc_cable_label_slave_rack')) {
			// $this->deleteSign($photo);
			$photo->ri_dc_cable_label_slave_rack = $this->upload_ri_dc_cable_label_slave_rack($request);
        }
        
        if($request->hasFile('ri_gnd_con_1')) {
			// $this->deleteSign($photo);
            $photo->ri_gnd_con_1 = $this->upload_ri_gnd_con_1($request);
        }

        if($request->hasFile('ri_gnd_con_2')) {
			// $this->deleteSign($photo);
			$photo->ri_gnd_con_2 = $this->upload_ri_gnd_con_2($request);
        }
        
        if($request->hasFile('ri_gnd_con_3')) {
			// $this->deleteSign($photo);
            $photo->ri_gnd_con_3 = $this->upload_ri_gnd_con_3($request);
        }

        if($request->hasFile('ri_gnd_con_4')) {
			// $this->deleteSign($photo);
			$photo->ri_gnd_con_4 = $this->upload_ri_gnd_con_4($request);
        }
        
        if($request->hasFile('ri_laying_cable_1')) {
			// $this->deleteSign($photo);
            $photo->ri_laying_cable_1 = $this->upload_ri_laying_cable_1($request);
        }

        if($request->hasFile('ri_laying_cable_2')) {
			// $this->deleteSign($photo);
			$photo->ri_laying_cable_2 = $this->upload_ri_laying_cable_2($request);
        }
        
        if($request->hasFile('ri_laying_cable_3')) {
			// $this->deleteSign($photo);
            $photo->ri_laying_cable_3 = $this->upload_ri_laying_cable_3($request);
        }

        if($request->hasFile('ri_laying_cable_4')) {
			// $this->deleteSign($photo);
			$photo->ri_laying_cable_4 = $this->upload_ri_laying_cable_4($request);
        }
        
        if($request->hasFile('ri_fan_main')) {
			// $this->deleteSign($photo);
            $photo->ri_fan_main = $this->upload_ri_fan_main($request);
        }

        if($request->hasFile('ri_fan_slave')) {
			// $this->deleteSign($photo);
			$photo->ri_fan_slave = $this->upload_ri_fan_slave($request);
        }
        
        if($request->hasFile('ri_lamp_main')) {
			// $this->deleteSign($photo);
            $photo->ri_lamp_main = $this->upload_ri_lamp_main($request);
        }
        
        if($request->hasFile('ri_lamp_slave')) {
			// $this->deleteSign($photo);
			$photo->ri_lamp_slave = $this->upload_ri_lamp_slave($request);
        }
        
        if($request->hasFile('ri_panel_dist')) {
			// $this->deleteSign($photo);
            $photo->ri_panel_dist = $this->upload_ri_panel_dist($request);
        }

        if($request->hasFile('ri_ac_measure_rs')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_rs = $this->upload_ri_ac_measure_rs($request);
        }
        
        if($request->hasFile('ri_ac_measure_st')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_measure_st = $this->upload_ri_ac_measure_st($request);
        }

        if($request->hasFile('ri_ac_measure_rt')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_rt = $this->upload_ri_ac_measure_rt($request);
        }
        
        if($request->hasFile('ri_ac_measure_rn')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_measure_rn = $this->upload_ri_ac_measure_rn($request);
        }

        if($request->hasFile('ri_ac_measure_sn')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_sn = $this->upload_ri_ac_measure_sn($request);
        }
        
        if($request->hasFile('ri_ac_measure_tn')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_measure_tn = $this->upload_ri_ac_measure_tn($request);
        }

        if($request->hasFile('ri_ac_measure_ng')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_ng = $this->upload_ri_ac_measure_ng($request);
        }
        
        if($request->hasFile('ri_dc_output')) {
			// $this->deleteSign($photo);
            $photo->ri_dc_output = $this->upload_ri_dc_output($request);
        }
        
        if($request->hasFile('ri_ac_output')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_output = $this->upload_ri_ac_output($request);
        }

        if($request->hasFile('ri_control_module_setting_1')) {
			// $this->deleteSign($photo);
			$photo->ri_control_module_setting_1 = $this->upload_ri_control_module_setting_1($request);
        }
        
        if($request->hasFile('ri_control_module_setting_2')) {
			// $this->deleteSign($photo);
            $photo->ri_control_module_setting_2 = $this->upload_ri_control_module_setting_2($request);
        }
        
        if($request->hasFile('ri_control_module_setting_3')) {
			// $this->deleteSign($photo);
			$photo->ri_control_module_setting_3 = $this->upload_ri_control_module_setting_3($request);
        }
        
        if($request->hasFile('ri_control_module_setting_4')) {
			// $this->deleteSign($photo);
            $photo->ri_control_module_setting_4 = $this->upload_ri_control_module_setting_4($request);
        }

        if($request->hasFile('ri_alarm_connection_1')) {
			// $this->deleteSign($photo);
			$photo->ri_alarm_connection_1 = $this->upload_ri_alarm_connection_1($request);
        }
        
        if($request->hasFile('ri_alarm_connection_2')) {
			// $this->deleteSign($photo);
            $photo->ri_alarm_connection_2 = $this->upload_ri_alarm_connection_2($request);
        }

        if($request->hasFile('ri_alarm_label_1')) {
			// $this->deleteSign($photo);
			$photo->ri_alarm_label_1 = $this->upload_ri_alarm_label_1($request);
        }
        
        if($request->hasFile('ri_alarm_label_2')) {
			// $this->deleteSign($photo);
            $photo->ri_alarm_label_2 = $this->upload_ri_alarm_label_2($request);
        }

        if($request->hasFile('ri_battery_rack')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_rack = $this->upload_ri_battery_rack($request);
        }
        
        if($request->hasFile('ri_battery_bank_inst_1')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_inst_1 = $this->upload_ri_battery_bank_inst_1($request);
        }

        if($request->hasFile('ri_battery_bank_inst_2')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_inst_2 = $this->upload_ri_battery_bank_inst_2($request);
        }
        
        if($request->hasFile('ri_battery_bank_inst_3')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_inst_3 = $this->upload_ri_battery_bank_inst_3($request);
        }
        
        if($request->hasFile('ri_battery_bank_inst_4')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_inst_4 = $this->upload_ri_battery_bank_inst_4($request);
        }

        if($request->hasFile('ri_battery_bank_1_dc_measure')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_1_dc_measure = $this->upload_ri_battery_bank_1_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_bank_2_dc_measure')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_2_dc_measure = $this->upload_ri_battery_bank_2_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_bank_3_dc_measure')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_3_dc_measure = $this->upload_ri_battery_bank_3_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_bank_4_dc_measure')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_4_dc_measure = $this->upload_ri_battery_bank_4_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_label')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_label = $this->upload_ri_battery_label($request);
        }

        if($request->hasFile('ri_battery_main_bar_inst')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_main_bar_inst = $this->upload_ri_battery_main_bar_inst($request);
        }
        
        if($request->hasFile('ri_battery_mcb_fuse')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_mcb_fuse = $this->upload_ri_battery_mcb_fuse($request);
        }

        if($request->hasFile('site_layout_rectifier')) {
			// $this->deleteSign($photo);
			$photo->site_layout_rectifier = $this->upload_site_layout_rectifier($request);
        }
        
        $photo->doc_no = $doc_no;
        $photo->save();
        
       
        return redirect('/report')->with('message', 'Dokumen & Foto Berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photos = Photo::findOrFail($id);
		return view('pages.photos.update', ['photos' => $photos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo = Photo::findOrFail($id);
        $this->validate($request, [
            'gi_site_information' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_access' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_front_view' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_rear_view' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'gi_site_left_view' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_lamp_slave' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_5' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_6' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_7' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_foundation_wide' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_foundation_weight' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_foundation_long' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'nf_installation_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'site_layout' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_base_frame_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_base_frame_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_main_rack_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_slave_rack_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_main_rack_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_slave_rack_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_pln_kwh_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_pln_kwh_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_mcb_before' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_acpdb_mcb_after' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_rectifier_front' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_main_mcb_ac_input' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_mcb_dc_dist' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_controller_module' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_module_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_con_main_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_label_main_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_con_slave_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_cable_label_slave_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_gnd_con_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_laying_cable_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_fan_main' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_fan_slave' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_lamp_main' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_lamp_slave' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_panel_dist' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_rs' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_st' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_rt' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_rn' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_sn' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_tn' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_measure_ng' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_dc_output' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_ac_output' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_control_module_setting_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_connection_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_connection_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_label_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_alarm_label_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_rack' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_1' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_2' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_3' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_inst_4' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_1_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_2_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_3_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_bank_4_dc_measure' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_label' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_main_bar_inst' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'ri_battery_mcb_fuse' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
            'site_layout_rectifier' => 'file|image|mimes:jpeg,png,jpg,JPG,PNG,JPEG|MAX:30000',
        ]);

        $doc_no = DB::table('documents')
        ->latest('doc_no')
        ->first()
        ->doc_no;
        
        if($request->hasFile('gi_site_information')) {
			// $this->deleteSign($photo);
			$photo->gi_site_information = $this->upload_gi_site_information($request);
        }
        
        if($request->hasFile('gi_site_access')) {
			// $this->deleteSign($photo);
            $photo->gi_site_access = $this->upload_gi_site_access($request);
        }

        if($request->hasFile('gi_site_front_view')) {
			// $this->deleteSign($photo);
			$photo->gi_site_front_view = $this->upload_gi_site_front_view($request);
        }
        
        if($request->hasFile('gi_site_rear_view')) {
			// $this->deleteSign($photo);
            $photo->gi_site_rear_view = $this->upload_gi_site_rear_view($request);
        }

        if($request->hasFile('gi_site_left_view')) {
			// $this->deleteSign($photo);
			$photo->gi_site_left_view = $this->upload_gi_site_left_view($request);
        }
        
        if($request->hasFile('gi_site_right_view')) {
			// $this->deleteSign($photo);
            $photo->gi_site_right_view = $this->upload_gi_site_right_view($request);
        }
        
        if($request->hasFile('nf_installation_before')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_before = $this->upload_nf_installation_before($request);
        }
        
        if($request->hasFile('nf_installation_1')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_1 = $this->upload_nf_installation_1($request);
        }

        if($request->hasFile('nf_installation_2')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_2 = $this->upload_nf_installation_2($request);
        }
        
        if($request->hasFile('nf_installation_3')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_3 = $this->upload_nf_installation_3($request);
        }

        if($request->hasFile('nf_installation_4')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_4 = $this->upload_nf_installation_4($request);
        }
        
        if($request->hasFile('nf_installation_5')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_5 = $this->upload_nf_installation_5($request);
        }

        if($request->hasFile('nf_installation_6')) {
			// $this->deleteSign($photo);
			$photo->nf_installation_6 = $this->upload_nf_installation_6($request);
        }
        
        if($request->hasFile('nf_installation_7')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_7 = $this->upload_nf_installation_7($request);
        }

        if($request->hasFile('nf_foundation_wide')) {
			// $this->deleteSign($photo);
			$photo->nf_foundation_wide = $this->upload_nf_foundation_wide($request);
        }
        
        if($request->hasFile('nf_foundation_weight')) {
			// $this->deleteSign($photo);
            $photo->nf_foundation_weight = $this->upload_nf_foundation_weight($request);
        }

        if($request->hasFile('nf_foundation_long')) {
			// $this->deleteSign($photo);
			$photo->nf_foundation_long = $this->upload_nf_foundation_long($request);
        }
        
        if($request->hasFile('nf_installation_after')) {
			// $this->deleteSign($photo);
            $photo->nf_installation_after = $this->upload_nf_installation_after($request);
        }
        
        if($request->hasFile('site_layout')) {
			// $this->deleteSign($photo);
			$photo->site_layout = $this->upload_site_layout($request);
        }
        
        if($request->hasFile('ri_base_frame_before')) {
			// $this->deleteSign($photo);
            $photo->ri_base_frame_before = $this->upload_ri_base_frame_before($request);
        }

        if($request->hasFile('ri_base_frame_after')) {
			// $this->deleteSign($photo);
			$photo->ri_base_frame_after = $this->upload_ri_base_frame_after($request);
        }
        
        if($request->hasFile('ri_main_rack_1')) {
			// $this->deleteSign($photo);
            $photo->ri_main_rack_1 = $this->upload_ri_main_rack_1($request);
        }

        if($request->hasFile('ri_slave_rack_1')) {
			// $this->deleteSign($photo);
			$photo->ri_slave_rack_1 = $this->upload_ri_slave_rack_1($request);
        }
        
        if($request->hasFile('ri_main_rack_2')) {
			// $this->deleteSign($photo);
            $photo->ri_main_rack_2 = $this->upload_ri_main_rack_2($request);
        }

        if($request->hasFile('ri_slave_rack_2')) {
			// $this->deleteSign($photo);
			$photo->ri_slave_rack_2 = $this->upload_ri_slave_rack_2($request);
        }
        
        if($request->hasFile('ri_pln_kwh_before')) {
			// $this->deleteSign($photo);
            $photo->ri_pln_kwh_before = $this->upload_ri_pln_kwh_before($request);
        }

        if($request->hasFile('ri_pln_kwh_after')) {
			// $this->deleteSign($photo);
			$photo->ri_pln_kwh_after = $this->upload_ri_pln_kwh_after($request);
        }
        
        if($request->hasFile('ri_acpdb_before')) {
			// $this->deleteSign($photo);
            $photo->ri_acpdb_before = $this->upload_ri_acpdb_before($request);
        }

        if($request->hasFile('ri_acpdb_after')) {
			// $this->deleteSign($photo);
			$photo->ri_acpdb_after = $this->upload_ri_acpdb_after($request);
        }
        
        if($request->hasFile('ri_acpdb_mcb_before')) {
			// $this->deleteSign($photo);
            $photo->ri_acpdb_mcb_before = $this->upload_ri_acpdb_mcb_before($request);
        }
        
        if($request->hasFile('ri_acpdb_mcb_after')) {
			// $this->deleteSign($photo);
			$photo->ri_acpdb_mcb_after = $this->upload_ri_acpdb_mcb_after($request);
        }
        
        if($request->hasFile('ri_rectifier_front')) {
			// $this->deleteSign($photo);
            $photo->ri_rectifier_front = $this->upload_ri_rectifier_front($request);
        }

        if($request->hasFile('ri_main_mcb_ac_input')) {
			// $this->deleteSign($photo);
			$photo->ri_main_mcb_ac_input = $this->upload_ri_main_mcb_ac_input($request);
        }
        
        if($request->hasFile('ri_mcb_dc_dist')) {
			// $this->deleteSign($photo);
            $photo->ri_mcb_dc_dist = $this->upload_ri_mcb_dc_dist($request);
        }

        if($request->hasFile('ri_controller_module')) {
			// $this->deleteSign($photo);
			$photo->ri_controller_module = $this->upload_ri_controller_module($request);
        }
        
        if($request->hasFile('ri_module_1')) {
			// $this->deleteSign($photo);
            $photo->ri_module_1 = $this->upload_ri_module_1($request);
        }

        if($request->hasFile('ri_module_2')) {
			// $this->deleteSign($photo);
			$photo->ri_module_2 = $this->upload_ri_module_2($request);
        }
        
        if($request->hasFile('ri_module_3')) {
			// $this->deleteSign($photo);
            $photo->ri_module_3 = $this->upload_ri_module_3($request);
        }

        if($request->hasFile('ri_module_4')) {
			// $this->deleteSign($photo);
			$photo->ri_module_4 = $this->upload_ri_module_4($request);
        }
        
        if($request->hasFile('ri_dc_cable_con_main_rack')) {
			// $this->deleteSign($photo);
            $photo->ri_dc_cable_con_main_rack = $this->upload_ri_dc_cable_con_main_rack($request);
        }

        if($request->hasFile('ri_dc_cable_label_main_rack')) {
			// $this->deleteSign($photo);
			$photo->ri_dc_cable_label_main_rack = $this->upload_ri_dc_cable_label_main_rack($request);
        }
        
        if($request->hasFile('ri_dc_cable_con_slave_rack')) {
			// $this->deleteSign($photo);
            $photo->ri_dc_cable_con_slave_rack = $this->upload_ri_dc_cable_con_slave_rack($request);
        }
        
        if($request->hasFile('ri_dc_cable_label_slave_rack')) {
			// $this->deleteSign($photo);
			$photo->ri_dc_cable_label_slave_rack = $this->upload_ri_dc_cable_label_slave_rack($request);
        }
        
        if($request->hasFile('ri_gnd_con_1')) {
			// $this->deleteSign($photo);
            $photo->ri_gnd_con_1 = $this->upload_ri_gnd_con_1($request);
        }

        if($request->hasFile('ri_gnd_con_2')) {
			// $this->deleteSign($photo);
			$photo->ri_gnd_con_2 = $this->upload_ri_gnd_con_2($request);
        }
        
        if($request->hasFile('ri_gnd_con_3')) {
			// $this->deleteSign($photo);
            $photo->ri_gnd_con_3 = $this->upload_ri_gnd_con_3($request);
        }

        if($request->hasFile('ri_gnd_con_4')) {
			// $this->deleteSign($photo);
			$photo->ri_gnd_con_4 = $this->upload_ri_gnd_con_4($request);
        }
        
        if($request->hasFile('ri_laying_cable_1')) {
			// $this->deleteSign($photo);
            $photo->ri_laying_cable_1 = $this->upload_ri_laying_cable_1($request);
        }

        if($request->hasFile('ri_laying_cable_2')) {
			// $this->deleteSign($photo);
			$photo->ri_laying_cable_2 = $this->upload_ri_laying_cable_2($request);
        }
        
        if($request->hasFile('ri_laying_cable_3')) {
			// $this->deleteSign($photo);
            $photo->ri_laying_cable_3 = $this->upload_ri_laying_cable_3($request);
        }

        if($request->hasFile('ri_laying_cable_4')) {
			// $this->deleteSign($photo);
			$photo->ri_laying_cable_4 = $this->upload_ri_laying_cable_4($request);
        }
        
        if($request->hasFile('ri_fan_main')) {
			// $this->deleteSign($photo);
            $photo->ri_fan_main = $this->upload_ri_fan_main($request);
        }

        if($request->hasFile('ri_fan_slave')) {
			// $this->deleteSign($photo);
			$photo->ri_fan_slave = $this->upload_ri_fan_slave($request);
        }
        
        if($request->hasFile('ri_lamp_main')) {
			// $this->deleteSign($photo);
            $photo->ri_lamp_main = $this->upload_ri_lamp_main($request);
        }
        
        if($request->hasFile('ri_lamp_slave')) {
			// $this->deleteSign($photo);
			$photo->ri_lamp_slave = $this->upload_ri_lamp_slave($request);
        }
        
        if($request->hasFile('ri_panel_dist')) {
			// $this->deleteSign($photo);
            $photo->ri_panel_dist = $this->upload_ri_panel_dist($request);
        }

        if($request->hasFile('ri_ac_measure_rs')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_rs = $this->upload_ri_ac_measure_rs($request);
        }
        
        if($request->hasFile('ri_ac_measure_st')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_measure_st = $this->upload_ri_ac_measure_st($request);
        }

        if($request->hasFile('ri_ac_measure_rt')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_rt = $this->upload_ri_ac_measure_rt($request);
        }
        
        if($request->hasFile('ri_ac_measure_rn')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_measure_rn = $this->upload_ri_ac_measure_rn($request);
        }

        if($request->hasFile('ri_ac_measure_sn')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_sn = $this->upload_ri_ac_measure_sn($request);
        }
        
        if($request->hasFile('ri_ac_measure_tn')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_measure_tn = $this->upload_ri_ac_measure_tn($request);
        }

        if($request->hasFile('ri_ac_measure_ng')) {
			// $this->deleteSign($photo);
			$photo->ri_ac_measure_ng = $this->upload_ri_ac_measure_ng($request);
        }
        
        if($request->hasFile('ri_dc_output')) {
			// $this->deleteSign($photo);
            $photo->ri_dc_output = $this->upload_ri_dc_output($request);
        }
        
        if($request->hasFile('ri_ac_output')) {
			// $this->deleteSign($photo);
            $photo->ri_ac_output = $this->upload_ri_ac_output($request);
        }

        if($request->hasFile('ri_control_module_setting_1')) {
			// $this->deleteSign($photo);
			$photo->ri_control_module_setting_1 = $this->upload_ri_control_module_setting_1($request);
        }
        
        if($request->hasFile('ri_control_module_setting_2')) {
			// $this->deleteSign($photo);
            $photo->ri_control_module_setting_2 = $this->upload_ri_control_module_setting_2($request);
        }
        
        if($request->hasFile('ri_control_module_setting_3')) {
			// $this->deleteSign($photo);
			$photo->ri_control_module_setting_3 = $this->upload_ri_control_module_setting_3($request);
        }
        
        if($request->hasFile('ri_control_module_setting_4')) {
			// $this->deleteSign($photo);
            $photo->ri_control_module_setting_4 = $this->upload_ri_control_module_setting_4($request);
        }

        if($request->hasFile('ri_alarm_connection_1')) {
			// $this->deleteSign($photo);
			$photo->ri_alarm_connection_1 = $this->upload_ri_alarm_connection_1($request);
        }
        
        if($request->hasFile('ri_alarm_connection_2')) {
			// $this->deleteSign($photo);
            $photo->ri_alarm_connection_2 = $this->upload_ri_alarm_connection_2($request);
        }

        if($request->hasFile('ri_alarm_label_1')) {
			// $this->deleteSign($photo);
			$photo->ri_alarm_label_1 = $this->upload_ri_alarm_label_1($request);
        }
        
        if($request->hasFile('ri_alarm_label_2')) {
			// $this->deleteSign($photo);
            $photo->ri_alarm_label_2 = $this->upload_ri_alarm_label_2($request);
        }

        if($request->hasFile('ri_battery_rack')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_rack = $this->upload_ri_battery_rack($request);
        }
        
        if($request->hasFile('ri_battery_bank_inst_1')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_inst_1 = $this->upload_ri_battery_bank_inst_1($request);
        }

        if($request->hasFile('ri_battery_bank_inst_2')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_inst_2 = $this->upload_ri_battery_bank_inst_2($request);
        }
        
        if($request->hasFile('ri_battery_bank_inst_3')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_inst_3 = $this->upload_ri_battery_bank_inst_3($request);
        }
        
        if($request->hasFile('ri_battery_bank_inst_4')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_inst_4 = $this->upload_ri_battery_bank_inst_4($request);
        }

        if($request->hasFile('ri_battery_bank_1_dc_measure')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_1_dc_measure = $this->upload_ri_battery_bank_1_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_bank_2_dc_measure')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_bank_2_dc_measure = $this->upload_ri_battery_bank_2_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_bank_3_dc_measure')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_3_dc_measure = $this->upload_ri_battery_bank_3_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_bank_4_dc_measure')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_bank_4_dc_measure = $this->upload_ri_battery_bank_4_dc_measure($request);
        }
        
        if($request->hasFile('ri_battery_label')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_label = $this->upload_ri_battery_label($request);
        }

        if($request->hasFile('ri_battery_main_bar_inst')) {
			// $this->deleteSign($photo);
			$photo->ri_battery_main_bar_inst = $this->upload_ri_battery_main_bar_inst($request);
        }
        
        if($request->hasFile('ri_battery_mcb_fuse')) {
			// $this->deleteSign($photo);
            $photo->ri_battery_mcb_fuse = $this->upload_ri_battery_mcb_fuse($request);
        }

        if($request->hasFile('site_layout_rectifier')) {
			// $this->deleteSign($photo);
			$photo->site_layout_rectifier = $this->upload_site_layout_rectifier($request);
        }
        
        $photo->save();
        
        return redirect('/report')->with('message', 'Dokumen & Foto Berhasil Diupdate!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photos = Photo::findOrFail($id);
		$photos->delete();
		return redirect('/report')->with('message', 'Photo Berhasil Dihapus!');
    }
}
