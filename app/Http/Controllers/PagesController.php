<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function dashboard()
    {
        return view('pages.home');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function createatp()
    {
        return view('pages.create-atp');
    }

    public function report()
    {
        return view('pages.report');
    }

    public function createuser()
    {
        return view('pages.createuser');
    }

}
