<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Document;
use PDF;
use Illuminate\Support\Facades\Validator;
use PhpParser\Comment\Doc;
use Storage;
use App\Models\Photo;
use DataTables;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReportNotificationEmail;
use App\Models\EmailLog;

class DocumentsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$documents = DB::table('documents')->where('doc_type', '=', '0')->orWhere('doc_type', null)->get();
		return view('pages.report.select', ['documents' => $documents]);
	}

    public function indexdoc1()
	{
		$documents = DB::table('documents')->where('doc_type', '=', '1')->get();
		return view('pages.report.selectrectbatt', ['documents' => $documents]);
	}

	public function indexapproval()
	{
		$documents = DB::table('documents')->get();
		return view('pages.approval', ['documents' => $documents]);
	}

	public function json(){
        return Datatables::of(Document::all())->make(true);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$documents = DB::table('documents')->get();
        return view('pages.report.create',['documents'=>$documents]);
	}

	// Upload + Validasi file BoQ
	private function uploadBoq(Request $request) {
		$gambar = $request->file('boq');
		$type = $gambar->getClientOriginalExtension();

		if($request->file('boq')->isValid()) {
			$filename = "boq-" . $request->site_id . "." . $type;
			$uploadPath = "boq/";
			$request->file('boq')->move($uploadPath, $filename);
			return $filename;
		}
		return false;
	}

	// Hapus file BoQ di Local Drive
	private function hapusBoq(Documents $documents) {
		$exist = Storage::disk('boq')->exist($documents->boq);
		if(isset($documents->file_boq) && $exist) {
			$delete = Storage::disk('boq')->delete($documents->boq);
			if($delete) {
				return true;
			}
			return false;
		}
	}

	// Delete file BoQ
	private function deleteBoq($request) {
		$file = 'boq/'.$request->boq;
		if(is_file($file)) {
			unlink($file);
		}
	}

	// Upload + Validasi file BAPA
	private function uploadBapa(Request $request) {
		$gambar = $request->file('bapa');
		$type = $gambar->getClientOriginalExtension();

		if($request->file('bapa')->isValid()) {
			$filename = "bapa-" . $request->site_id . "." . $type;
			$uploadPath = "bapa/";
			$request->file('bapa')->move($uploadPath, $filename);
			return $filename;
		}
		return false;
	}

	// Hapus file BAPA di Local Drive
	private function hapusBapa(Documents $documents) {
		$exist = Storage::disk('bapa')->exist($documents->bapa);
		if(isset($documents->file_bapa) && $exist) {
			$delete = Storage::disk('bapa')->delete($documents->bapa);
			if($delete) {
				return true;
			}
			return false;
		}
	}

	// Delete file BAPA
	private function deleteBapa($request) {
		$file = 'bapa/'.$request->bapa;
		if(is_file($file)) {
			unlink($file);
		}
	}

	// Upload + Validasi file tambahan
	private function uploadTambahan(Request $request) {
        $gambar = $request->file('tambahan');
        if ($gambar != null) {
		    $type = $gambar->getClientOriginalExtension();

            if($request->file('tambahan')->isValid()) {
                $filename = "tambahan-" . $request->site_id . "." . $type;
                $uploadPath = "tambahan/";
                $request->file('tambahan')->move($uploadPath, $filename);
                return $filename;
            }
        }
        return false;
	}

	// Hapus file tambahan di Local Drive
	private function hapusTambahan(Documents $documents) {
		$exist = Storage::disk('tambahan')->exist($documents->tambahan);
		if(isset($documents->file_tambahan) && $exist) {
			$delete = Storage::disk('tambahan')->delete($documents->tambahan);
			if($delete) {
				return true;
			}
			return false;
		}
	}

	// Delete file tambahan
	private function deleteTambahan($request) {
		$file = 'tambahan/'.$request->tambahan;
		if(is_file($file)) {
			unlink($file);
		}
	}

	public function getTimestamp($request) {
		$current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();

		return $current_date_time;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'bapa' => 'required|file|max:30000|mimes:pdf',
			'boq' => 'required|file|max:30000|mimes:pdf',
			'tambahan' => 'file|max:30000|mimes:pdf',
		]);

		DB::table('documents')->insert([

			// Meta Data
			'doc_date' => $this->getTimestamp($request),
			//'doc_no' => $request->doc_no,
			//'doc_ver' => $request->doc_ver,

			//File BAPA & BoQ
			'file_bapa' => $this->uploadBapa($request),
			'file_boq' => $this->uploadBoq($request),
			'file_tambahan' => $this->uploadTambahan($request),

			// Basic Information
			'doc_name' => $request->doc_name, 'doc_info' => $request->doc_info,
            'doc_type' => $request->doc_type,
			'project_id' => $request->project_id,
			'project_name' => $request->project_name,
			'site_name' =>$request->site_name, 'site_id' => $request->site_id,
			'site_loc' => $request->site_loc, 'site_area' => $request->site_area,


			// Document Status
			'doc_status' => 0,
			'doc_status_temp' => 0,

			// Rectifier System
			'rectifier_series' => $request->rectifier_series, 'rectifier_type' => $request->rectifier_type, 'rectifier_capacity' => $request->rectifier_capacity,
			'voltage_input' => $request->voltage_input,

			// Visual Check
			// 1.1 Rack
			'rack_type' => $request->rack_type,
			'rack_type_cond' => $request->rack_type_cond,
			'rack_dimension' => $request->rack_dimension,

			// 1.2 Name Plate
			'tsel_name_plate' => $request->tsel_name_plate, 'tsel_name_plate_cond' => $request->tsel_name_plate_cond,
			'vendor_name_plate' => $request->vendor_name_plate, 'vendor_name_plate_cond' => $request->vendor_name_plate_cond,

			// 1.3 Incoming AC Distribution
			'ac_cable_type_size' => $request->ac_cable_type_size, 'ac_cable_type_size_cond' => $request->ac_cable_type_size_cond,
			'ac_mcb_brand' => $request->ac_mcb_brand, 'ac_mcb_brand_cond' => $request->ac_mcb_brand_cond,
			'ac_mcb_type' => $request->ac_mcb_type, 'ac_mcb_type_cond' => $request->ac_mcb_type_cond,
			'ac_mcb_rate' => $request->ac_mcb_rate, 'ac_mcb_rate_cond' => $request->ac_mcb_rate_cond,

			// 1.4 Rectifier Module Distribution
			'rec_mcb_brand' => $request->rec_mcb_brand, 'rec_mcb_brand_cond' => $request->rec_mcb_brand_cond,
			'rec_mcb_type' => $request->rec_mcb_type, 'rec_mcb_type_cond' => $request->rec_mcb_type_cond,
			'rec_mcb_rate' => $request->rec_mcb_rate, 'rec_mcb_rate_cond' => $request->rec_mcb_rate_cond,

			// 1.5 Battery Distribution
			'batt_mcb_brand' => $request->batt_mcb_brand, 'batt_mcb_brand_cond' => $request->batt_mcb_brand_cond,
			'batt_mcb_type' => $request->batt_mcb_type, 'batt_mcb_type_cond' => $request->batt_mcb_type_cond,
			'batt_mcb_rate' => $request->batt_mcb_rate, 'batt_mcb_rate_cond' => $request->batt_mcb_rate_cond,
			'batt_mcb_quantity' => $request->batt_mcb_quantity, 'batt_mcb_quantity_cond' => $request->batt_mcb_quantity_cond,

			// 1.6 Load Distribution
			'load_mcb_brand' => $request->load_mcb_brand, 'load_mcb_brand_cond' => $request->load_mcb_brand_cond,
			'load_mcb_type' => $request->load_mcb_type, 'load_mcb_type_cond' => $request->load_mcb_type_cond,
			'load_mcb_rate_1' => $request->load_mcb_rate_1, 'load_mcb_rate_1_cond' => $request->load_mcb_rate_1_cond,
			'load_mcb_rate_2' => $request->load_mcb_rate_2, 'load_mcb_rate_2_cond' => $request->load_mcb_rate_2_cond,
			'load_mcb_rate_3' => $request->load_mcb_rate_3, 'load_mcb_rate_3_cond' => $request->load_mcb_rate_3_cond,

			// 1.7 Incoming Arrester
			'arrester_brand' => $request->arrester_brand, 'arrester_brand_cond' => $request->arrester_brand_cond,
			'arrester_type' => $request->arrester_type, 'arrester_type_cond' => $request->arrester_type_cond,

			// 1.8 Subtrack System Module
			'subtrack_quantity' => $request->subtrack_quantity, 'subtrack_quantity_cond' => $request->subtrack_quantity_cond,
			'subtrack_serial_number_1' => $request->subtrack_serial_number_1, 'subtrack_serial_number_1_cond' => $request->subtrack_serial_number_1_cond,
			'subtrack_serial_number_2' => $request->subtrack_serial_number_2, 'subtrack_serial_number_2_cond' => $request->subtrack_serial_number_2_cond,
			'subtrack_serial_number_3' => $request->subtrack_serial_number_3, 'subtrack_serial_number_3_cond' => $request->subtrack_serial_number_3_cond,

			// 1.9 Battery Rack Type
			'batt_rack_type'=> $request->batt_rack_type,
			'batt_rack_dimension' => $request->batt_rack_dimension, 'batt_rack_dimension_cond' => $request->batt_rack_dimension_cond,
			'batt_rack_capacity_bank_block' => $request->batt_rack_capacity_bank_block, 'batt_rack_capacity_bank_block_cond' => $request->batt_rack_capacity_bank_block_cond,
			'batt_rack_quantity' => $request->batt_rack_quantity, 'batt_rack_quantity_cond' => $request->batt_rack_quantity_cond,

			// 1.10 Battery Security System
			'batt_cage' => $request->batt_cage, 'batt_cage_cond' => $request->batt_cage_cond,
			'batt_limit_switch_alarm' => $request->batt_limit_switch_alarm, 'batt_limit_switch_alarm_cond' => $request->batt_limit_switch_alarm_cond,

			// 1.11 DDF for Alarm
			'ddf_wiring' => $request->ddf_wiring, 'ddf_wiring_cond' => $request->ddf_wiring_cond,
			'ddf_alarm' => $request->ddf_alarm, 'ddf_alarm_cond' => $request->ddf_alarm_cond,

			// 2.1 Monitor Control card
			'monitor_control_type' => $request->monitor_control_type, 'monitor_control_type_cond' => $request->monitor_control_type_cond,
			'monitor_control_serial_number' => $request->monitor_control_serial_number, 'monitor_control_serial_number_cond' => $request->monitor_control_serial_number_cond,

			// 3.1 Battery
			'batt_brand' => $request->batt_brand, 'batt_brand_cond' => $request->batt_brand_cond,
			'batt_type' => $request->batt_type, 'batt_type_cond' => $request->batt_type_cond,
			'batt_bank' => $request->batt_bank, 'batt_bank_cond' => $request->batt_bank_cond,
			'batt_phy_cond' => $request->batt_phy_cond, 'batt_phy_cond_cond' => $request->batt_phy_cond_cond,
			'batt_serial_number_label' => $request->batt_serial_number_label, 'batt_serial_number_label_cond' => $request->batt_serial_number_label_cond,
			'batt_bank_label' => $request->batt_bank_label, 'batt_bank_label_cond' => $request->batt_bank_label_cond,

			// 3.2 Serial Number Battery
			'batt_sn_1' => $request->batt_sn_1, 'batt_sn_1_bank' => $request->batt_sn_1_bank,
			'batt_sn_2' => $request->batt_sn_2, 'batt_sn_2_bank' => $request->batt_sn_2_bank,
			'batt_sn_3' => $request->batt_sn_3, 'batt_sn_3_bank' => $request->batt_sn_3_bank,
			'batt_sn_4' => $request->batt_sn_4, 'batt_sn_4_bank' => $request->batt_sn_4_bank,
			'batt_sn_5' => $request->batt_sn_5, 'batt_sn_5_bank' => $request->batt_sn_5_bank,
			//'batt_sn_6' => $request->batt_sn_6, 'batt_sn_6_bank' => $request->batt_sn_6_bank,
			//'batt_sn_7' => $request->batt_sn_7, 'batt_sn_7_bank' => $request->batt_sn_7_bank,
			//'batt_sn_8' => $request->batt_sn_8, 'batt_sn_8_bank' => $request->batt_sn_8_bank,
			//'batt_sn_9' => $request->batt_sn_9, 'batt_sn_9_bank' => $request->batt_sn_9_bank,
			//'batt_sn_10' => $request->batt_sn_10, 'batt_sn_10_bank' => $request->batt_sn_10_bank,


			// 4.1 Rectifier Module
			'rec_mod_brand' => $request->rec_mod_brand, 'rec_mod_brand_cond' => $request->rec_mod_brand_cond,
			'rec_mod_type' => $request->rec_mod_type, 'rec_mod_type_cond' => $request->rec_mod_type_cond,
			'rec_mod_capacity' => $request->rec_mod_capacity, 'rec_mod_capacity_cond' => $request->rec_mod_capacity_cond,

			// 4.2 Serial Number Rectifier Module
			'rec_sn1' => $request->rec_sn1, 'rec_sn1_phase' => $request->rec_sn1_phase,
			'rec_sn2' => $request->rec_sn2, 'rec_sn2_phase' => $request->rec_sn2_phase,
			'rec_sn3' => $request->rec_sn3, 'rec_sn3_phase' => $request->rec_sn3_phase,
			'rec_sn4' => $request->rec_sn4, 'rec_sn4_phase' => $request->rec_sn4_phase,
			'rec_sn5' => $request->rec_sn5, 'rec_sn5_phase' => $request->rec_sn5_phase,
			'rec_sn6' => $request->rec_sn6, 'rec_sn6_phase' => $request->rec_sn6_phase,
			'rec_sn7' => $request->rec_sn7, 'rec_sn7_phase' => $request->rec_sn7_phase,
			'rec_sn8' => $request->rec_sn8, 'rec_sn8_phase' => $request->rec_sn8_phase,
			'rec_sn9' => $request->rec_sn9, 'rec_sn9_phase' => $request->rec_sn9_phase,

			// 5.1 Installation Rectifier
			'inst_ac_to_rec' => $request->inst_ac_to_rec, 'inst_ac_to_rec_cond' => $request->inst_ac_to_rec_cond,
			'inst_alarm_to_rec' => $request->inst_alarm_to_rec, 'inst_alarm_to_rec_cond' => $request->inst_alarm_to_rec_cond,
			'label_ddf' => $request->label_ddf, 'label_ddf_cond' => $request->label_ddf_cond,
			'inst_blank_panel' => $request->inst_blank_panel, 'inst_blank_panel_cond' => $request->inst_blank_panel_cond,

			// 5.2 Installation Battery
			'inst_batt_pos' => $request->inst_batt_pos, 'inst_batt_pos_cond' => $request->inst_batt_pos_cond,
			'inst_batt_bank' => $request->inst_batt_bank, 'inst_batt_bank_cond' => $request->inst_batt_bank_cond,
			'inst_batt_temp' => $request->inst_batt_temp, 'inst_batt_temp_cond' => $request->inst_batt_temp_cond,

			// 5.3 Installation SDPAC
			'inst_ac_to_rec_sdpac' => $request->inst_ac_to_rec_sdpac, 'inst_ac_to_rec_sdpac_cond' => $request->inst_ac_to_rec_sdpac_cond,
			'inst_arrester' => $request->inst_arrester, 'inst_arrester_cond' => $request->inst_arrester_cond,

			// 5.4 Installation Grounding
			'ground_cable_size' => $request->ground_cable_size, 'ground_cable_size_cond' => $request->ground_cable_size_cond,
			'inst_ground_to_rectifier' => $request->inst_ground_to_rectifier, 'inst_ground_to_rectifier_cond' => $request->inst_ground_to_rectifier_cond,

			// Functional
			// 1.1 Current Display
			'func_mc_cdisplay_rectifier_ref' => $request->func_mc_cdisplay_rectifier_ref, 'func_mc_cdisplay_rectifier_res' => $request->func_mc_cdisplay_rectifier_res,
			'func_mc_cdisplay_load_ref' => $request->func_mc_cdisplay_load_ref, 'func_mc_cdisplay_load_res' => $request->func_mc_cdisplay_load_res,
			'func_mc_cdisplay_battery_ref' => $request->func_mc_cdisplay_battery_ref, 'func_mc_cdisplay_battery_res' => $request->func_mc_cdisplay_battery_res,

			// 1.2 Current Measurement
			'func_mc_cmeasure_load_ref' => $request->func_mc_cmeasure_load_ref, 'func_mc_cmeasure_load_res' => $request->func_mc_cmeasure_load_res,
			'func_mc_cmeasure_batt_ref' => $request->func_mc_cmeasure_batt_ref, 'func_mc_cmeasure_batt_res' => $request->func_mc_cmeasure_batt_res,

			// 1.3 Bus Voltage Measurement
			'func_mc_bus_volt_display_ref' => $request->func_mc_bus_volt_display_ref, 'func_mc_bus_volt_display_res' => $request->func_mc_bus_volt_display_res,
			'func_mc_bus_volt_measure_ref' => $request->func_mc_bus_volt_measure_ref, 'func_mc_bus_volt_measure_res' => $request->func_mc_bus_volt_measure_res,

			// 1.4 AC Voltage Display
			'func_mc_ac_volt_display_ref' => $request->func_mc_ac_volt_display_ref, 'func_mc_ac_volt_display_res' => $request->func_mc_ac_volt_display_res,

			// 1.5 AC Voltage Measurement
			'func_mc_ac_volt_measure_v_phase_r_ref' => $request->func_mc_ac_volt_measure_v_phase_r_ref, 'func_mc_ac_volt_measure_v_phase_r_res' => $request->func_mc_ac_volt_measure_v_phase_r_res,
			'func_mc_ac_volt_measure_v_phase_s_ref' => $request->func_mc_ac_volt_measure_v_phase_s_ref, 'func_mc_ac_volt_measure_v_phase_s_res' => $request->func_mc_ac_volt_measure_v_phase_s_res,
			'func_mc_ac_volt_measure_v_phase_t_ref' => $request->func_mc_ac_volt_measure_v_phase_t_ref, 'func_mc_ac_volt_measure_v_phase_t_res' => $request->func_mc_ac_volt_measure_v_phase_t_res,
			'func_mc_ac_volt_measure_v_netral_ref' => $request->func_mc_ac_volt_measure_v_netral_ref, 'func_mc_ac_volt_measure_v_netral_res' => $request->func_mc_ac_volt_measure_v_netral_res,

			// 1.6 LVD Setting
			'func_mc_lvd_1_dc_ref' => $request->func_mc_lvd_1_dc_ref, 'func_mc_lvd_1_dc_res' => $request->func_mc_lvd_1_dc_res,
			'func_mc_lvd_1_rc_ref' => $request->func_mc_lvd_1_rc_ref, 'func_mc_lvd_1_rc_res' => $request->func_mc_lvd_1_rc_res,
			'func_mc_lvd_2_dc_ref' => $request->func_mc_lvd_2_dc_ref, 'func_mc_lvd_2_dc_res' => $request->func_mc_lvd_2_dc_res,
			'func_mc_lvd_2_rc_ref' => $request->func_mc_lvd_2_rc_ref, 'func_mc_lvd_2_rc_res' => $request->func_mc_lvd_2_rc_res,

			// 1.7 Alarm Setting
			'func_mc_alarm_high_float_ref' => $request->func_mc_alarm_high_float_ref, 'func_mc_alarm_high_float_res' => $request->func_mc_alarm_high_float_res,
			'func_mc_alarm_low_float_ref' => $request->func_mc_alarm_low_float_ref, 'func_mc_alarm_low_float_res' => $request->func_mc_alarm_low_float_res,
			'func_mc_alarm_low_load_ref' => $request->func_mc_alarm_low_load_ref, 'func_mc_alarm_low_load_res' => $request->func_mc_alarm_low_load_res,
			'func_mc_alarm_ac_fail_ref' => $request->func_mc_alarm_ac_fail_ref, 'func_mc_alarm_ac_fail_res' => $request->func_mc_alarm_ac_fail_res,
			'func_mc_alarm_partial_ac_fail_ref' => $request->func_mc_alarm_partial_ac_fail_ref, 'func_mc_alarm_partial_ac_fail_res' => $request->func_mc_alarm_partial_ac_fail_res,
			'func_mc_alarm_battery_fuse_fail_ref' => $request->func_mc_alarm_battery_fuse_fail_ref, 'func_mc_alarm_battery_fuse_fail_res' => $request->func_mc_alarm_battery_fuse_fail_res,
			'func_mc_alarm_rectifier_fail_ref' => $request->func_mc_alarm_rectifier_fail_ref, 'func_mc_alarm_rectifier_fail_res' => $request->func_mc_alarm_rectifier_fail_res,
			'func_mc_alarm_battery_high_temp_ref' => $request->func_mc_alarm_battery_high_temp_ref, 'func_mc_alarm_battery_high_temp_res' => $request->func_mc_alarm_battery_high_temp_res,

			// 1.8 Alarm Relay M & S
			'func_mc_alarm_relay_1_ref' => $request->func_mc_alarm_relay_1_ref, 'func_mc_alarm_relay_1_res' => $request->func_mc_alarm_relay_1_res,
			'func_mc_alarm_relay_2_ref' => $request->func_mc_alarm_relay_2_ref, 'func_mc_alarm_relay_2_res' => $request->func_mc_alarm_relay_2_res,
			'func_mc_alarm_relay_3_ref' => $request->func_mc_alarm_relay_3_ref, 'func_mc_alarm_relay_3_res' => $request->func_mc_alarm_relay_3_res,
			'func_mc_alarm_relay_4_ref' => $request->func_mc_alarm_relay_4_ref, 'func_mc_alarm_relay_4_res' => $request->func_mc_alarm_relay_4_res,
			'func_mc_alarm_relay_5_ref' => $request->func_mc_alarm_relay_5_ref, 'func_mc_alarm_relay_5_res' => $request->func_mc_alarm_relay_5_res,
			'func_mc_alarm_relay_6_ref' => $request->func_mc_alarm_relay_6_ref, 'func_mc_alarm_relay_6_res' => $request->func_mc_alarm_relay_6_res,
			'func_mc_alarm_relay_7_ref' => $request->func_mc_alarm_relay_7_ref, 'func_mc_alarm_relay_7_res' => $request->func_mc_alarm_relay_7_res,
			'func_mc_alarm_relay_8_ref' => $request->func_mc_alarm_relay_8_ref, 'func_mc_alarm_relay_8_res' => $request->func_mc_alarm_relay_8_res,
			'func_mc_alarm_relay_9_ref' => $request->func_mc_alarm_relay_9_ref, 'func_mc_alarm_relay_9_res' => $request->func_mc_alarm_relay_9_res,



		]);

		return redirect('/photos/create')->with('message', 'Dokumen Berhasil Di buat!');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{

	}

	public function approve_0($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 2,
			    'datesign_pm' => $current_date_time
			    ]);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_2($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$documents = Document::findOrFail($id);
        if ($documents->doc_type == 1) {
            $documents->doc_status = 8;
            $documents->datesign_infra = $current_date_time;
            $documents->save();
        } else {
            if($documents->doc_status_temp == 5 || $documents->doc_status_temp == 6) {
                $documents->doc_status = 5;
                $documents->doc_status_temp = 0;
                $documents->datesign_infra = $current_date_time;
                $documents->save();
            } else {
                $document = DB::table('documents')
                ->where('doc_no', $id)
                ->update([
                    'doc_status' => 3,
                    'datesign_infra' => $current_date_time
                    ]);

                $penerima = DB::table('users')
                ->select('*')
                ->where('location', $documents->site_loc)
                ->first();

                Mail::to($penerima->email)->send(new ReportNotificationEmail($documents, $penerima->name));

                $email_log = new EmailLog;
                $email_log->id_penerima = $penerima->id;
                $email_log->doc_no = $documents->doc_no;
                if (count(Mail::failures()) > 0) {
                    $email_log->status = 'Failed';
                } else {
                    $email_log->status = 'Success';
                }

                $email_log->save();
            }
        }


		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_3($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 4,
			    'datesign_rtpo' => $current_date_time
			    ]);

        $reviewed_document = Document::findOrFail($id);
		$penerima = DB::table('users')
			->select('*')
			->where('location', $reviewed_document->site_area)
			->first();

		Mail::to($penerima->email)->send(new ReportNotificationEmail($reviewed_document, $penerima->name));

        $email_log = new EmailLog;
        $email_log->id_penerima = $penerima->id;
		$email_log->doc_no = $reviewed_document->doc_no;
		if (count(Mail::failures()) > 0) {
			$email_log->status = 'Failed';
		} else {
			$email_log->status = 'Success';
		}

		$email_log->save();

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_4($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 5,
			    'datesign_ns' => $current_date_time
			]);

        $reviewed_document = Document::findOrFail($id);
		$penerima = DB::table('users')
			->select('*')
			->where('jobdesk', 'tkmcpo')
			->orderBy('id','DESC')
			->first();

        Mail::to($penerima->email)->send(new ReportNotificationEmail($reviewed_document, $penerima->name));

        $email_log = new EmailLog;
        $email_log->id_penerima = $penerima->id;
		$email_log->doc_no = $reviewed_document->doc_no;
		if (count(Mail::failures()) > 0) {
			$email_log->status = 'Failed';
		} else {
			$email_log->status = 'Success';
		}

		$email_log->save();

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_5($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 6,
			    'datesign_stafcpo' => $current_date_time
			    ]);

        $reviewed_document = Document::findOrFail($id);
		$penerima = DB::table('users')
			->select('*')
			->where('jobdesk', 'tkmmanagercpo')
			->orderBy('id','DESC')
			->first();

        Mail::to($penerima->email)->send(new ReportNotificationEmail($reviewed_document, $penerima->name));

        $email_log = new EmailLog;
        $email_log->id_penerima = $penerima->id;
		$email_log->doc_no = $reviewed_document->doc_no;
		if (count(Mail::failures()) > 0) {
			$email_log->status = 'Failed';
		} else {
			$email_log->status = 'Success';
		}

		$email_log->save();

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function approve_6($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 7,
			    'datesign_cpo' => $current_date_time
			    ]);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function approve_rect_batt_reviewer($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 10,
			    'datesign_rev_rect_batt' => $current_date_time,
			    'datesign_pm_rect_batt' => $current_date_time,
			    ]);

// 			->update([
// 			    'doc_status' => 9,
// 			    'datesign_rev_rect_batt' => $current_date_time
// 			    ]);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

    public function approve_rect_batt_pm($id)
	{
	    $current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();
		$document = DB::table('documents')
			->where('doc_no', $id)
			->update([
			    'doc_status' => 10,
			    'datesign_pm_rect_batt' => $current_date_time
			    ]);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Approve!');
	}

	public function reject(Request $request, $id, $id_user)
	{
		$this->validate($request, [
			'doc_remarks' => 'required|string',
		]);

		$document = DB::table('documents')
			->where('doc_no', $id)
			->update(
				[
					'doc_status' => 1,
					'doc_remarks' => $request->doc_remarks
				]
			);

		$current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();

		$document_details = DB::table('documents')
			->where('doc_no', $id)
			->select('site_name', 'site_id', 'site_loc', 'site_area')
			->first();

		$user = DB::table('users')
			->where('id', $id_user)
			->select('name')
			->first();

		$reject = DB::table('rejection_records')
			->insert(
				[
					'doc_id' => $id,
					'site_id' => $document_details->site_id,
					'site_name' => $document_details->site_name,
					'site_loc' => $document_details->site_loc,
					'site_area' => $document_details->site_area,
					'date' => $current_date_time,
					'user_id' => $id_user,
					'user_name' => $user->name,
					'doc_remarks' => $request->doc_remarks
				]
			);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Reject!');
	}

	public function reject_tkmcpo(Request $request, $id, $id_user)
	{
		$this->validate($request, [
			'doc_remarks' => 'required|string',
		]);

		$document = DB::table('documents')
			->where('doc_no', $id)
			->update(
				[
					'doc_status' => 1,
					'doc_status_temp' => 5,
					'doc_remarks' => $request->doc_remarks
				]
			);

		$current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();

		$document_details = DB::table('documents')
			->where('doc_no', $id)
			->select('site_name', 'site_id', 'site_loc', 'site_area')
			->first();

		$user = DB::table('users')
			->where('id', $id_user)
			->select('name')
			->first();

		$reject = DB::table('rejection_records')
			->insert(
				[
					'doc_id' => $id,
					'site_id' => $document_details->site_id,
					'site_name' => $document_details->site_name,
					'site_loc' => $document_details->site_loc,
					'site_area' => $document_details->site_area,
					'date' => $current_date_time,
					'user_id' => $id_user,
					'user_name' => $user->name,
					'doc_remarks' => $request->doc_remarks
				]
			);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Reject!');
	}

	public function reject_tkmmanagercpo(Request $request, $id, $id_user)
	{
		$this->validate($request, [
			'doc_remarks' => 'required|string',
		]);

		$document = DB::table('documents')
			->where('doc_no', $id)
			->update(
				[
					'doc_status' => 1,
					'doc_status_temp' => 6,
					'doc_remarks' => $request->doc_remarks
				]
			);

		$current_date_time = \Carbon\Carbon::now()->toDayDateTimeString();

		$document_details = DB::table('documents')
			->where('doc_no', $id)
			->select('site_name', 'site_id', 'site_loc', 'site_area')
			->first();

		$user = DB::table('users')
			->where('id', $id_user)
			->select('name')
			->first();

		$reject = DB::table('rejection_records')
			->insert(
				[
					'doc_id' => $id,
					'site_id' => $document_details->site_id,
					'site_name' => $document_details->site_name,
					'site_loc' => $document_details->site_loc,
					'site_area' => $document_details->site_area,
					'date' => $current_date_time,
					'user_id' => $id_user,
					'user_name' => $user->name,
					'doc_remarks' => $request->doc_remarks
				]
			);

		return redirect('/report')->with('message', 'Dokumen Berhasil Di Reject!');
	}

	public function cetak_atp($id)
	{
		$document = Document::findOrFail($id);
		$pdf = PDF::loadview('/pages/report/atp',
			[
				'documents'=>$document
			]
		);
		return $pdf->stream();
	}

	public function cetak_bal($id)
	{
		$document = Document::findOrFail($id);
		$pdf = PDF::loadview('/pages/report/bal',['documents'=>$document]);
		return $pdf->stream();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$documents = Document::findOrFail($id);
		return view('pages.report.update', ['documents' => $documents]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'boq' => 'file|max:30000|mimes:pdf',
			'bapa' => 'file|max:30000|mimes:pdf',
			'tambahan' => 'file|max:30000|mimes:pdf',
		]);


		$documents = Document::findOrFail($id);

		if($request->hasFile('tambahan')) {
			$this->deleteTambahan($documents);
			$documents->file_tambahan = $this->uploadTambahan($request);
		}

		if($request->hasFile('boq')) {
			$this->deleteBoq($documents);
			$documents->file_boq = $this->uploadBoq($request);
		}

		//Cek apakah user upload file bapa
		if($request->hasFile('bapa')) {
			$this->deleteBapa($documents);
			$documents->file_bapa = $this->uploadBapa($request);
		}

		$documents->save();

		DB::table('documents')->where('doc_no',$id)->update([
			// Meta Data
			//'doc_no' => $request->doc_no,
			//'doc_date' => $request->doc_date, 'doc_ver' => $request->doc_ver,

			// Basic Information
			'doc_name' => $request->doc_name, 'doc_info' => $request->doc_info,
            'doc_type' => $request->doc_type,
		    'project_id' => $request->project_id,
			'project_name' => $request->project_name,
			'site_name' =>$request->site_name, 'site_id' => $request->site_id,
			'site_loc' => $request->site_loc, 'site_area' => $request->site_area,


			// Document Status
			'doc_status' => 0,

			// Rectifier System
			'rectifier_series' => $request->rectifier_series, 'rectifier_type' => $request->rectifier_type, 'rectifier_capacity' => $request->rectifier_capacity,
			'voltage_input' => $request->voltage_input,

			// Visual Check
			// 1.1 Rack
			'rack_type' => $request->rack_type,
			'rack_type_cond' => $request->rack_type_cond,
			'rack_dimension' => $request->rack_dimension,

			// 1.2 Name Plate
			'tsel_name_plate' => $request->tsel_name_plate, 'tsel_name_plate_cond' => $request->tsel_name_plate_cond,
			'vendor_name_plate' => $request->vendor_name_plate, 'vendor_name_plate_cond' => $request->vendor_name_plate_cond,

			// 1.3 Incoming AC Distribution
			'ac_cable_type_size' => $request->ac_cable_type_size, 'ac_cable_type_size_cond' => $request->ac_cable_type_size_cond,
			'ac_mcb_brand' => $request->ac_mcb_brand, 'ac_mcb_brand_cond' => $request->ac_mcb_brand_cond,
			'ac_mcb_type' => $request->ac_mcb_type, 'ac_mcb_type_cond' => $request->ac_mcb_type_cond,
			'ac_mcb_rate' => $request->ac_mcb_rate, 'ac_mcb_rate_cond' => $request->ac_mcb_rate_cond,

			// 1.4 Rectifier Module Distribution
			'rec_mcb_brand' => $request->rec_mcb_brand, 'rec_mcb_brand_cond' => $request->rec_mcb_brand_cond,
			'rec_mcb_type' => $request->rec_mcb_type, 'rec_mcb_type_cond' => $request->rec_mcb_type_cond,
			'rec_mcb_rate' => $request->rec_mcb_rate, 'rec_mcb_rate_cond' => $request->rec_mcb_rate_cond,

			// 1.5 Battery Distribution
			'batt_mcb_brand' => $request->batt_mcb_brand, 'batt_mcb_brand_cond' => $request->batt_mcb_brand_cond,
			'batt_mcb_type' => $request->batt_mcb_type, 'batt_mcb_type_cond' => $request->batt_mcb_type_cond,
			'batt_mcb_rate' => $request->batt_mcb_rate, 'batt_mcb_rate_cond' => $request->batt_mcb_rate_cond,
			'batt_mcb_quantity' => $request->batt_mcb_quantity, 'batt_mcb_quantity_cond' => $request->batt_mcb_quantity_cond,

			// 1.6 Load Distribution
			'load_mcb_brand' => $request->load_mcb_brand, 'load_mcb_brand_cond' => $request->load_mcb_brand_cond,
			'load_mcb_type' => $request->load_mcb_type, 'load_mcb_type_cond' => $request->load_mcb_type_cond,
			'load_mcb_rate_1' => $request->load_mcb_rate_1, 'load_mcb_rate_1_cond' => $request->load_mcb_rate_1_cond,
			'load_mcb_rate_2' => $request->load_mcb_rate_2, 'load_mcb_rate_2_cond' => $request->load_mcb_rate_2_cond,
			'load_mcb_rate_3' => $request->load_mcb_rate_3, 'load_mcb_rate_3_cond' => $request->load_mcb_rate_3_cond,

			// 1.7 Incoming Arrester
			'arrester_brand' => $request->arrester_brand, 'arrester_brand_cond' => $request->arrester_brand_cond,
			'arrester_type' => $request->arrester_type, 'arrester_type_cond' => $request->arrester_type_cond,

			// 1.8 Subtrack System Module
			'subtrack_quantity' => $request->subtrack_quantity, 'subtrack_quantity_cond' => $request->subtrack_quantity_cond,
			'subtrack_serial_number_1' => $request->subtrack_serial_number_1, 'subtrack_serial_number_1_cond' => $request->subtrack_serial_number_1_cond,
			'subtrack_serial_number_2' => $request->subtrack_serial_number_2, 'subtrack_serial_number_2_cond' => $request->subtrack_serial_number_2_cond,
			'subtrack_serial_number_3' => $request->subtrack_serial_number_3, 'subtrack_serial_number_3_cond' => $request->subtrack_serial_number_3_cond,

			// 1.9 Battery Rack Type
			'batt_rack_type'=> $request->batt_rack_type,
			'batt_rack_dimension' => $request->batt_rack_dimension, 'batt_rack_dimension_cond' => $request->batt_rack_dimension_cond,
			'batt_rack_capacity_bank_block' => $request->batt_rack_capacity_bank_block, 'batt_rack_capacity_bank_block_cond' => $request->batt_rack_capacity_bank_block_cond,
			'batt_rack_quantity' => $request->batt_rack_quantity, 'batt_rack_quantity_cond' => $request->batt_rack_quantity_cond,

			// 1.10 Battery Security System
			'batt_cage' => $request->batt_cage, 'batt_cage_cond' => $request->batt_cage_cond,
			'batt_limit_switch_alarm' => $request->batt_limit_switch_alarm, 'batt_limit_switch_alarm_cond' => $request->batt_limit_switch_alarm_cond,

			// 1.11 DDF for Alarm
			'ddf_wiring' => $request->ddf_wiring, 'ddf_wiring_cond' => $request->ddf_wiring_cond,
			'ddf_alarm' => $request->ddf_alarm, 'ddf_alarm_cond' => $request->ddf_alarm_cond,

			// 2.1 Monitor Control card
			'monitor_control_type' => $request->monitor_control_type, 'monitor_control_type_cond' => $request->monitor_control_type_cond,
			'monitor_control_serial_number' => $request->monitor_control_serial_number, 'monitor_control_serial_number_cond' => $request->monitor_control_serial_number_cond,

			// 3.1 Battery
			'batt_brand' => $request->batt_brand, 'batt_brand_cond' => $request->batt_brand_cond,
			'batt_type' => $request->batt_type, 'batt_type_cond' => $request->batt_type_cond,
			'batt_bank' => $request->batt_bank, 'batt_bank_cond' => $request->batt_bank_cond,
			'batt_phy_cond' => $request->batt_phy_cond, 'batt_phy_cond_cond' => $request->batt_phy_cond_cond,
			'batt_serial_number_label' => $request->batt_serial_number_label, 'batt_serial_number_label_cond' => $request->batt_serial_number_label_cond,
			'batt_bank_label' => $request->batt_bank_label, 'batt_bank_label_cond' => $request->batt_bank_label_cond,

			// 3.2 Serial Number Battery
			'batt_sn_1' => $request->batt_sn_1, 'batt_sn_1_bank' => $request->batt_sn_1_bank,
			'batt_sn_2' => $request->batt_sn_2, 'batt_sn_2_bank' => $request->batt_sn_2_bank,
			'batt_sn_3' => $request->batt_sn_3, 'batt_sn_3_bank' => $request->batt_sn_3_bank,
			'batt_sn_4' => $request->batt_sn_4, 'batt_sn_4_bank' => $request->batt_sn_4_bank,
			'batt_sn_5' => $request->batt_sn_5, 'batt_sn_5_bank' => $request->batt_sn_5_bank,
			//'batt_sn_6' => $request->batt_sn_6, 'batt_sn_6_bank' => $request->batt_sn_6_bank,
			//'batt_sn_7' => $request->batt_sn_7, 'batt_sn_7_bank' => $request->batt_sn_7_bank,
			//'batt_sn_8' => $request->batt_sn_8, 'batt_sn_8_bank' => $request->batt_sn_8_bank,
			//'batt_sn_9' => $request->batt_sn_9, 'batt_sn_9_bank' => $request->batt_sn_9_bank,
			//'batt_sn_10' => $request->batt_sn_10, 'batt_sn_10_bank' => $request->batt_sn_10_bank,


			// 4.1 Rectifier Module
			'rec_mod_brand' => $request->rec_mod_brand, 'rec_mod_brand_cond' => $request->rec_mod_brand_cond,
			'rec_mod_type' => $request->rec_mod_type, 'rec_mod_type_cond' => $request->rec_mod_type_cond,
			'rec_mod_capacity' => $request->rec_mod_capacity, 'rec_mod_capacity_cond' => $request->rec_mod_capacity_cond,

			// 4.2 Serial Number Rectifier Module
			'rec_sn1' => $request->rec_sn1, 'rec_sn1_phase' => $request->rec_sn1_phase,
			'rec_sn2' => $request->rec_sn2, 'rec_sn2_phase' => $request->rec_sn2_phase,
			'rec_sn3' => $request->rec_sn3, 'rec_sn3_phase' => $request->rec_sn3_phase,
			'rec_sn4' => $request->rec_sn4, 'rec_sn4_phase' => $request->rec_sn4_phase,
			'rec_sn5' => $request->rec_sn5, 'rec_sn5_phase' => $request->rec_sn5_phase,
			'rec_sn6' => $request->rec_sn6, 'rec_sn6_phase' => $request->rec_sn6_phase,
			'rec_sn7' => $request->rec_sn7, 'rec_sn7_phase' => $request->rec_sn7_phase,
			'rec_sn8' => $request->rec_sn8, 'rec_sn8_phase' => $request->rec_sn8_phase,
			'rec_sn9' => $request->rec_sn9, 'rec_sn9_phase' => $request->rec_sn9_phase,

			// 5.1 Installation Rectifier
			'inst_ac_to_rec' => $request->inst_ac_to_rec, 'inst_ac_to_rec_cond' => $request->inst_ac_to_rec_cond,
			'inst_alarm_to_rec' => $request->inst_alarm_to_rec, 'inst_alarm_to_rec_cond' => $request->inst_alarm_to_rec_cond,
			'label_ddf' => $request->label_ddf, 'label_ddf_cond' => $request->label_ddf_cond,
			'inst_blank_panel' => $request->inst_blank_panel, 'inst_blank_panel_cond' => $request->inst_blank_panel_cond,

			// 5.2 Installation Battery
			'inst_batt_pos' => $request->inst_batt_pos, 'inst_batt_pos_cond' => $request->inst_batt_pos_cond,
			'inst_batt_bank' => $request->inst_batt_bank, 'inst_batt_bank_cond' => $request->inst_batt_bank_cond,
			'inst_batt_temp' => $request->inst_batt_temp, 'inst_batt_temp_cond' => $request->inst_batt_temp_cond,

			// 5.3 Installation SDPAC
			'inst_ac_to_rec_sdpac' => $request->inst_ac_to_rec_sdpac, 'inst_ac_to_rec_sdpac_cond' => $request->inst_ac_to_rec_sdpac_cond,
			'inst_arrester' => $request->inst_arrester, 'inst_arrester_cond' => $request->inst_arrester_cond,

			// 5.4 Installation Grounding
			'ground_cable_size' => $request->ground_cable_size, 'ground_cable_size_cond' => $request->ground_cable_size_cond,
			'inst_ground_to_rectifier' => $request->inst_ground_to_rectifier, 'inst_ground_to_rectifier_cond' => $request->inst_ground_to_rectifier_cond,

			// Functional
			// 1.1 Current Display
			'func_mc_cdisplay_rectifier_ref' => $request->func_mc_cdisplay_rectifier_ref, 'func_mc_cdisplay_rectifier_res' => $request->func_mc_cdisplay_rectifier_res,
			'func_mc_cdisplay_load_ref' => $request->func_mc_cdisplay_load_ref, 'func_mc_cdisplay_load_res' => $request->func_mc_cdisplay_load_res,
			'func_mc_cdisplay_battery_ref' => $request->func_mc_cdisplay_battery_ref, 'func_mc_cdisplay_battery_res' => $request->func_mc_cdisplay_battery_res,

			// 1.2 Current Measurement
			'func_mc_cmeasure_load_ref' => $request->func_mc_cmeasure_load_ref, 'func_mc_cmeasure_load_res' => $request->func_mc_cmeasure_load_res,
			'func_mc_cmeasure_batt_ref' => $request->func_mc_cmeasure_batt_ref, 'func_mc_cmeasure_batt_res' => $request->func_mc_cmeasure_batt_res,

			// 1.3 Bus Voltage Measurement
			'func_mc_bus_volt_display_ref' => $request->func_mc_bus_volt_display_ref, 'func_mc_bus_volt_display_res' => $request->func_mc_bus_volt_display_res,
			'func_mc_bus_volt_measure_ref' => $request->func_mc_bus_volt_measure_ref, 'func_mc_bus_volt_measure_res' => $request->func_mc_bus_volt_measure_res,

			// 1.4 AC Voltage Display
			'func_mc_ac_volt_display_ref' => $request->func_mc_ac_volt_display_ref, 'func_mc_ac_volt_display_res' => $request->func_mc_ac_volt_display_res,

			// 1.5 AC Voltage Measurement
			'func_mc_ac_volt_measure_v_phase_r_ref' => $request->func_mc_ac_volt_measure_v_phase_r_ref, 'func_mc_ac_volt_measure_v_phase_r_res' => $request->func_mc_ac_volt_measure_v_phase_r_res,
			'func_mc_ac_volt_measure_v_phase_s_ref' => $request->func_mc_ac_volt_measure_v_phase_s_ref, 'func_mc_ac_volt_measure_v_phase_s_res' => $request->func_mc_ac_volt_measure_v_phase_s_res,
			'func_mc_ac_volt_measure_v_phase_t_ref' => $request->func_mc_ac_volt_measure_v_phase_t_ref, 'func_mc_ac_volt_measure_v_phase_t_res' => $request->func_mc_ac_volt_measure_v_phase_t_res,
			'func_mc_ac_volt_measure_v_netral_ref' => $request->func_mc_ac_volt_measure_v_netral_ref, 'func_mc_ac_volt_measure_v_netral_res' => $request->func_mc_ac_volt_measure_v_netral_res,

			// 1.6 LVD Setting
			'func_mc_lvd_1_dc_ref' => $request->func_mc_lvd_1_dc_ref, 'func_mc_lvd_1_dc_res' => $request->func_mc_lvd_1_dc_res,
			'func_mc_lvd_1_rc_ref' => $request->func_mc_lvd_1_rc_ref, 'func_mc_lvd_1_rc_res' => $request->func_mc_lvd_1_rc_res,
			'func_mc_lvd_2_dc_ref' => $request->func_mc_lvd_2_dc_ref, 'func_mc_lvd_2_dc_res' => $request->func_mc_lvd_2_dc_res,
			'func_mc_lvd_2_rc_ref' => $request->func_mc_lvd_2_rc_ref, 'func_mc_lvd_2_rc_res' => $request->func_mc_lvd_2_rc_res,

			// 1.7 Alarm Setting
			'func_mc_alarm_high_float_ref' => $request->func_mc_alarm_high_float_ref, 'func_mc_alarm_high_float_res' => $request->func_mc_alarm_high_float_res,
			'func_mc_alarm_low_float_ref' => $request->func_mc_alarm_low_float_ref, 'func_mc_alarm_low_float_res' => $request->func_mc_alarm_low_float_res,
			'func_mc_alarm_low_load_ref' => $request->func_mc_alarm_low_load_ref, 'func_mc_alarm_low_load_res' => $request->func_mc_alarm_low_load_res,
			'func_mc_alarm_ac_fail_ref' => $request->func_mc_alarm_ac_fail_ref, 'func_mc_alarm_ac_fail_res' => $request->func_mc_alarm_ac_fail_res,
			'func_mc_alarm_partial_ac_fail_ref' => $request->func_mc_alarm_partial_ac_fail_ref, 'func_mc_alarm_partial_ac_fail_res' => $request->func_mc_alarm_partial_ac_fail_res,
			'func_mc_alarm_battery_fuse_fail_ref' => $request->func_mc_alarm_battery_fuse_fail_ref, 'func_mc_alarm_battery_fuse_fail_res' => $request->func_mc_alarm_battery_fuse_fail_res,
			'func_mc_alarm_rectifier_fail_ref' => $request->func_mc_alarm_rectifier_fail_ref, 'func_mc_alarm_rectifier_fail_res' => $request->func_mc_alarm_rectifier_fail_res,
			'func_mc_alarm_battery_high_temp_ref' => $request->func_mc_alarm_battery_high_temp_ref, 'func_mc_alarm_battery_high_temp_res' => $request->func_mc_alarm_battery_high_temp_res,

			// 1.8 Alarm Relay M & S
			'func_mc_alarm_relay_1_ref' => $request->func_mc_alarm_relay_1_ref, 'func_mc_alarm_relay_1_res' => $request->func_mc_alarm_relay_1_res,
			'func_mc_alarm_relay_2_ref' => $request->func_mc_alarm_relay_2_ref, 'func_mc_alarm_relay_2_res' => $request->func_mc_alarm_relay_2_res,
			'func_mc_alarm_relay_3_ref' => $request->func_mc_alarm_relay_3_ref, 'func_mc_alarm_relay_3_res' => $request->func_mc_alarm_relay_3_res,
			'func_mc_alarm_relay_4_ref' => $request->func_mc_alarm_relay_4_ref, 'func_mc_alarm_relay_4_res' => $request->func_mc_alarm_relay_4_res,
			'func_mc_alarm_relay_5_ref' => $request->func_mc_alarm_relay_5_ref, 'func_mc_alarm_relay_5_res' => $request->func_mc_alarm_relay_5_res,
			'func_mc_alarm_relay_6_ref' => $request->func_mc_alarm_relay_6_ref, 'func_mc_alarm_relay_6_res' => $request->func_mc_alarm_relay_6_res,
			'func_mc_alarm_relay_7_ref' => $request->func_mc_alarm_relay_7_ref, 'func_mc_alarm_relay_7_res' => $request->func_mc_alarm_relay_7_res,
			'func_mc_alarm_relay_8_ref' => $request->func_mc_alarm_relay_8_ref, 'func_mc_alarm_relay_8_res' => $request->func_mc_alarm_relay_8_res,
			'func_mc_alarm_relay_9_ref' => $request->func_mc_alarm_relay_9_ref, 'func_mc_alarm_relay_9_res' => $request->func_mc_alarm_relay_9_res,

			'doc_remarks' => "-",

		]);

		$photos_id = DB::table('photos')
		->select('photos_id')
        ->where('doc_no', $id)
		->value('photos_id');

		return redirect('photos/'. $photos_id .'/edit')->with('message', 'Document Berhasil Diubah!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$photos_id = DB::table('photos')
        	->select('photos_id')
        	->where('doc_no', $id)
        	->value('photos_id');

        $documents = Document::findOrFail($id);
		$this->deleteBoq($documents);
        $this->deleteBapa($documents);
        $documents->delete();

        $photos = Photo::findOrFail($photos_id);
        $photos->delete();
        return redirect('/report')->with('message', 'Dokumen & Foto Berhasil Dihapus!');

	}

	public function send_mail()
	{
		Mail::to('vjuliandito@gmail.com')->send(new ReportNotificationEmail());
	}

	public function mail_test()
	{
		return view("pages.mailtest");
	}

	public function test_approve_2()
	{
		$dummy_document = (object) [
			'site_id' => 'SITE ID',
			'project_name' => 'PROJECT NAME',
			'site_name' => 'SITE NAME',
			'site_loc' => 'RTPO BALIKPAPAN',
			'site_area' => 'NS BALIKPAPAN',
		];
		$penerima = DB::table('users')
			->select('email', 'name', 'location')
			->where('location', $dummy_document->site_loc)
			->first();

		Mail::to('kejuliandito@gmail.com')->send(new ReportNotificationEmail($dummy_document, $penerima->name));
		echo "Approve 2 Sent!";
	}

	public function test_approve_3()
	{
		$dummy_document = (object) [
			'site_id' => 'SITE ID',
			'project_name' => 'PROJECT NAME',
			'site_name' => 'SITE NAME',
			'site_loc' => 'RTPO BALIKPAPAN',
			'site_area' => 'NS BALIKPAPAN',
		];
		$penerima = DB::table('users')
			->select('email', 'name', 'location', 'jobdesk')
			->where('location', $dummy_document->site_area)
			->first();

		Mail::to('kejuliandito@gmail.com')->send(new ReportNotificationEmail($dummy_document, $penerima->name));
		echo "Approve 3 Sent!";
	}

	public function test_approve_4()
	{
		$dummy_document = (object) [
			'site_id' => 'SITE ID',
			'project_name' => 'PROJECT NAME',
			'site_name' => 'SITE NAME',
			'site_loc' => 'RTPO BALIKPAPAN',
			'site_area' => 'NS BALIKPAPAN',
		];
		$penerima = DB::table('users')
			->select('email', 'name', 'location', 'jobdesk')
			->where('jobdesk', 'tkmcpo')
			->orderBy('id','DESC')
			->first();

			Mail::to('kejuliandito@gmail.com')->send(new ReportNotificationEmail($dummy_document, $penerima->name));
			echo "Approve 4 Sent!";
	}

	public function test_approve_5()
	{
		$dummy_document = (object) [
			'site_id' => 'SITE ID',
			'project_name' => 'PROJECT NAME',
			'site_name' => 'SITE NAME',
			'site_loc' => 'RTPO BALIKPAPAN',
			'site_area' => 'NS BALIKPAPAN',
		];
		$penerima = DB::table('users')
			->select('email', 'name', 'location', 'jobdesk')
			->where('jobdesk', 'tkmmanagercpo')
			->orderBy('id','DESC')
			->first();

		Mail::to('kejuliandito@gmail.com')->send(new ReportNotificationEmail($dummy_document, $penerima->name));
		echo "Approve 5 Sent!";
	}

	/*
	 **************************************
	 * Date Modified : 17/06/2021 10:20   *
	 * Author        : Setiawan Joko      *
	 **************************************
	 */

    public function viewTotalDocs(){
        $user = auth()->user();

        if(is_null($user->location)) {
            $documents = Document::where([
                ['doc_type', 0]
            ])->orWhere([
                ['doc_type', null]
            ]);
        }

		if($user->jobdesk == 'reviewertkmrto'){
			$documents = Document::where([
                ['doc_type', 0],
                ['doc_status', '>=', 3],
                ['site_loc', $user->location]
            ])->orWhere([
                ['doc_type', null],
                ['doc_status', '>=', 3],
                ['site_loc', $user->location]
            ])->orWhere([
                ['doc_type', 0],
                ['doc_status',1],
                ['site_loc', $user->location]
            ])->orWhere([
                ['doc_type', null],
                ['doc_status',1],
                ['site_loc', $user->location]
            ])->get();
        }

        if($user->jobdesk == 'tkmmanagerns'){
			$documents = Document::where([
                ['doc_type', 0],
                ['doc_status', '>=', 3],
                ['site_area', $user->location]
            ])->orWhere([
                ['doc_type', null],
                ['doc_status', '>=', 3],
                ['site_area', $user->location]
            ])->orWhere([
                ['doc_type', 0],
				['doc_status',1],
				['site_area', $user->location]
            ])->orWhere([
                ['doc_type', 0],
                ['doc_status',1],
				['site_area', $user->location]
			])->get();
        }

        return response()->view('pages.report.index', compact('documents'));

    }

    public function viewNeedReviewDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 0]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 0]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 2]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 2]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 3],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 3],
                    ['site_loc', $user->location]
                ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 4],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 4],
                    ['site_area', $user->location]
                ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 5]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 5]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 6]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 6]
                ])->get(); break;
            default: break;
        }

        return response()->view('pages.report.index', compact('documents'));
    }

    public function viewApprovedDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', '>', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 1]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                        ['doc_type', 0],
                        ['doc_status', '>', 2]
                    ])->orWhere([
                        ['doc_type', null],
                        ['doc_status', '>', 2]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', '>', 3],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 3],
                    ['site_loc', $user->location]
            ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', '>', 4],
                    ['site_area', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 4],
                ['site_area', $user->location]
            ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', '>', 5]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 5]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', '>', 6]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', '>', 6]
                ])->get(); break;
            default: break;
        }

        return response()->view('pages.report.index', compact('documents'));
    }

    public function viewRejectedDocs(){
        $user = auth()->user();

        switch ($user->jobdesk){
            case 'vlcpm':  $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkminframanager': $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'reviewertkmrto':  $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 1],
                    ['site_loc', $user->location]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1],
                    ['site_loc', $user->location]
            ])->get(); break;
            case 'tkmmanagerns':    $documents = Document::where([
                ['doc_status', 1],
                ['site_area', $user->location]
            ])->get(); break;
            case 'tkmcpo':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            case 'tkmmanagercpo':    $documents = Document::where([
                    ['doc_type', 0],
                    ['doc_status', 1]
                ])->orWhere([
                    ['doc_type', null],
                    ['doc_status', 1]
                ])->get(); break;
            default: break;
        }

        return response()->view('pages.report.index', compact('documents'));
    }

}
