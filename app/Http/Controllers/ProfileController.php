<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Storage;

class ProfileController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	// Upload + Validasi Gambar Sign
	private function uploadSign(Request $request) {
		$gambar = $request->file('sign');
		$type = $gambar->getClientOriginalExtension();

		if($request->file('sign')->isValid()) {
			$filename = "Sign-" . $request->email . "." . $type;
			$uploadPath = "images/sign/";
			$request->file('sign')->move($uploadPath, $filename);
			return $filename;
		}
		return false;
	}

	// Hapus Gambar Sign di Local Drive
	private function hapusSign(User $user) {
		$exist = Storage::disk('sign')->exist($user->sign);
		if(isset($user->sign) && $exist) {
			$delete = Storage::disk('sign')->delete($user->sign);
			if($delete) {
				return true;
			}
			return false;
		}
	}

	// Delete Gambar Sign
	private function deleteSign($request) {
		$file = 'images/sign/'.$request->sign;
		if(is_file($file)) {
			unlink($file);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$users = User::findOrFail($id);
		return view('pages.profile.update', ['users' => $users]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'password' => 'required|string|min:8|confirmed',
		]);

		$user = User::findOrFail($id);
		$user->real_password = $request->password;
		$user->password = Hash::make($request->password);

		$user->save();
		return redirect('/profile/'.$id.'/edit')->with('message', 'Profile Berhasil Diubah!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
