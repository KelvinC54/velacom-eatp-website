<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

    protected $documents = 'documents';
    protected $table = 'photos';

    protected $fillable = [
        //doc meta
        'doc_no',
        
        //general information
        'gi_site_information','gi_site_access',
        'gi_site_front_view','gi_site_rear_view',
        'gi_site_left_view','gi_site_right_view',
        
        //New Foundation Installation
        'nf_installation_before','nf_installation_1','nf_installation_2',
        'nf_installation_3','nf_installation_4','nf_installation_5',
        'nf_installation_6','nf_installation_7','nf_foundation_wide',
        'nf_foundation_weight','nf_foundation_long','nf_installation_after',
        'site_layout',
        
        //Rectifier Installation
        'ri_base_frame_before','ri_base_frame_after','ri_main_rack_1',
        'ri_slave_rack_1','ri_main_rack_2','ri_slave_rack_2','ri_pln_kwh_before',
        'ri_pln_kwh_after','ri_acpdb_before','ri_acpdb_after','ri_acpdb_mcb_before',
        'ri_acpdb_mcb_after','ri_rectifier_front','ri_main_mcb_ac_input','ri_mcb_dc_dist',
        'ri_controller_module','ri_module_1','ri_module_2','ri_module_3','ri_module_4','ri_dc_cable_con_main_rack',
        'ri_dc_cable_label_main_rack','ri_dc_cable_con_slave_rack','ri_dc_cable_label_slave_rack','ri_gnd_con_1',
        'ri_gnd_con_2','ri_gnd_con_3','ri_gnd_con_4','ri_laying_cable_1','ri_laying_cable_2','ri_laying_cable_3',
        'ri_laying_cable_4','ri_fan_main','ri_fan_slave','ri_lamp_main','ri_lamp_slave','ri_panel_dist',
        'ri_ac_measure_rs','ri_ac_measure_st','ri_ac_measure_rt','ri_ac_measure_rn','ri_ac_measure_sn',
        'ri_ac_measure_tn','ri_ac_measure_ng','ri_dc_output','ri_ac_output','ri_control_module_setting_1',
        'ri_control_module_setting_2','ri_control_module_setting_3','ri_control_module_setting_4',
        'ri_alarm_connection_1','ri_alarm_connection_2','ri_alarm_label_1','ri_alarm_label_2','ri_battery_rack',
        'ri_battery_bank_inst_1','ri_battery_bank_inst_2','ri_battery_bank_inst_3','ri_battery_bank_inst_4','ri_battery_bank_inst_5','ri_battery_bank_1_dc_measure',
        'ri_battery_bank_2_dc_measure','ri_battery_bank_3_dc_measure','ri_battery_bank_4_dc_measure','ri_battery_bank_5_dc_measure','ri_battery_label','ri_battery_main_bar_inst',
        'ri_battery_mcb_fuse','site_layout_rectifier'
        ];

    public function document(){
        return$this->belongsTo('App\Models\Document');
    }


    protected $primaryKey = 'photos_id';
    public $timestamps = false;
}
