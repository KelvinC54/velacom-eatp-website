<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	use HasFactory;

	protected $table = "documents";

	protected $fillable = [

		// Meta Data
		'doc_no','file_bapa','file_boq','file_tambahan','doc_date','doc_ver','doc_type',

		//doc remarks
		'doc_remarks',

		//doc timestamp sign
		'datesign_pm',
		'datesign_infra',
		'datesign_rtpo',
		'datesign_ns',
		'datesign_stafcpo',
		'datesign_cpo',
        'datesign_rev_rect_batt',
        'datesign_pm_rect_batt',

		// Document Status
		'doc_status', 'doc_status_temp',

		// Basic Information
		'doc_name','doc_info',
		'project_name','project_id',
		'site_id','site_name',
		'site_loc','site_area',

		// Rectifier System
		'rectifier_series','rectifier_type','rectifier_capacity','voltage_input',

		// Visual Check
		// 1.1 Rack
		'rack_type','rack_type_cond','rack_dimension',

		// 1.2 Name Plate
		'tsel_name_plate','tsel_name_plate_cond',
		'vendor_name_plate','vendor_name_plate_cond',

		// 1.3 Incoming AC Distribution
		'ac_cable_type_size','ac_cable_type_size_cond',
		'ac_mcb_brand','ac_mcb_brand_cond',
		'ac_mcb_type','ac_mcb_type_cond',
		'ac_mcb_rate','ac_mcb_rate_cond',

		// 1.4 Rectifier Module Distribution
		'rec_mcb_brand','rec_mcb_brand_cond',
		'rec_mcb_type','rec_mcb_type_cond',
		'rec_mcb_rate','rec_mcb_rate_cond',

		// 1.5 Battery Distribution
		'batt_mcb_brand','batt_mcb_brand_cond',
		'batt_mcb_type','batt_mcb_type_cond',
		'batt_mcb_rate','batt_mcb_rate_cond',
		'batt_mcb_quantity','batt_mcb_quantity_cond',

		// 1.6 Load Distribution
		'load_mcb_brand','load_mcb_brand_cond',
		'load_mcb_type','load_mcb_type_cond',
		'load_mcb_rate_1','load_mcb_rate_1_cond',
		'load_mcb_rate_2','load_mcb_rate_2_cond',
		'load_mcb_rate_3','load_mcb_rate_3_cond',

		// 1.7 Incoming Arrester
		'arrester_brand','arrester_brand_cond',
		'arrester_type','arrester_type_cond',

		// 1.8 Subtrack System Module
		'subtrack_quantity','subtrack_quantity_cond',
		'subtrack_serial_number_1','subtrack_serial_number_1_cond',
		'subtrack_serial_number_2','subtrack_serial_number_2_cond',
		'subtrack_serial_number_3','subtrack_serial_number_3_cond',

		// 1.9 Battery Rack Type
		'batt_rack_type',
		'batt_rack_dimension','batt_rack_dimension_cond',
		'batt_rack_capacity_bank_block','batt_rack_capacity_bank_block_cond',
		'batt_rack_quantity','batt_rack_quantity_cond',

		// 1.10 Battery Security System
		'batt_cage','batt_cage_cond',
		'batt_limit_switch_alarm','batt_limit_switch_alarm_cond',

		// 1.11 DDF for Alarm
		'ddf_wiring','ddf_wiring_cond',
		'ddf_alarm','ddf_alarm_cond',

		// 2.1 Monitor Control card
		'monitor_control_type','monitor_control_type_cond',
		'monitor_control_serial_number','monitor_control_serial_number_cond',

		// 3.1 Battery
		'batt_brand','batt_brand_cond',
		'batt_type','batt_type_cond',
		'batt_bank','batt_bank_cond',
		'batt_phy_cond','batt_phy_cond_cond',
		'batt_serial_number_label','batt_serial_number_label_cond',
		'batt_bank_label','batt_bank_label_cond',

		// 3.2 Serial Number Battery
		'batt_sn_1','batt_sn_1_bank',
		'batt_sn_2','batt_sn_2_bank',
		'batt_sn_3','batt_sn_3_bank',
		'batt_sn_4','batt_sn_4_bank',
		'batt_sn_5','batt_sn_5_bank',
		'batt_sn_6','batt_sn_6_bank',
		'batt_sn_7','batt_sn_7_bank',
		'batt_sn_8','batt_sn_8_bank',
		'batt_sn_9','batt_sn_9_bank',
		'batt_sn_10','batt_sn_10_bank',

		// 4.1 Rectifier Module
		'rec_mod_brand','rec_mod_brand_cond',
		'rec_mod_type','rec_mod_type_cond',
		'rec_mod_capacity','rec_mod_capacity_cond',

		// 4.2 Serial Number Rectifier Module
		'rec_sn1','rec_sn1_phase',
		'rec_sn2','rec_sn2_phase',
		'rec_sn3','rec_sn3_phase',
		'rec_sn4','rec_sn4_phase',
		'rec_sn5','rec_sn5_phase',
		'rec_sn6','rec_sn6_phase',
		'rec_sn7','rec_sn7_phase',
		'rec_sn8','rec_sn8_phase',
		'rec_sn9','rec_sn9_phase',

		// 5.1 Installation Rectifier
		'inst_ac_to_rec','inst_ac_to_rec_cond',
		'inst_alarm_to_rec','inst_alarm_to_rec_cond',
		'label_ddf','label_ddf_cond',
		'inst_blank_panel','inst_blank_panel_cond',

		// 5.2 Installation Battery
		'inst_batt_pos','inst_batt_pos_cond',
		'inst_batt_bank','inst_batt_bank_cond',
		'inst_batt_temp','inst_batt_temp_cond',

		// 5.3 Installation SDPAC
		'inst_ac_to_rec_sdpac','inst_ac_to_rec_sdpac_cond',
		'inst_arrester','inst_arrester_cond',

		// 5.4 Installation Grounding
		'ground_cable_size','ground_cable_size_cond',
		'inst_ground_to_rectifier','inst_ground_to_rectifier_cond',

		// Functional
		// 1.1 Current Display
		'func_mc_cdisplay_rectifier_ref','func_mc_cdisplay_rectifier_res',
		'func_mc_cdisplay_load_ref','func_mc_cdisplay_load_res',
		'func_mc_cdisplay_battery_ref','func_mc_cdisplay_battery_res',

		// 1.2 Current Measurement
		'func_mc_cmeasure_load_ref','func_mc_cmeasure_load_res',
		'func_mc_cmeasure_batt_ref','func_mc_cmeasure_batt_res',

		// 1.3 Bus Voltage Measurement
		'func_mc_bus_volt_display_ref','func_mc_bus_volt_display_res',
		'func_mc_bus_volt_measure_ref','func_mc_bus_volt_measure_res',

		// 1.4 AC Voltage Display
		'func_mc_ac_volt_display_ref','func_mc_ac_volt_display_res',

		// 1.5 AC Voltage Measurement
		'func_mc_ac_volt_measure_v_phase_r_ref','func_mc_ac_volt_measure_v_phase_r_res',
		'func_mc_ac_volt_measure_v_phase_s_ref','func_mc_ac_volt_measure_v_phase_s_res',
		'func_mc_ac_volt_measure_v_phase_t_ref','func_mc_ac_volt_measure_v_phase_t_res',
		'func_mc_ac_volt_measure_v_netral_ref','func_mc_ac_volt_measure_v_netral_res',

		// 1.6 LVD Setting
		'func_mc_lvd_1_dc_ref','func_mc_lvd_1_dc_res',
		'func_mc_lvd_1_rc_ref','func_mc_lvd_1_rc_res',
		'func_mc_lvd_2_dc_ref','func_mc_lvd_2_dc_res',
		'func_mc_lvd_2_rc_ref','func_mc_lvd_2_rc_res',

		// 1.7 Alarm Setting
		'func_mc_alarm_high_float_ref','func_mc_alarm_high_float_res',
		'func_mc_alarm_low_float_ref','func_mc_alarm_low_float_res',
		'func_mc_alarm_low_load_ref','func_mc_alarm_low_load_res',
		'func_mc_alarm_ac_fail_ref','func_mc_alarm_ac_fail_res',
		'func_mc_alarm_partial_ac_fail_ref','func_mc_alarm_partial_ac_fail_res',
		'func_mc_alarm_battery_fuse_fail_ref','func_mc_alarm_battery_fuse_fail_res',
		'func_mc_alarm_rectifier_fail_ref','func_mc_alarm_rectifier_fail_res',
		'func_mc_alarm_battery_high_temp_ref','func_mc_alarm_battery_high_temp_res',

		// 1.8 Alarm Relay M & S
		'func_mc_alarm_relay_1_ref','func_mc_alarm_relay_1_res',
		'func_mc_alarm_relay_2_ref','func_mc_alarm_relay_2_res',
		'func_mc_alarm_relay_3_ref','func_mc_alarm_relay_3_res',
		'func_mc_alarm_relay_4_ref','func_mc_alarm_relay_4_res',
		'func_mc_alarm_relay_5_ref','func_mc_alarm_relay_5_res',
		'func_mc_alarm_relay_6_ref','func_mc_alarm_relay_6_res',
		'func_mc_alarm_relay_7_ref','func_mc_alarm_relay_7_res',
		'func_mc_alarm_relay_8_ref','func_mc_alarm_relay_8_res',
		'func_mc_alarm_relay_9_ref','func_mc_alarm_relay_9_res',

	];

	protected $primaryKey = 'doc_no';

	public function photo(){
		return $this->hasOne('App\Models\Photo');
	}
	public $timestamps = false;

}
