<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Rejection extends Model
{

    protected $table = 'rejection_records';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'doc_id',
        'site_id',
        'site_name',
        'site_loc',
        'site_area',
        'date',
        'user_id',
        'user_name',
        'doc_remarks'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function document(){
        return $this->belongsTo(Document::class, 'doc_id', 'id');
    }
}
