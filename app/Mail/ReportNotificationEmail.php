<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportNotificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $document;
    public $nama;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document_approved, $penerima)
    {
        $this->nama = $penerima;
        $this->document = $document_approved;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info-noreply@velosa.co.id')
                    ->subject('Pemberitahuan Review Dokumen ATP')
                    ->view('mail.reportnotificationemail');
    }
}

?>
