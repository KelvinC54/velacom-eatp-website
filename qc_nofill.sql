-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 11, 2021 at 10:52 PM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eatp`
--

-- --------------------------------------------------------

--
-- Table structure for table `qc_nofill`
--

CREATE TABLE `qc_nofill` (
  `id` int(10) NOT NULL,
  `site_id` varchar(30) DEFAULT NULL,
  `type_of_work` varchar(70) DEFAULT NULL,
  `ne_type` varchar(30) DEFAULT NULL,
  `node_name` varchar(70) DEFAULT NULL,
  `doc_tac` varchar(30) DEFAULT NULL,
  `node_id` varchar(30) DEFAULT NULL,
  `po_number` varchar(30) DEFAULT NULL,
  `final_result` varchar(50) DEFAULT NULL,
  `exe_date` varchar(100) DEFAULT NULL,
  `acceptance_date` varchar(100) DEFAULT NULL,
  `drive_test_status` int(5) DEFAULT NULL,
  `kpi_status` int(5) DEFAULT NULL,
  `capture_status` int(5) DEFAULT NULL,
  `site_config_status` int(5) DEFAULT NULL,
  `namesign_pm_vlc` varchar(100) DEFAULT NULL,
  `datesign_pm_vlc` varchar(100) DEFAULT NULL,
  `namesign_infra` varchar(100) DEFAULT NULL,
  `datesign_infra` varchar(100) DEFAULT NULL,
  `namesign_reviewer` varchar(100) DEFAULT NULL,
  `datesign_reviewer` varchar(100) DEFAULT NULL,
  `namesign_pm_tkm` varchar(100) DEFAULT NULL,
  `datesign_pm_tkm` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `qc_nofill`
--
ALTER TABLE `qc_nofill`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `qc_nofill`
--
ALTER TABLE `qc_nofill`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
